#include "generic_webcam.h"
#include "UI/logger.h"

#include <chrono>
#include <QInputDialog>

QStringList Generic_Webcam::getDeviceList(){
    // Check whether devices are available
    const QList<QCameraDevice> cameras = QMediaDevices::videoInputs();
    if (QMediaDevices::videoInputs().count() <= 0){
        std::cout << "No generic USB webcams found." << std::endl;
    }

    // Create a list of camera names
    QStringList camera_names;
    for(const QCameraDevice& device : cameras){
        camera_names.push_back(device.description());
    }

    return camera_names;
}

bool Generic_Webcam::init(const int& idx){

    // Check whether devices are available
    const QList<QCameraDevice> cameras = QMediaDevices::videoInputs();
    if (QMediaDevices::videoInputs().count() <= 0){
        return false;
    }

    // Create the selected camera
    camera_ = std::shared_ptr<QCamera>(new QCamera(cameras[idx]));

    format_list_ = cameras[idx].videoFormats();

    QStringList format_strings;
    for(const QCameraFormat& format : format_list_){
        format_strings.push_back(QString::number(format.resolution().width())
                                 +"X"+
                                 QString::number(format.resolution().height())
                                 +"  "+
                                 QString::number(format.minFrameRate())
                                 +" - "+
                                 QString::number(format.maxFrameRate())
                                 +" FPS");
    }
    formats_ = std::shared_ptr<Property_Selector>(new Property_Selector(nullptr, "Generic_Webcam_Format", "Format",format_strings,0));
    connect(formats_.get(), &Property_Selector::valueChanged, [this](const int& idx){
        camera_->setCameraFormat(format_list_[idx]);
    });
    camera_->setCameraFormat(format_list_[0]);
    properties_.push_back(formats_);

    if(camera_->isExposureModeSupported(QCamera::ExposureManual)){
        // Set manual exposure control to prevent brightness problems
        exposure_ = std::shared_ptr<Property_Spin_F>(new Property_Spin_F(nullptr, "Generic_Webcam_Exposure", "Exposure", 0.0, 100.0, 50.0));
        connect(exposure_.get(), &Property_Spin_F::valueChanged, [this](const float& exp){
            camera_->setManualExposureTime(exp/100.0f * 1.0f/5.0f);
        });

        camera_->setExposureMode(QCamera::ExposureManual);
        camera_->setManualExposureTime(exposure_->value()/100.0f * 1.0f/30.0f);
        properties_.push_back(exposure_);
    }

    if(camera_->isFocusModeSupported(QCamera::FocusModeManual)){
        // Set manual focus control to prevent refocusing during calibration
        focus_distance_ = std::shared_ptr<Property_Spin_F>(new Property_Spin_F(nullptr, "Generic_Webcam_Focus_Distance", "Focus distance (cm)", 0.0, 999999.99, 70.0));
        connect(focus_distance_.get(), &Property_Spin_F::valueChanged, [this](const float& dist){
            camera_->setFocusDistance(dist);
        });

        camera_->setFocusMode(QCamera::FocusModeManual);
        camera_->setFocusDistance(focus_distance_->value());
        properties_.push_back(focus_distance_);
    }

    // Connect camera error log to GCC tool log
    // Note: This is commented out since it leads to random crashes on some
    //       test systems
    //    connect(camera_.get(), &QCamera::errorOccurred,
    //            [this](QCamera::Error error, const QString &errorString){ERROR(errorString);});

    // Create capture session and set camera
    capture_session_ = std::shared_ptr<QMediaCaptureSession>(new QMediaCaptureSession());
    capture_session_->setCamera(camera_.get());

    // Create frame grabber
    video_sink_ = std::shared_ptr<QVideoSink>(new QVideoSink());
    capture_session_->setVideoSink(video_sink_.get());

    // Connect the grabber to handle new images
    connect(video_sink_.get(), &QVideoSink::videoFrameChanged,
            this, &Generic_Webcam::getNewImage);
    // The following connection is only used once and will be removed after
    // setInitialParams is called the first time
    connect(video_sink_.get(), &QVideoSink::videoFrameChanged,
            this, &Generic_Webcam::setInitialParams);

    return true;
}


bool Generic_Webcam::start(){

    // Note: Depending on the system, currently the camera stream is started
    //       without calling this function. In these cases stop() also does not
    //       work. It is not clear whether this is a bug in QtMultimedia or a
    //       problem with device drivers.

    STATUS("Camera start requested..");
    if(camera_){
        camera_->start();
    }
    return true;
}

bool Generic_Webcam::pause(){
    // TODO: Find a way to pause the loop
    return true;
}

bool Generic_Webcam::stop(){

    // Note: Check the note in start()

    STATUS("Camera stop requested..");
    if(camera_){
        camera_->stop();
    }
    return true;
}

std::vector<Cam_Parameters> Generic_Webcam::getInitialParameters(){
    return std::vector<Cam_Parameters>(1, parameters_);
}

void Generic_Webcam::getNewImage(const QVideoFrame& frame){
    if(frame.isValid()){

        // Create timestamp
        long timestamp = std::chrono::duration_cast<std::chrono::milliseconds>(
                    std::chrono::system_clock::now().time_since_epoch()).count();
        std::shared_ptr<const Frame> stamped_frame = std::shared_ptr<Frame>(new Frame(frame, timestamp));
        emit receivedNewFrame(stamped_frame);
    }
}

void Generic_Webcam::setInitialParams(const QVideoFrame& frame){
    is_ready_ = true;

    // Set name
    parameters_.camera_name_ = camera_->cameraDevice().description().toStdString();

    // Set resolution
    parameters_.resolution_x = frame.width();
    parameters_.resolution_y = frame.height();

    // Set default initial calibration parameters
    // Principal point = image center
    parameters_.cx_ = float(frame.width()) / 2.0f;
    parameters_.cy_ = float(frame.height()) / 2.0f;
    // Focal length is calculated using the resolution and assuming 53.4 deg
    // HFOV (which is the HFOV of the webcam this was tested with)
    parameters_.fx_ = parameters_.cx_ / std::tan(53.4f * M_PI / 360.0f);
    parameters_.fy_ = parameters_.fx_;
    // Optional: Set the first distortion parameter to 1. This seems to be a
    // more stable initial setup for the optimization despite giving a slightly
    // wrong distortion in the beginning.
    // parameters_.k_[0] = 1.0;

    // Signal the new parameters to other program parts
    emit initialized(std::vector<Cam_Parameters>(1, parameters_));
    emit propertiesInitialized(properties_);

    // After initialization, this function can be disconnected
    disconnect(video_sink_.get(), &QVideoSink::videoFrameChanged,
               this, &Generic_Webcam::setInitialParams);
}
