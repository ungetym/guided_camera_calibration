////////////////////////////////////////////////////////////////////////////////
///                                                                          ///
///       Implementation of camera class for a generic RGB webcam            ///
///                                                                          ///
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Cameras/camera.h"

#include <QMediaCaptureSession>
#include <QtMultimedia/QMediaDevices>
#include <QtMultimedia/QCamera>
#include <QVideoSink>

///
/// \brief The Generic_Webcam class
///
class Generic_Webcam : public Camera{
    Q_OBJECT

public slots:
    ///
    /// \brief getDeviceList is used to collect the device lists for different
    ///                      camera class implementations and give the user the
    ///                      opportunity to select among all available devices
    /// \return
    ///
    QStringList getDeviceList();

    ///
    /// \brief init checks if the selected device is available and initializes
    ///             it
    /// \param idx  is the index of the device in the list of devices
    /// \return     true, if the initialization was successful, false otherwise
    ///
    bool init(const int &idx);


    ///
    /// \brief start initializes the frame grabber loop and the camera
    ///              parameters via setInitialParams().
    /// \return      true, if successful, false otherwise
    ///
    bool start();

    ///
    /// \brief pause simply pauses the frame grabber. Note: Not implemented.
    /// \return      true, if successful, false otherwise
    ///
    bool pause();

    ///
    /// \brief stop halts the frame grabber loop
    /// \return     true, if successful, false otherwise
    ///
    bool stop();

    ///
    /// \brief getInitialParameters returns the camera parameters
    /// \return
    ///
    std::vector<Cam_Parameters> getInitialParameters();

    ///
    /// \brief isReady can be used to ask the camera class if the camera is
    ///                ready to capture images and the parameters are
    ///                initialized.
    /// \return
    ///
    bool isReady(){return is_ready_;}

    ///
    /// \brief getProperties is a getter for the property widgets controlling
    ///                      camera behaviour such as gain, exposure and white
    ///                      balance
    /// \return
    ///
    std::vector<std::shared_ptr<Property>> getProperties(){return properties_;}

    ///
    /// \brief supportsVisualization returns false since visualizeCalibration is
    ///                              not implemented
    /// \return
    ///
    bool supportsVisualization(){return false;}

    ///
    /// \brief visualizeCalibration is not implemented for a generic webcam
    ///
    /// \param parameters           the parameter configuration to be used for
    ///                             the visualization
    /// \return                     true if successful, false otherwise
    ///
    bool visualizeCalibration(const std::vector<Cam_Parameters>& parameters){
        return false;
    }

private slots:

    ///
    /// \brief getNewImage is called when a new frame is received. The frame is
    ///                    then checked for validity and signalled as Frame
    ///                    struct
    /// \param frame       received from the capturing loop
    ///
    void getNewImage(const QVideoFrame &frame);

    ///
    /// \brief setInitialParams uses the image resolution and a hard-coded
    ///                         guess for the FoV for determining an initial
    ///                         K matrix for the optimization.
    /// \param frame
    ///
    void setInitialParams(const QVideoFrame& frame);

private:

    /// Pointer to the QCamera device
    std::shared_ptr<QCamera> camera_;

    /// Capture session handling the camera stream
    std::shared_ptr<QMediaCaptureSession> capture_session_;

    /// Capture object to get single frames from the camera stream
    std::shared_ptr<QVideoSink> video_sink_;

    /// Initial device parameters
    Cam_Parameters parameters_;

    /// Contains the device status. Is set to true after initialization.
    bool is_ready_ = false;

    /// Properties that can be modified by the user
    QList<QCameraFormat> format_list_;
    std::shared_ptr<Property_Selector> formats_;
    std::shared_ptr<Property_Spin_F> focus_distance_;
    std::shared_ptr<Property_Spin_F> exposure_;

    std::vector<std::shared_ptr<Property>> properties_;
};
