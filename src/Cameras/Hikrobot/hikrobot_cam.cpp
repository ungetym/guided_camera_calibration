#include "hikrobot_cam.h"

#include "UI/logger.h"

#include <fstream>
#include <QInputDialog>
#include <QProcess>
#include <QThread>

Hikrobot_Cam::~Hikrobot_Cam(){
    // Close device
    if (MV_CC_CloseDevice(handle_) != MV_OK){
        ERROR("Closing Hikrobot device failed.");
    }
    if (handle_){
        MV_CC_DestroyHandle(handle_);
        handle_ = nullptr;
    }
}

QStringList Hikrobot_Cam::getDeviceList(){

    QStringList camera_names;

    MV_CC_DEVICE_INFO_LIST dev_list;
    memset(&dev_list, 0, sizeof(MV_CC_DEVICE_INFO_LIST));

    int nRet = MV_OK;
    // Get devices list
    nRet = MV_CC_EnumDevices(MV_GIGE_DEVICE | MV_USB_DEVICE, &dev_list);
    if (MV_OK != nRet){
        std::cerr << "Failed to get Hikrobot device list." << std::endl;
        return camera_names;
    }

    // Check number of devices found
    if(dev_list.nDeviceNum == 0){
        std::cout << "No Hikrobot devices found." << std::endl;
    }
    // Create list of camera names
    for (int i = 0; i < dev_list.nDeviceNum; i++){
        MV_CC_DEVICE_INFO* info = dev_list.pDeviceInfo[i];
        //PrintDeviceInfo(info);
        if (info == nullptr){
            camera_names.push_back("");
            continue;
        }
        if (info->nTLayerType == MV_GIGE_DEVICE){
            camera_names.push_back("Hikrobot - "+QString(reinterpret_cast<char*>(info->SpecialInfo.stGigEInfo.chSerialNumber )));
        }
        else if (info->nTLayerType == MV_USB_DEVICE){
            camera_names.push_back("Hikrobot - "+QString(reinterpret_cast<char*>(info->SpecialInfo.stUsb3VInfo.chSerialNumber )));
        }
    }
    return camera_names;
}

bool Hikrobot_Cam::init(const int &idx){

    MV_CC_DEVICE_INFO_LIST dev_list;
    memset(&dev_list, 0, sizeof(MV_CC_DEVICE_INFO_LIST));

    // Get devices list
    if (MV_CC_EnumDevices(MV_GIGE_DEVICE | MV_USB_DEVICE, &dev_list) != MV_OK){
        ERROR("Failed to get Hikrobot device list.");
        return false;
    }

    // Check number of devices found
    if(dev_list.nDeviceNum == 0){
        ERROR("No Hikrobot devices found.");
        return false;
    }


    // Open selected device
    if (MV_CC_CreateHandle(&handle_, dev_list.pDeviceInfo[idx]) != MV_OK){
        ERROR("Could not create Hikrobot camera handle.");
        return false;
    }
    if (MV_CC_OpenDevice(handle_) != MV_OK){
        ERROR("Could not open Hikrobot device!");
        return false;
    }

    info_ = dev_list.pDeviceInfo[idx];
    // Optimal network package size detection for GigE cameras
    if (info_->nTLayerType == MV_GIGE_DEVICE){
        int packet_size = MV_CC_GetOptimalPacketSize(handle_);
        if (packet_size > 0){
            if(MV_CC_SetIntValue(handle_,"GevSCPSPacketSize", packet_size) != MV_OK){
                WARN("Hikrobot GigE camera network package size could not be set.");
            }
        }
        else{
            WARN("Hikrobot GigE camera network package size could not be determined.");
        }
    }

    // Set Trigger mode and source
    if (MV_CC_SetEnumValue(handle_, "TriggerMode", 1) != MV_OK){
        ERROR("Unable to adjust Hikrobot cam trigger mode.");
        return false;
    }
    if (MV_CC_SetEnumValue(handle_, "TriggerSource", MV_TRIGGER_SOURCE_SOFTWARE) != MV_OK){
        ERROR("Unable to set Hikrobot cam trigger source.");
        return false;
    }

   // Set Acquistion Frame Rate
    // if (MV_CC_SetFloatValue(handle_, "AcquisitionFrameRate", 33.0f) != MV_OK) {
    //      std::cerr << "Unable to set Acquistion Frame Rate " << std::endl;
    //     return false;

    // };


    //Set pixel format
    if (MV_CC_SetEnumValue(handle_, "PixelFormat", PixelType_Gvsp_RGB8_Packed)  != MV_OK){

        std::cerr << "Unable to set RGB  pixel format " << std::endl;
        return false;

    }
   // int ret = MV_CC_RegisterImageCallBackForRGB(handle_, Hikrobot_Cam::getCameraImageCallback, this);



    // get IFloat variable
    MVCC_FLOATVALUE val_f = {0};
    int nRet = MV_CC_GetFloatValue(handle_, "ExposureTime", &val_f);
    if (MV_OK == nRet)
    {
        std::cout << "exposure time current value: " << val_f.fCurValue << std::endl;
        std::cout << "exposure time max value: " << val_f.fMax << std::endl;
        std::cout << "exposure time min value: " << val_f.fMin << std::endl;
    }
    else
    {
        std::cout << "get exposure time failed! nRet " << nRet << std::endl;
    }

    std::cout << "Set exposure_ mode and source" << std::endl;
    exposure_ = std::shared_ptr<Property_Spin_F>(
                new Property_Spin_F(nullptr, "Hikrobot_Exposure", "Exposure (ms)", val_f.fMin, val_f.fMax, val_f.fCurValue));
    
    MVCC_INTVALUE val_i = {0};
    nRet = MV_CC_GetIntValue(handle_, "Brightness", &val_i);
    if (nRet != MV_OK){
        std::cerr << "Unable to set brightness" << std::endl;
    }
    brightness_ = std::shared_ptr<Property_Slider>(
                new Property_Slider(nullptr, "Hikrobot_Brightness", "Brightness", val_i.nMin, val_i.nMax, val_i.nCurValue, "Reset"));

    nRet = MV_CC_GetIntValue(handle_, "Saturation", &val_i);
    if (nRet != MV_OK){
        std::cerr << "Unable to set saturation" << std::endl;
    }
    saturation_ = std::shared_ptr<Property_Slider>(
                new Property_Slider(nullptr, "Hikrobot_Saturation", "Saturation", val_i.nMin, val_i.nMax, val_i.nCurValue, "Reset"));
    
    //brightness_ = saturation_;
    nRet = MV_CC_GetIntValue(handle_, "Sharpness", &val_i);
    if (nRet != MV_OK){
        std::cerr << "Unable to set sharpness" << std::endl;
    }
    sharpness_ = std::shared_ptr<Property_Slider>(
                new Property_Slider(nullptr, "Hikrobot_Sharpness", "Sharpness", val_i.nMin, val_i.nMax, val_i.nCurValue, "Reset"));

    nRet = MV_CC_GetFloatValue(handle_, "Gain", &val_f);
    
    if (nRet != MV_OK){
        std::cerr << "Unable to set gain" << std::endl;
    }
    gain_ = std::shared_ptr<Property_Spin_F>(
                new Property_Spin_F(nullptr, "Hikrobot_Gain", "Gain", val_f.fMin, val_f.fMax, val_f.fCurValue));

    reset_to_auto_ = std::shared_ptr<Property_Button>(new Property_Button(nullptr, "Hikrobot_Auto_Mode", "Activate Auto-Mode"));

    // Connect properties
    connect(exposure_.get(), &Property_Spin_F::valueChanged, this, &Hikrobot_Cam::setPropertyState);
    connect(brightness_.get(), &Property_Slider::valueChanged, this, &Hikrobot_Cam::setPropertyState);
    connect(saturation_.get(), &Property_Slider::valueChanged, this, &Hikrobot_Cam::setPropertyState);
    connect(sharpness_.get(), &Property_Slider::valueChanged, this, &Hikrobot_Cam::setPropertyState);
    connect(gain_.get(), &Property_Spin_F::valueChanged, this, &Hikrobot_Cam::setPropertyState);
    connect(reset_to_auto_.get(), &Property_Button::valueChanged, this, &Hikrobot_Cam::setPropertyState);

    properties_ = {exposure_, brightness_, saturation_, sharpness_, gain_,
                   reset_to_auto_};

    // Connect the image loop signal and slot
    connect(this, &Hikrobot_Cam::requestNewImage,
            this, &Hikrobot_Cam::getNewImage, Qt::QueuedConnection);

    return true;
}

bool Hikrobot_Cam::start(){
    // Check if available
    if(!handle_){
        ERROR("No Azure Kinect available!");
        return false;
    }

    STATUS("Camera start requested..");

    // Start the device_
    if (MV_CC_StartGrabbing(handle_) != MV_OK){
        ERROR("Capture start failed!");
        return false;
    }

    // Get payload size - TODO: understand what is happening here
    memset(&param_, 0, sizeof(MVCC_INTVALUE));
    if (MV_CC_GetIntValue(handle_, "PayloadSize", &param_) != MV_OK){
        ERROR("Get PayloadSize failed.");
        return false;
    }

    // info_ = {0};
    // memset(&info_, 0, sizeof(MV_FRAME_OUT_INFO_EX));
    data_ = (unsigned char *)malloc(sizeof(unsigned char) * param_.nCurValue);
    if (data_ == nullptr){
        return false;
    }
    data_size_ = param_.nCurValue;



    is_capturing_ = true;
    // Start parameter initialization
    setInitialParams();

    return true;
}

bool Hikrobot_Cam::pause(){

    // Toggle the is_capturing_ variable which is used in getNewImage() to
    // decide whether a frame will be requested
    is_capturing_ = !is_capturing_;
    if(!is_capturing_){
        STATUS("Camera pause requested..");
    }
    else{
        STATUS("Camera unpause requested..");
    }

    return true;
}

bool Hikrobot_Cam::stop(){
    STATUS("Camera stop requested..");

    // Stop the device - TODO: Check whether the devices can be started again
    // after stopping it this way. Do we need to call init() again?
    is_capturing_ = false;
    if(MV_CC_StopGrabbing(handle_) != MV_OK){
        ERROR("Capture stop failed!");
        return false;
    }
    return true;
}

std::vector<Cam_Parameters> Hikrobot_Cam::getInitialParameters(){
    return {parameters_};
}

void Hikrobot_Cam::getNewImage(){

    if(!is_capturing_){
        return;
    }

    // Create timestamp
    
    long timestamp = std::chrono::duration_cast<std::chrono::milliseconds>(
                std::chrono::system_clock::now().time_since_epoch()).count();
    // Get color image
    if(MV_CC_TriggerSoftwareExecute(handle_) != MV_OK){
        ERROR("Trigger fail in Hikrobot device.");
    }

   
    MV_FRAME_OUT_INFO_EX frame_info = {0};
    memset(&frame_info, 0, sizeof(MV_FRAME_OUT_INFO_EX));
    if (MV_CC_GetOneFrameTimeout(handle_, data_, data_size_, &frame_info, 100) != MV_OK){
        ERROR("Failed to grab frame - timeout.");
    }

    long timestamp1 = std::chrono::duration_cast<std::chrono::milliseconds>(
                std::chrono::system_clock::now().time_since_epoch()).count();
  //  std::cout<<  timestamp1- timestamp<<std::endl;       
    current_rgb_frame_ = QImage(data_, frame_info.nWidth, frame_info.nHeight, QImage::Format_RGB888);

    // Signal new RGB+IR frame to calibration tool
    std::shared_ptr<const Frame> stamped_frame = std::shared_ptr<Frame>(new Frame(current_rgb_frame_, timestamp));
    emit receivedNewFrame(stamped_frame);

    // Request the next frame if capture loop is not paused
    if(is_capturing_){
        emit requestNewImage();
    }
}

void Hikrobot_Cam::setInitialParams(){

    // Get color image
    if(MV_CC_TriggerSoftwareExecute(handle_) != MV_OK){
        ERROR("Trigger fail in Hikrobot device.");
    }
    MV_FRAME_OUT_INFO_EX frame_info = {0};
    memset(&frame_info, 0, sizeof(MV_FRAME_OUT_INFO_EX));

    if (MV_CC_GetOneFrameTimeout(handle_, data_, data_size_, &frame_info, 1000) != MV_OK){
        ERROR("Failed to grab frame - timeout.");
    }

    // Set camera names
    if (info_->nTLayerType == MV_GIGE_DEVICE){
        parameters_.camera_name_ = std::string(reinterpret_cast<char*>(info_->SpecialInfo.stGigEInfo.chSerialNumber));
    }
    else if (info_->nTLayerType == MV_USB_DEVICE){
        parameters_.camera_name_ = std::string(reinterpret_cast<char*>(info_->SpecialInfo.stUsb3VInfo.chSerialNumber));
    }

    // Set resolution
    parameters_.resolution_x = frame_info.nWidth;
    parameters_.resolution_y = frame_info.nHeight;

    // Set default initial calibration parameters
    // Principal point = image center
    parameters_.cx_ = float(parameters_.resolution_x) / 2.0f;
    parameters_.cy_ = float(parameters_.resolution_y) / 2.0f;
    // Focal length is calculated using the resolution and assuming 66 deg
    // HFOV (which is the HFOV of the webcam this was tested with)
    parameters_.fx_ = parameters_.cx_ / std::tan(66.0f * M_PI / 360.0f);
    parameters_.fy_ = parameters_.fx_;
    // Set the first distortion parameter to 1. This seems to be a more stable
    // initial setup for the optimization despite giving a slightly wrong
    // distortion in the beginning.
    parameters_.k_[0] = 0.0;

    // Signal new parameters to other program parts such as the AR overlay and
    // the optimizer
    emit initialized({parameters_});

    // Signal properties to camera control widget
    emit propertiesInitialized(properties_);

    // Start capturing
    emit requestNewImage();
}

bool Hikrobot_Cam::setPropertyState(const QVariant& value){

    // Check whether camera is available
    if(handle_){

        QObject* sender = QObject::sender();
        if(sender == exposure_.get()){
            if (MV_CC_SetExposureTime(handle_, value.toFloat()) != MV_OK){
                ERROR("Unable to set exposure time");
            }
        }
        else if(sender == brightness_.get()){
            if (MV_CC_SetBrightness(handle_, value.toInt()) != MV_OK){
                ERROR("Unable to set brightness");
            }
        }
        else if(sender == saturation_.get()){
            if (MV_CC_SetSaturation(handle_, value.toInt()) != MV_OK){
                ERROR("Unable to set saturation");
            }
        }
        else if(sender == sharpness_.get()){
            if (MV_CC_SetSharpness(handle_, value.toInt()) != MV_OK){
                ERROR("Unable to set sharpness");
            }
        }
        else if(sender == gain_.get()){
            if (MV_CC_SetGain(handle_, value.toFloat()) != MV_OK){
                ERROR("Unable to set gain");
            }
        }

    }

    return false;
}

bool Hikrobot_Cam::visualizeCalibration(const std::vector<Cam_Parameters>& parameters){
    return false;
}
