#include "azure_kinect.h"
#include "UI/logger.h"

#include <fstream>
#include <QInputDialog>
#include <QProcess>
#include <QThread>

QStringList Azure_Kinect::getDeviceList(){
    QStringList camera_names;
    // Check whether devices are available
    uint32_t device_count = k4a_device_get_installed_count();

    if(device_count == 0){
        std::cout << "No Azure Kinect devices found." << std::endl;
    }

    for (uint8_t deviceIndex = 0; deviceIndex < device_count; deviceIndex++){
        // Open device
        if (K4A_RESULT_SUCCEEDED != k4a_device_open(deviceIndex, &device_)){
            std::cerr << "Failed to open device" << std::endl;;
            continue;
        }

        // Get serial
        char *serial_number = nullptr;
        size_t serial_number_length = 0;

        if (K4A_BUFFER_RESULT_TOO_SMALL != k4a_device_get_serialnum(device_, nullptr, &serial_number_length)){
            std::cerr << "Failed to get serial number length" << std::endl;;
            k4a_device_close(device_);
            device_ = nullptr;
            continue;
        }
        std::vector<char> serial(serial_number_length);
        serial_number = serial.data();

        if (K4A_BUFFER_RESULT_SUCCEEDED != k4a_device_get_serialnum(device_, serial_number, &serial_number_length)){
            std::cerr << "Failed to get serial number" << std::endl;;
            free(serial_number);
            serial_number = NULL;
            k4a_device_close(device_);
            device_ = nullptr;
            continue;
        }
        // Add serial to list
        camera_names.push_back("Azure Kinect - " + QString(serial_number));

        // Close device
        k4a_device_close(device_);
        device_ = nullptr;
    }

    return camera_names;
}

bool Azure_Kinect::init(const int &idx){

    // Open selected device
    if (K4A_RESULT_SUCCEEDED != k4a_device_open(idx, &device_)){
        printf("Failed to open device\n");
        return false;
    }

    // Get serial
    size_t serial_number_length = 0;
    if (K4A_BUFFER_RESULT_TOO_SMALL != k4a_device_get_serialnum(device_,
                                                                nullptr,
                                                                &serial_number_length)){
        ERROR("Failed to get serial number length");
        serial_ = "unknown";
    }
    else{
        serial_ = std::string(serial_number_length, '*');
        if (K4A_BUFFER_RESULT_SUCCEEDED != k4a_device_get_serialnum(device_,
                                                                    &serial_[0],
                                                                    &serial_number_length)){
            ERROR("Failed to get serial number");
            serial_ = "unknown";
        }
    }

    // Set config parameters - TODO: Add GUI options for this
    config_ = K4A_DEVICE_CONFIG_INIT_DISABLE_ALL;
    config_.camera_fps = K4A_FRAMES_PER_SECOND_15;
    config_.color_format = K4A_IMAGE_FORMAT_COLOR_BGRA32;
    config_.color_resolution = K4A_COLOR_RESOLUTION_1080P;
    config_.depth_mode = K4A_DEPTH_MODE_WFOV_UNBINNED;
    config_.synchronized_images_only = true;

    // Init properties
    exposure_ = std::shared_ptr<Property_Spin>(new Property_Spin(nullptr, "Exposure (ms)", 0, 2000000, 10000));
    brightness_ = std::shared_ptr<Property_Slider>(new Property_Slider(nullptr, "Brightness", 0, 255, 128, "Reset"));
    contrast_  = std::shared_ptr<Property_Slider>(new Property_Slider(nullptr, "Contrast", 0, 10, 5, "Reset"));
    saturation_  = std::shared_ptr<Property_Slider>(new Property_Slider(nullptr, "Saturation", 0, 64, 32, "Reset"));
    sharpness_  = std::shared_ptr<Property_Slider>(new Property_Slider(nullptr, "Sharpness", 0, 255, 2, "Reset"));
    white_balance_ = std::shared_ptr<Property_Slider>(new Property_Slider(nullptr,"White Balance", 0, 12000, 4500, "Reset"));
    gain_  = std::shared_ptr<Property_Slider>(new Property_Slider(nullptr, "Gain", 0, 255, 100, "Reset"));
    powerline_frequency_ = std::shared_ptr<Property_Selector>(new Property_Selector(nullptr, "Powerline Frequency", {"50Hz", "60Hz"}, 1));
    backlight_compensation_ = std::shared_ptr<Property_Check>(new Property_Check(nullptr, "Turn Backlight Compensation On", false,
                                                                                 true, "Turn Backlight Compensation Off"));
    reset_to_auto_ = std::shared_ptr<Property_Button>(new Property_Button(nullptr, "Activate Auto-Mode"));


    // Connect properties
    connect(exposure_.get(), &Property_Spin::valueChanged, this, &Azure_Kinect::setPropertyState);
    connect(brightness_.get(), &Property_Slider::valueChanged, this, &Azure_Kinect::setPropertyState);
    connect(contrast_.get(), &Property_Slider::valueChanged, this, &Azure_Kinect::setPropertyState);
    connect(saturation_.get(), &Property_Slider::valueChanged, this, &Azure_Kinect::setPropertyState);
    connect(sharpness_.get(), &Property_Slider::valueChanged, this, &Azure_Kinect::setPropertyState);
    connect(white_balance_.get(), &Property_Slider::valueChanged, this, &Azure_Kinect::setPropertyState);
    connect(gain_.get(), &Property_Slider::valueChanged, this, &Azure_Kinect::setPropertyState);
    connect(powerline_frequency_.get(), &Property_Selector::valueChanged, this, &Azure_Kinect::setPropertyState);
    connect(backlight_compensation_.get(), &Property_Check::valueChanged, this, &Azure_Kinect::setPropertyState);
    connect(reset_to_auto_.get(), &Property_Button::valueChanged, this, &Azure_Kinect::setPropertyState);

    properties_ = {exposure_, brightness_,
                   contrast_, saturation_,
                   sharpness_, white_balance_,
                   gain_, powerline_frequency_,
                   backlight_compensation_, reset_to_auto_};

    // Connect the image loop signal and slot
    connect(this, &Azure_Kinect::requestNewImage, this, &Azure_Kinect::getNewImage, Qt::QueuedConnection);

    return true;
}

bool Azure_Kinect::start(){
    // Check if available
    if(!device_){
        ERROR("No Azure Kinect available!");
        return false;
    }

    STATUS("Camera start requested..");

    // Start the device_
    if (K4A_RESULT_SUCCEEDED != k4a_device_start_cameras(device_, &config_)){
        ERROR("Failed to start device!");
        return false;
    }

    is_capturing_ = true;
    // Start parameter initialization
    setInitialParams();

    return true;
}

bool Azure_Kinect::pause(){

    // Toggle the is_capturing_ variable which is used in getNewImage() to
    // decide whether a frame will be requested
    is_capturing_ = !is_capturing_;
    if(!is_capturing_){
        STATUS("Camera pause requested..");
    }
    else{
        STATUS("Camera unpause requested..");
        emit requestNewImage();
    }

    return true;
}

bool Azure_Kinect::stop(){
    STATUS("Camera stop requested..");

    // Stop the device - TODO: Check whether the devices can be started again
    // after stopping it this way. Do we need to call init() again?
    is_capturing_ = false;
    k4a_device_stop_cameras(device_);

    return true;
}

std::vector<Cam_Parameters> Azure_Kinect::getInitialParameters(){
    return parameters_;
}

void Azure_Kinect::getNewImage(){

    if(!is_capturing_){
        return;
    }

    // Get a frame - timeout set to 1000ms
    switch (k4a_device_get_capture(device_, &capture_, 1000)){
    case K4A_WAIT_RESULT_SUCCEEDED:
        break;
    case K4A_WAIT_RESULT_TIMEOUT:
        WARN("Timed out waiting for a capture.");
        return;
    case K4A_WAIT_RESULT_FAILED:
        ERROR("Failed to read a capture.");
        return;
    }

    // Create timestamp
    long timestamp = std::chrono::duration_cast<std::chrono::milliseconds>(
                std::chrono::system_clock::now().time_since_epoch()).count();

    // Get color image
    image_ = k4a_capture_get_color_image(capture_);
    if (image_){
        // Convert to cv::Mat for further color conversion
        rgb_temp_ = cv::Mat (parameters_[0].resolution_y,
                parameters_[0].resolution_x, CV_8UC4,
                k4a_image_get_buffer(image_));
        cv::cvtColor(rgb_temp_, rgb_temp_, cv::COLOR_BGRA2RGB);
        // Convert to QImage
        current_rgb_frame_ = QImage(rgb_temp_.data,
                                    parameters_[0].resolution_x,
                parameters_[0].resolution_y,
                QImage::Format_RGB888).copy();
        k4a_image_release(image_);
    }

    // Get IR16 image
    image_ = k4a_capture_get_ir_image(capture_);
    if (image_){
        // Convert to cv::Mat for rescaling
        ir_temp_ = cv::Mat (parameters_[1].resolution_y,
                parameters_[1].resolution_x, CV_16U, k4a_image_get_buffer(image_));
        // Rescale brightness
        // Note: the 16 bit ir images from the Azure Kinect seem to have an
        //       approximate value range of {0,..,1600} when the calibration
        //       target and the camera are at least 50cm apart
        ir_temp_.convertTo(ir_temp_, -1, 65535.0/1600.0, 0);
        // Convert to QImage.
        current_ir_frame_ = QImage(ir_temp_.data, parameters_[1].resolution_x,
                parameters_[1].resolution_y,
                QImage::Format_Grayscale16).copy();

        k4a_image_release(image_);
    }

    // Release capture
    k4a_capture_release(capture_);

    // Signal new RGB+IR frame to calibration tool
    std::shared_ptr<const Frame> stamped_frame = std::shared_ptr<Frame>(new Frame(current_rgb_frame_, current_ir_frame_, timestamp));
    emit receivedNewFrame(stamped_frame);

    // Force a maximum of 10 FPS to reduce CPU load
    // QThread::msleep(100);

    // Request the next frame if capture loop is not paused
    if(is_capturing_){
        emit requestNewImage();
    }
}

void Azure_Kinect::setInitialParams(){

    // Set camera names
    parameters_[0].camera_name_ = "Azure Kinect RGB - "+serial_;
    parameters_[1].camera_name_ = "Azure Kinect IR - "+serial_;

    // Read device calibration parameters
    k4a_calibration_t calibration;
    if(K4A_RESULT_SUCCEEDED != k4a_device_get_calibration(device_,
                                                          config_.depth_mode,
                                                          config_.color_resolution,
                                                          &calibration)){
        ERROR("Could not read device calibration paramters.");
        return;
    }

    // Helper variables for brevity
    auto color_param = calibration.color_camera_calibration.intrinsics.parameters.param;
    auto ir_param = calibration.depth_camera_calibration.intrinsics.parameters.param;

    // Set image resolutions
    parameters_[0].resolution_x = calibration.color_camera_calibration.resolution_width;
    parameters_[0].resolution_y = calibration.color_camera_calibration.resolution_height;
    parameters_[1].resolution_x = calibration.depth_camera_calibration.resolution_width;
    parameters_[1].resolution_y = calibration.depth_camera_calibration.resolution_height;

    // RGB calibration parameters
    // Principal point = image and distortion center
    parameters_[0].cx_ = color_param.cx;
    parameters_[0].cy_ = color_param.cy;
    // Focal length
    parameters_[0].fx_ = color_param.fx;
    parameters_[0].fy_ = color_param.fy;
    // Rational distortion model parameters
    parameters_[0].k_ = {color_param.k1, color_param.k2, color_param.k3,
                         color_param.k4, color_param.k5, color_param.k6};
    parameters_[0].p1_ = color_param.p1;
    parameters_[0].p2_ = color_param.p2;

    // IR calibration parameters
    // Principal point = image and distortion center
    parameters_[1].cx_ = ir_param.cx;
    parameters_[1].cy_ = ir_param.cy;
    // Focal length
    parameters_[1].fx_ = ir_param.fx;
    parameters_[1].fy_ = ir_param.fy;
    // Rational distortion model parameters
    parameters_[1].k_ = {ir_param.k1, ir_param.k2, ir_param.k3,
                         ir_param.k4, ir_param.k5, ir_param.k6};
    parameters_[1].p1_ = ir_param.p1;
    parameters_[1].p2_ = ir_param.p2;

    // Get IR camera to RGB camera transformation
    std::vector<float> R(9);
    float* R_array = calibration.extrinsics[K4A_CALIBRATION_TYPE_DEPTH][K4A_CALIBRATION_TYPE_COLOR].rotation;
    R.assign(R_array, R_array+9);
    float* t = calibration.extrinsics[K4A_CALIBRATION_TYPE_DEPTH][K4A_CALIBRATION_TYPE_COLOR].translation;
    // Set this transformation as IR camera pose. The RGB camera is considered
    // to be the reference camera.
    parameters_[1].t_ = Eigen::Vector3d(t[0],t[1],t[2]);
    parameters_[1].R_ = Eigen::Matrix3f(R.data()).cast<double>();
    // Also set the rotation as Rodrigues vector for optimization
    Eigen::AngleAxisd R_aa;
    R_aa.fromRotationMatrix(parameters_[1].R_);
    parameters_[1].R_rod_ = R_aa.angle() * R_aa.axis();

    // Now everything is prepared for capturing
    is_ready_ = true;

    // Signal new parameters to other program parts such as the AR overlay and
    // the optimizer
    emit initialized(parameters_);

    // Signal properties to camera control widget
    emit propertiesInitialized(properties_);

    // Start capturing
    emit requestNewImage();
}

bool Azure_Kinect::setPropertyState(const QVariant& value){
    //    // debug
    //    k4a_color_control_mode_t mode;
    //    int32_t value;
    //    std::cerr << "New measurements:" << std::endl;
    //    k4a_device_get_color_control(device_,K4A_COLOR_CONTROL_EXPOSURE_TIME_ABSOLUTE, &mode, &value);
    //    std::cerr << "Exp: " << value << std::endl;
    //    k4a_device_get_color_control(device_,K4A_COLOR_CONTROL_BRIGHTNESS, &mode, &value);
    //    std::cerr << "Brightness: " << value << std::endl;
    //    k4a_device_get_color_control(device_,K4A_COLOR_CONTROL_CONTRAST, &mode, &value);
    //    std::cerr << "Contrast: " << value << std::endl;
    //    k4a_device_get_color_control(device_,K4A_COLOR_CONTROL_SATURATION, &mode, &value);
    //    std::cerr << "Saturation: " << value << std::endl;
    //    k4a_device_get_color_control(device_,K4A_COLOR_CONTROL_SHARPNESS, &mode, &value);
    //    std::cerr << "Sharpness: " << value << std::endl;
    //    k4a_device_get_color_control(device_,K4A_COLOR_CONTROL_WHITEBALANCE, &mode, &value);
    //    std::cerr << "White Balance: " << value << std::endl;
    //    k4a_device_get_color_control(device_,K4A_COLOR_CONTROL_BACKLIGHT_COMPENSATION, &mode, &value);
    //    std::cerr << "Backlight Compensation: " << value << std::endl;
    //    k4a_device_get_color_control(device_,K4A_COLOR_CONTROL_GAIN, &mode, &value);
    //    std::cerr << "Gain: " << value << std::endl;
    //    k4a_device_get_color_control(device_,K4A_COLOR_CONTROL_POWERLINE_FREQUENCY, &mode, &value);
    //    std::cerr << "Powerline Freq: " << value << std::endl;

    if(device_){

        QObject* sender = QObject::sender();
        k4a_color_control_command_t cmd;
        k4a_color_control_mode_t mode = K4A_COLOR_CONTROL_MODE_MANUAL;
        QVariant value_to_set = value;
        if(sender == exposure_.get()){
            cmd = K4A_COLOR_CONTROL_EXPOSURE_TIME_ABSOLUTE;

            if (value_to_set.toInt() < 1){
                mode = K4A_COLOR_CONTROL_MODE_AUTO;
            }

        }
        else if(sender == brightness_.get()){
            cmd = K4A_COLOR_CONTROL_BRIGHTNESS;
        }
        else if(sender == contrast_.get()){
            cmd = K4A_COLOR_CONTROL_CONTRAST;
        }
        else if(sender == saturation_.get()){
            cmd = K4A_COLOR_CONTROL_SATURATION;
        }
        else if(sender == sharpness_.get()){
            cmd = K4A_COLOR_CONTROL_SHARPNESS;
        }
        else if(sender == white_balance_.get()){
            cmd = K4A_COLOR_CONTROL_WHITEBALANCE;
        }
        else if(sender == backlight_compensation_.get()){
            cmd = K4A_COLOR_CONTROL_BACKLIGHT_COMPENSATION;
        }
        else if(sender == gain_.get()){
            cmd = K4A_COLOR_CONTROL_GAIN;
            if (value_to_set.toInt() < 1){
                mode = K4A_COLOR_CONTROL_MODE_AUTO;
            }
        }
        else if(sender == powerline_frequency_.get()){
            cmd = K4A_COLOR_CONTROL_POWERLINE_FREQUENCY;
            value_to_set = QVariant(value.toInt()+1);
        }
        else{
            mode = K4A_COLOR_CONTROL_MODE_AUTO;
            std::vector<k4a_color_control_command_t> cmds = {
                K4A_COLOR_CONTROL_EXPOSURE_TIME_ABSOLUTE,
                K4A_COLOR_CONTROL_BRIGHTNESS,
                K4A_COLOR_CONTROL_CONTRAST,
                K4A_COLOR_CONTROL_SATURATION,
                K4A_COLOR_CONTROL_SHARPNESS,
                K4A_COLOR_CONTROL_WHITEBALANCE,
                K4A_COLOR_CONTROL_BACKLIGHT_COMPENSATION,
                K4A_COLOR_CONTROL_GAIN,
                K4A_COLOR_CONTROL_POWERLINE_FREQUENCY
            };
            for(const k4a_color_control_command_t& control_cmd : cmds){
                k4a_device_set_color_control(device_, cmd , mode, 0);
            }
            return true;
        }

        return (K4A_RESULT_SUCCEEDED == k4a_device_set_color_control(device_,
                                                                     cmd ,
                                                                     mode,
                                                                     value_to_set.toInt()));

    }

    return false;
}

bool Azure_Kinect::visualizeCalibration(const std::vector<Cam_Parameters>& parameters){

    if(parameters.size() != 2){
        ERROR("For a pointcloud creation both camera parameter sets are required.");
        return false;
    }

    /// Convert parameters into k4a compatible structs
    k4a_calibration_t calibration;
    if(K4A_RESULT_SUCCEEDED != k4a_device_get_calibration(device_,
                                                          config_.depth_mode,
                                                          config_.color_resolution,
                                                          &calibration)){
        ERROR("Could not read device calibration paramters.");
        return false;
    }

    // Set new intrinsics
    for(size_t i = 0; i<2; i++){

        k4a_calibration_intrinsic_parameters_t& intr = (i==0) ? calibration.color_camera_calibration.intrinsics.parameters :
                                                                calibration.depth_camera_calibration.intrinsics.parameters;

        intr.param.cx = float(parameters[i].cx_);
        intr.param.cy = float(parameters[i].cy_);
        intr.param.fx = float(parameters[i].fx_);
        intr.param.fy = float(parameters[i].fy_);
        intr.param.p1 = float(parameters[i].p1_);
        intr.param.p2 = float(parameters[i].p2_);
        intr.param.k1 = float(parameters[i].k_[0]);
        intr.param.k2 = float(parameters[i].k_[1]);
        intr.param.k3 = float(parameters[i].k_[2]);
        intr.param.k4 = float(parameters[i].k_[3]);
        intr.param.k5 = float(parameters[i].k_[4]);
        intr.param.k6 = float(parameters[i].k_[5]);
    }

    // Set new extrinsics
    auto temp = parameters[1].R_.transpose();
    Eigen::Matrix3f R_rgb_to_ir = temp.cast<float>();
    float* R_data = R_rgb_to_ir.data();
    Eigen::Vector3f t_rgb_to_ir = - R_rgb_to_ir * parameters[1].t_.cast<float>();
    float* t_data = t_rgb_to_ir.data();
    std::copy(R_data, R_data+8,
              calibration.extrinsics[K4A_CALIBRATION_TYPE_COLOR][K4A_CALIBRATION_TYPE_DEPTH].rotation);
    std::copy(t_data, t_data+2,
              calibration.extrinsics[K4A_CALIBRATION_TYPE_COLOR][K4A_CALIBRATION_TYPE_DEPTH].translation);

    Eigen::Matrix3f R_ir_to_rgb = parameters[1].R_.cast<float>();
    R_data = R_ir_to_rgb.data();
    Eigen::Vector3f t_ir_to_rgb = parameters[1].t_.cast<float>();
    t_data = t_ir_to_rgb.data();
    std::copy(R_data, R_data+8,
              calibration.extrinsics[K4A_CALIBRATION_TYPE_DEPTH][K4A_CALIBRATION_TYPE_COLOR].rotation);
    std::copy(t_data, t_data+2,
              calibration.extrinsics[K4A_CALIBRATION_TYPE_DEPTH][K4A_CALIBRATION_TYPE_COLOR].translation);


    // Create k4a transform from calibration
    k4a_transformation_t transform = k4a_transformation_create(&calibration);

    /// Get an rgb+depth frame
    switch (k4a_device_get_capture(device_, &capture_, 1000)){
    case K4A_WAIT_RESULT_SUCCEEDED:
        break;
    case K4A_WAIT_RESULT_TIMEOUT:
        WARN("Timed out waiting for a capture.");
        return false;
    case K4A_WAIT_RESULT_FAILED:
        ERROR("Failed to read a capture.");
        return false;
    }

    // Get color and depth image
    k4a_image_t image_rgb = k4a_capture_get_color_image(capture_);
    k4a_image_t image_depth = k4a_capture_get_depth_image(capture_);

    // Release capture
    k4a_capture_release(capture_);


    /// Transform the depth image into a pointcloud

    // Create transformed depth image
    k4a_image_t image_depth_transformed = NULL;
    if (K4A_RESULT_SUCCEEDED != k4a_image_create(K4A_IMAGE_FORMAT_DEPTH16,
                                                 parameters[0].resolution_x,
                                                 parameters[0].resolution_y,
                                                 parameters[0].resolution_x * (int)sizeof(uint16_t),
                                                 &image_depth_transformed)){
        ERROR("Failed to create transformed depth image.");
        return false;
    }

    // Transform depth image into rgb frame
    if (K4A_RESULT_SUCCEEDED !=
            k4a_transformation_depth_image_to_color_camera(transform, image_depth,
                                                           image_depth_transformed))
    {
        ERROR("Failed to compute transformed depth image.");
        return false;
    }

    // For debugging: Create cv::Mat from k4a transformed
    //cv::Mat transformed_depth_image_cv(color.rows, color.cols, CV_16UC1,
    //                                   (void *)k4a_image_get_buffer(image_depth_transformed));
    //cv::imwrite("transformed.png", transformed_depth_image_cv);


    // Create pointcloud image
    k4a_image_t image_pcl = NULL;
    if (K4A_RESULT_SUCCEEDED != k4a_image_create(K4A_IMAGE_FORMAT_CUSTOM,
                                                 parameters[0].resolution_x,
                                                 parameters[0].resolution_y,
                                                 parameters[0].resolution_x * 3 * (int)sizeof(int16_t),
                                                 &image_pcl))
    {
        ERROR("Failed to create point cloud image.");
        return false;
    }

    // Create pointcloud from transformed image
    if (K4A_RESULT_SUCCEEDED !=
            k4a_transformation_depth_image_to_point_cloud(transform,
                                                          image_depth_transformed,
                                                          K4A_CALIBRATION_TYPE_COLOR,
                                                          image_pcl))
    {
        ERROR("Failed to compute point cloud.");
        return false;
    }


    /// Export to ply file based on Azure Kinect SDK transformation example

    struct Colored_Point
    {
        int16_t xyz[3];
        uint8_t rgb[3];
    };

    int16_t *cloud_image_data = (int16_t *)(void *)k4a_image_get_buffer(image_pcl);
    uint8_t *color_image_data = k4a_image_get_buffer(image_rgb);

    std::vector<Colored_Point> cloud;

#pragma omp parallel for
    for(int i = 0; i < parameters[0].resolution_x*parameters[0].resolution_y; i++){

        Colored_Point point;
        point.xyz[0] = cloud_image_data[3 * i + 0];
        point.xyz[1] = -cloud_image_data[3 * i + 1];
        point.xyz[2] = -cloud_image_data[3 * i + 2];
        if (point.xyz[2] == 0){
            continue;
        }

        point.rgb[0] = color_image_data[4 * i + 0];
        point.rgb[1] = color_image_data[4 * i + 1];
        point.rgb[2] = color_image_data[4 * i + 2];
        uint8_t alpha = color_image_data[4 * i + 3];

        if ((point.rgb[0] < 30 && point.rgb[1] < 30 && point.rgb[2] < 30) || alpha < 30){
            continue;
        }

        cloud.push_back(point);
    }


    std::string file_name = "test.ply";

    // Write header
    std::ofstream ofs(file_name);
    ofs << "ply" << std::endl;
    ofs << "format ascii 1.0" << std::endl;
    ofs << "element vertex" << " " << cloud.size() << std::endl;
    ofs << "property float x" << std::endl;
    ofs << "property float y" << std::endl;
    ofs << "property float z" << std::endl;
    ofs << "property uchar red" << std::endl;
    ofs << "property uchar green" << std::endl;
    ofs << "property uchar blue" << std::endl;
    ofs << "end_header" << std::endl;
    ofs.close();

    // Write pointcloud data
    std::stringstream ss;
#pragma omp parallel for
    for (size_t i = 0; i < cloud.size(); i++){
        // image data is BGR
        ss << (float)cloud[i].xyz[0] << " " << (float)cloud[i].xyz[1] << " " << (float)cloud[i].xyz[2];
        ss << " " << (float)cloud[i].rgb[2] << " " << (float)cloud[i].rgb[1] << " " << (float)cloud[i].rgb[0];
        ss << std::endl;
    }
    std::ofstream ofs_text(file_name, std::ios::out | std::ios::app);
    ofs_text.write(ss.str().c_str(), (std::streamsize)ss.str().length());
    STATUS("Pointcloud successfully written to file.");

    QProcess::startDetached("meshlab", {QString::fromStdString(file_name)});

    return true;
}
