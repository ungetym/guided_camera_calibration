////////////////////////////////////////////////////////////////////////////////
///                                                                          ///
///     The abstract class pattern defines the methods to be implemented     ///
///                             for every pattern                            ///
///                                                                          ///
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Common/data_types.h"
#include "UI/logger.h"

#include <QObject>

class Pattern: public QObject{
    Q_OBJECT

public:

    ///
    /// \brief patternProperties
    /// \return
    ///
    virtual std::vector<std::shared_ptr<Property>> patternProperties() = 0;

    ///
    /// \brief detectorProperties
    /// \return
    ///
    virtual std::vector<std::shared_ptr<Property>> detectorProperties() = 0;

    ///
    /// \brief getProperties
    /// \return
    ///
    virtual std::vector<std::shared_ptr<Property>> getProperties() = 0;

    ///
    /// \brief getPattern
    /// \return
    ///
    virtual QImage getPattern() = 0;

public slots:
    ///
    /// \brief createPattern
    /// \return
    ///
    virtual bool createPattern() = 0;

    ///
    /// \brief detectPattern
    /// \param frame
    /// \return
    ///
    virtual bool detectPattern(std::shared_ptr<const Frame>& frame) = 0;

    ///
    /// \brief detectPattern
    /// \param frame
    /// \param features
    /// \return
    ///
    virtual bool detectPattern(const QImage& frame, Detection features) = 0;

    ///
    /// \brief setOptimizationMode is the setter for the optimization mode which
    ///                            determines the images (rgb and/or ir) used
    ///                            for detection
    /// \param mode
    ///
    virtual void setOptimizationMode(const int& mode) = 0;

    ///
    /// \brief setProposedPose is used to receive the next pose proposal from
    ///                        the optimization, which can e.g. be used to
    ///                        properly scale the IR image brightness
    /// \param pose
    ///
    virtual void setProposedPose(const Pose& pose) = 0;

signals:

    ///
    /// \brief patternChanged
    /// \param pattern
    ///
    void patternChanged(const QImage& pattern);

    ///
    /// \brief patternPartitionChanged
    /// \param partition
    ///
    void patternPartitionChanged(const Partition& partition);

    ///
    /// \brief patternDetected
    /// \param detection
    ///
    void patternDetected(const Detection_Frame& detection);

    ///
    /// \brief log
    /// \param msg
    /// \param type
    ///
    void log(const QString& msg, const int& type);
};
