#include "checkerboard.h"

Checkerboard::Checkerboard(){
    width_ = std::shared_ptr<Property_Spin>(new Property_Spin(nullptr, "Checkerboard_Width", "Width", 3, 20, 7));
    height_ = std::shared_ptr<Property_Spin>(new Property_Spin(nullptr, "Checkerboard_Height", "Height", 3, 20, 4));
    board_border_ = std::shared_ptr<Property_Slider>(new Property_Slider(nullptr, "Checkerboard_Border_Size", "Board border size", 0, 100, 100));
    square_size_in_mm_ = std::shared_ptr<Property_Spin_F>(new Property_Spin_F(nullptr, "Checkerboard_Square_Size", "Square size (mm)", 0, 500, 43.75));
    detector_flags_ = std::shared_ptr<Property_Selector>(new Property_Selector(nullptr, "Checkerboard_Detector_Flags", "Detector Flags", {"Adaptive Thresh",
                                                                                                           "Normalize",
                                                                                                           "Adaptive Thresh + Normalize",
                                                                                                           "Adaptive Thresh + Fast Check",
                                                                                                           "Adaptive Thresh + Normalize + Fast Check",
                                                                                                           "Normalize + Fast Check"}, 4));
    refinement_ = std::shared_ptr<Property_Check>(new Property_Check(nullptr, "Checkerboard_Refinement", "Corner Refinement", refinement_enabled_));

    connect(detector_flags_.get(), &Property_Selector::valueChanged, this, &Checkerboard::detectorModeChanged);
    connect(refinement_.get(), &Property_Check::valueChanged, this, &Checkerboard::refinementModeChanged);
    connect(square_size_in_mm_.get(), &Property_Spin_F::valueEditFinished, [this](){createPattern();});

    pattern_properties_= {width_, height_, board_border_, square_size_in_mm_};
    detector_properties_ = {square_size_in_mm_, detector_flags_, refinement_};

    all_properties_ = {width_, height_, board_border_, square_size_in_mm_,
                       square_size_in_mm_, detector_flags_, refinement_};
}

std::vector<std::shared_ptr<Property>> Checkerboard::patternProperties(){
    return pattern_properties_;
}

std::vector<std::shared_ptr<Property>> Checkerboard::detectorProperties(){
    return detector_properties_;
}

std::vector<std::shared_ptr<Property>> Checkerboard::getProperties(){
    return all_properties_;
}

QImage Checkerboard::getPattern(){
    return min_pattern_qimg_;
}

bool Checkerboard::createPattern(){

    // Get current values from property widgets
    int width = width_->value() + 1;
    int height = height_->value() + 1;
    int px_border = int(0.1 * float(board_border_->value()));
    float square_size_in_mm = square_size_in_mm_->value();


    // Calculate optimal draw size to prevent interpolation which results in blurred edges
    int px_per_square = 10;

    int draw_width = width * px_per_square + 2 * px_border;
    int draw_height = height * px_per_square + 2 * px_border;

    // Draw board to image
    cv::Mat partition_img = cv::Mat::zeros(height, width, CV_8UC1);
    for(int row = 0; row < height; row++){
        for(int col = 0; col < width; col++){
            if( (col % 2 + row % 2) % 2 == 0 ){
                partition_img.at<uint8_t>(row,col) = 255;
            }
        }
    }
    // Upscaling
    cv::resize(partition_img, partition_img, cv::Size(width * px_per_square, height * px_per_square), 0, 0, cv::INTER_NEAREST);
    STATUS("Checkerboard successfully generated.");

    min_pattern_img_ = cv::Mat(draw_height, draw_width, CV_8UC1, 255);
    partition_img.copyTo(min_pattern_img_(cv::Rect(px_border, px_border,
                                                   partition_img.cols,
                                                   partition_img.rows)));

    // Emit new pattern as QImage
    QImage::Format format = QImage::Format_Grayscale8;
    min_pattern_qimg_ = QImage(min_pattern_img_.data, min_pattern_img_.cols, min_pattern_img_.rows, min_pattern_img_.step, format);
    emit patternChanged(min_pattern_qimg_);

    // Calculate partition
    partition_.clear();
    int id;
    for (size_t y = 0; y < height+1; y++){
        id = (y-1)*(width-1);
        for (size_t x = 0; x < width+1; x++){
            partition_.corners_.push_back(square_size_in_mm * Eigen::Vector3d(double(x),double(y),0.0));
            if((y > 0) && (y < height) && (x > 0) && (x < width)){
                partition_.id_corners_.push_back(Idx_ID(partition_.corners_.size()-1, id));
                id++;
                if((y == 1 || y == height-1) && (x == 1 || x == width - 1)){
                    partition_.outer_id_corners_.push_back(partition_.id_corners_.back());
                }
                float x_mid = std::abs(float(x)-float(width)/2.0f);
                float y_mid = std::abs(float(y)-float(height)/2.0f);
                if(y_mid > 0.4f && y_mid < 1.1f && x_mid > 0.4f && x_mid < 1.1f){
                    partition_.inner_id_corners_.push_back(partition_.id_corners_.back());
                }
            }
            if((y == 0 || y == height) && (x == 0 || x == width)){
                partition_.outer_board_corners_.push_back(partition_.corners_.back());
            }
        }
    }

    partition_.inner_corner_distances_.push_back(partition_.inner_id_corners_[1].id_-partition_.inner_id_corners_[0].id_);
    partition_.inner_corner_distances_.push_back((partition_.inner_id_corners_[2].id_-partition_.inner_id_corners_[0].id_)/(width-1));

    // Calculate board center
    partition_.center_ = Eigen::Vector3d(0,0,0);
    for(const Eigen::Vector3d& outer_corner: partition_.outer_board_corners_){
        partition_.center_ += outer_corner;
    }
    partition_.center_ /= double(partition_.outer_board_corners_.size());

    // Center 3D corners
    for(Eigen::Vector3d& corner : partition_.corners_){
        corner -= partition_.center_;
    }
    for(Eigen::Vector3d& corner : partition_.outer_board_corners_){
        corner -= partition_.center_;
    }
    partition_.center_ = Eigen::Vector3d(0.0,0.0,0.0);

    // Calculate board axes for visualization
    double axes_size = double(square_size_in_mm) * double(std::min(width, height)) / 3.0;
    partition_.axes_.push_back(Eigen::Vector3d(axes_size, 0.0, 0.0));
    partition_.axes_.push_back(Eigen::Vector3d(0.0, axes_size, 0.0));
    partition_.axes_.push_back(Eigen::Vector3d(0.0, 0.0, -axes_size));

    // Calculate board size in mm
    partition_.width_in_mm_ = square_size_in_mm * (double(width) + 2.0 * double(px_border)/double(px_per_square));
    partition_.height_in_mm_ = partition_.width_in_mm_ * double(height) / double(width);

    // Calculate minimum size of squares in image to be detected (20px per square)
    partition_.min_mm_per_pixel_ = square_size_in_mm / 20.0f;

    // Create partitioned overlay images
    cv::cvtColor(partition_img, partition_img, cv::COLOR_GRAY2RGBA);
    cv::Mat partition_img_accepted = partition_img.clone();
    format = QImage::Format_RGBA8888;

    uint8_t * img_data = (uint8_t*)partition_img.data;
    cv::MatStep step = partition_img.step;
    int idx;
    for (int row = 0; row < partition_img.rows; row++){
        for (int col = 0; col < partition_img.cols; col++){
            idx = col * 4 + row * step;
            img_data[idx + 1] = 165;
            img_data[idx + 2] = 0;
            img_data[idx + 3] = img_data[idx];
        }
    }

    // Create a copied image that is differently colored
    uint8_t * img_data_accepted = (uint8_t*)partition_img_accepted.data;
    for (int row = 0; row < partition_img_accepted.rows; row++){
        for (int col = 0; col < partition_img_accepted.cols; col++){
            idx = col * 4 + row * step;
            img_data_accepted[idx] = 0;
            img_data_accepted[idx + 2] = 0;
            img_data_accepted[idx + 3] = img_data_accepted[idx + 1];
        }
    }


    for (size_t x = 0; x < width; x++){
        for (size_t y = 0; y < height; y++){
            size_t upper_left = y * (width+1) + x;
            size_t upper_right = upper_left + 1;
            size_t bottom_left = upper_left + (width+1);
            size_t bottom_right = bottom_left + 1;
            partition_.tile_corners_.push_back(Idx_Tuple(upper_left, upper_right, bottom_left, bottom_right));

            partition_.tiles_.push_back(
                        std::make_shared<cv::Mat>(
                            partition_img(
                                cv::Range(y * px_per_square,
                                          (y+1) * px_per_square),
                                cv::Range(x * px_per_square,
                                          (x+1) * px_per_square))));

            partition_.tiles_accepted_.push_back(
                        std::make_shared<cv::Mat>(
                            partition_img_accepted(
                                cv::Range(y * px_per_square,
                                          (y+1) * px_per_square),
                                cv::Range(x * px_per_square,
                                          (x+1) * px_per_square))));
        }
    }

    checkerboard_created_ = true;
    emit patternPartitionChanged(partition_);

    return true;
}

bool Checkerboard::detectPattern(std::shared_ptr<const Frame> &frame){
    // Copy data to prevent it being overwritten (TODO: find out, why this is
    // necessary!)
    long timestamp = frame->timestamp_;
    QImage frame_rgb_copy = frame->rgb_.copy();
    QImage frame_ir_copy = frame->ir_.copy();

    if(!checkerboard_created_){
        frame.reset();
        return false;
    }

    // Create timestamp
    long now = std::chrono::duration_cast<std::chrono::milliseconds>(
                std::chrono::system_clock::now().time_since_epoch()).count();

    // Limit detection frequency to 5 FPS in order to reduce CPU load in case of
    // high frame rates
   // std::cout<< timestamp - last_timestamp_<<  " "<< now - timestamp<< std::endl;
    if(timestamp - last_timestamp_ > 200 && now - timestamp < 150){
        last_timestamp_ = timestamp;
    }
    else{
        frame.reset();
        return false;
    }

    Detection detection_rgb = std::shared_ptr<std::vector<Feature_Point>>(new std::vector<Feature_Point>());
    Detection detection_ir = std::shared_ptr<std::vector<Feature_Point>>(new std::vector<Feature_Point>());


    if(!frame->is_stereo_){// Only one video feed available - no stereo camera
        if(!detectPattern(frame_rgb_copy, detection_rgb)){
            return false;
        }
        emit patternDetected(Detection_Frame(frame, {detection_rgb}));
        return true;
    }
    else if(mode_ == OPT_MODE_RGB){// RGB optimization
        if(!detectPattern(frame_rgb_copy, detection_rgb)){
            frame.reset();
            return false;
        }
        else{
            detectPattern(frame_ir_copy, detection_ir);
            emit patternDetected(Detection_Frame(frame, {detection_rgb, detection_ir}));
            return true;
        }
    }
    else if(mode_ == OPT_MODE_IR){// IR optimization
        if(!detectPattern(frame_ir_copy, detection_ir)){
            frame.reset();
            return false;
        }
        else{
            detectPattern(frame_rgb_copy, detection_rgb);
            emit patternDetected(Detection_Frame(frame,{detection_rgb, detection_ir}));
            return true;
        }
    }
    else{// Transformation optimization or overall optimization - pattern should be detected in both images
        if(!detectPattern(frame_ir_copy, detection_ir) || !detectPattern(frame_rgb_copy, detection_rgb)){
            frame.reset();
            return false;
        }

        emit patternDetected(Detection_Frame(frame,{detection_rgb, detection_ir}));
        return true;
    }
    frame.reset();
    return false;
}

bool Checkerboard::detectPattern(const QImage& frame, Detection detection){

    detection->clear();
    int width = width_->value();
    int height = height_->value();
    float square_size_in_mm = square_size_in_mm_->value();

    // Convert image to cv::Mat
    cv::Mat image;
    if(frame.format() == QImage::Format_Grayscale16){
        // IR image needs to be scaled according to pattern distance due to IR
        // light falloff
        image = cv::Mat(frame.height(), frame.width(), CV_16U,
                        const_cast<uchar*>(frame.bits()), size_t(frame.bytesPerLine()));
        image.convertTo(image, CV_8UC3, ir_scale_ / 257.0, 0);
    }
    else{
        QImage frame_rgb = frame.convertedTo(QImage::Format_RGB888);
        image = cv::Mat(frame_rgb.height(), frame_rgb.width(), CV_8UC3,
                        const_cast<uchar*>(frame_rgb.bits()), size_t(frame_rgb.bytesPerLine()));
        cv::cvtColor(image, image, cv::COLOR_RGB2BGR);
    }

    cv::Mat image_copy = image.clone();
    cv::Mat image_copy_2 = image_copy.clone();

    // Find checkerboard
    std::vector<cv::Point2f> corners;
    cv::findChessboardCorners(image_copy, cv::Size(width, height), corners, cv::CALIB_CB_ACCURACY + current_flags_);

    // Function for calculating the metric image coordinates of a corner
    auto checkerboardIdToImageCoordinates = [](const int& width, const int& id, const double& scale){
        double x = double(id % width);
        double y = double(int(double(id) / double(width)));
        return cv::Vec3d(scale * x, scale * y, 0.0);
    };
    // Find associated ids
    if(corners.size() > 0){
        if(refinement_enabled_){
            // Corner Refinement
            cv::Mat image_gray;
            cv::cvtColor(image_copy_2, image_gray, cv::COLOR_BGR2GRAY);
            cv::cornerSubPix(image_gray, corners, cv::Size(7,7), cv::Size(-1, -1),
                             cv::TermCriteria( cv::TermCriteria::EPS + cv::TermCriteria::COUNT, 30, 0.0001 ));
        }
        for(size_t id = 0; id < corners.size(); id++){
            detection->push_back(Feature_Point(cv::Vec2d(corners[id].x, corners[id].y), id,
                                               checkerboardIdToImageCoordinates(width, id, square_size_in_mm)));
        }
        return true;
    }
    return false;
}

void Checkerboard::setProposedPose(const Pose& pose){
    // Calculate IR brightness scaling based on pattern distance and rotation

    // Get pattern distance and rotation
    double dist = pose.getDistance() / 1000.0;
    double angle = pose.getAngleDegree();

    // Assume simple quadratic falloff with increasing distance
    double scaling = dist * dist;

    // When rotated, a larger part of the IR light is reflected off the pattern
    // away from the camera thus decreasing the brightness

    scaling *= (1.0 + angle/90.0);

    ir_scale_ = float(scaling);
}

void Checkerboard::detectorModeChanged(const int& mode){
    if(mode == 0){
        current_flags_ = cv::CALIB_CB_ADAPTIVE_THRESH;
    }
    else if(mode == 1){
        current_flags_ = cv::CALIB_CB_NORMALIZE_IMAGE;
    }
    else if(mode == 2){
        current_flags_ = cv::CALIB_CB_ADAPTIVE_THRESH + cv::CALIB_CB_NORMALIZE_IMAGE;
    }
    else if(mode == 3){
        current_flags_ = cv::CALIB_CB_ADAPTIVE_THRESH + cv::CALIB_CB_FAST_CHECK;
    }
    else if(mode == 4){
        current_flags_ = cv::CALIB_CB_ADAPTIVE_THRESH + cv::CALIB_CB_NORMALIZE_IMAGE + cv::CALIB_CB_FAST_CHECK;
    }
    else if(mode == 5){
        current_flags_ = cv::CALIB_CB_NORMALIZE_IMAGE + cv::CALIB_CB_FAST_CHECK;
    }
}

void Checkerboard::refinementModeChanged(const bool& enabled){
    refinement_enabled_ = enabled;
}
