#include "charuco.h"

ChArUco::ChArUco(){
    width_ = std::shared_ptr<Property_Spin>(new Property_Spin(nullptr, "ChArUco_Width", "Width", 3, 20, 8));
    height_ = std::shared_ptr<Property_Spin>(new Property_Spin(nullptr, "ChArUco_Height", "Height", 3, 20, 5));
    board_border_ = std::shared_ptr<Property_Slider>(new Property_Slider(nullptr, "ChArUco_Border_Size", "Board border size", 0, 100, 50));
    marker_size_ = std::shared_ptr<Property_Selector>(new Property_Selector(nullptr, "ChArUco_Marker_Size", "Marker size", marker_size_list_, 0));
    marker_border_size_ = std::shared_ptr<Property_Spin>(new Property_Spin(nullptr, "ChArUco_Marker_Border_Size", "Marker Border Size (px)", 0, 100, 1));
    marker_square_ratio_ = std::shared_ptr<Property_Spin_F>(new Property_Spin_F(nullptr, "ChArUco_Marker_Square_Ratio", "Marker/Square Ratio", 0, 1, 0.5));
    square_size_in_mm_ = std::shared_ptr<Property_Spin_F>(new Property_Spin_F(nullptr, "ChArUco_Square_Size", "Square size (mm)", 0, 500, 33));
    refinement_method_ = std::shared_ptr<Property_Selector>(new Property_Selector(nullptr, "ChArUco_Refinement_Method", "Refinement Method", {"None",
                                                                                                              "Subpixel",
                                                                                                              "Contour"}, 1));

    connect(refinement_method_.get(), &Property_Selector::valueChanged, this, &ChArUco::refinementMethodChanged);

    pattern_properties_= {width_, height_, board_border_, marker_size_, marker_border_size_, marker_square_ratio_};
    detector_properties_ = {square_size_in_mm_, refinement_method_};

    all_properties_ = {width_, height_, board_border_, marker_size_,
                       marker_border_size_, marker_square_ratio_,
                       square_size_in_mm_, refinement_method_};
}

std::vector<std::shared_ptr<Property>> ChArUco::patternProperties(){
    return pattern_properties_;
}

std::vector<std::shared_ptr<Property>> ChArUco::detectorProperties(){
    return detector_properties_;
}

std::vector<std::shared_ptr<Property>> ChArUco::getProperties(){
    return all_properties_;
}

QImage ChArUco::getPattern(){
    return min_pattern_qimg_;
}

bool ChArUco::createPattern(){

    // Get current values from property widgets
    int width = width_->value();
    int height = height_->value();
    float board_border = float(board_border_->value()) * 0.01;
    int aruco_code_idx = 4 * marker_size_->index();
    int marker_border_size = marker_border_size_->value();
    float marker_square_ratio = marker_square_ratio_->value();
    float square_size_in_mm = square_size_in_mm_->value();

    // Select dictionary size and create dictionary
    for(const int& dict_size : dict_size_list_){
        if(dict_size > (width * height + 1) / 2){
            try{
                dict_ = cv::aruco::getPredefinedDictionary(ARUCO_CODES[aruco_code_idx]);
                break;
            }
            catch(...){
                ERROR("Unable to create ArUco dictionary. OpenCV Error!");
                return false;
            }
        }
        aruco_code_idx++;
    }

    // Check dictionary
    if(!dict_){
        ERROR("Unable to create ArUco dictionary of sufficient size. Please select a smaller board size.");
        return false;
    }

    // Create board
    try{
        board_ = cv::aruco::CharucoBoard::create(width, height, square_size_in_mm, marker_square_ratio * square_size_in_mm, dict_);
    }
    catch(...){
        ERROR("Unable to create ChArUco board. OpenCV Error!");
        return false;
    }

    // Check board
    if(!board_){
        ERROR("Unable to create ChArUco board.");
        return false;
    }

    // Set detector parameters
    try{
        detector_params_ = cv::aruco::DetectorParameters::create();
        detector_params_->minMarkerPerimeterRate = 0.005;
        detector_params_->adaptiveThreshConstant = 5;
        detector_params_->adaptiveThreshWinSizeMax = 150;
        detector_params_->cornerRefinementMethod = cv::aruco::CORNER_REFINE_SUBPIX;
        detector_params_->minMarkerDistanceRate = 0.001;
    }
    catch(...){
        ERROR("Unable to create ChArUco detector parameters. OpenCV Error!");
        return false;
    }

    // Calculate optimal draw size to prevent interpolation which results in blurred edges
    int marker_size_px = 4+marker_size_->index();
    float px_per_square = 1.0/marker_square_ratio * float(marker_size_px + 2 * marker_border_size);
    float px_border = px_per_square * board_border;

    while((px_per_square - float(int(px_per_square)) > 0) && (px_per_square < 500) && (px_border-float(int(px_border))>0)){
        px_per_square*=10.0;
        px_border*=10.0;
    }

    int draw_width = width * px_per_square + 2 * int(px_border);
    int draw_height = height * px_per_square + 2 * int(px_border);

    // Draw board to image
    cv::Mat partition_img;
    try{
        board_->draw(cv::Size(draw_width, draw_height), min_pattern_img_, int(px_border), marker_border_size);
        // additional drawing without border for AR partition
        board_->draw(cv::Size(width * px_per_square, height * px_per_square), partition_img, 0, marker_border_size);
    }
    catch(...){
        ERROR("Unable to draw ChArUco marker board.");
        return false;
    }

    // Check drawn image
    if(min_pattern_img_.rows > 0 && min_pattern_img_.cols > 0 && partition_img.rows > 0 && partition_img.cols > 0){
        STATUS("ChArUco board successfully generated.");
    }
    else{
        ERROR("No pattern generated.");
        return false;
    }

    // Emit new pattern as QImage
    QImage::Format format = QImage::Format_Grayscale8;
    min_pattern_qimg_ = QImage(min_pattern_img_.data, min_pattern_img_.cols, min_pattern_img_.rows, min_pattern_img_.step, format);
    emit patternChanged(min_pattern_qimg_);

    // Calculate partition
    partition_.clear();
    int id;
    for (size_t y = 0; y < height+1; y++){
        id = (y-1)*(width-1);
        for (size_t x = 0; x < width+1; x++){
            partition_.corners_.push_back(square_size_in_mm * Eigen::Vector3d(double(x),double(y),0.0));
            if((y > 0) && (y < height) && (x > 0) && (x < width)){
                partition_.id_corners_.push_back(Idx_ID(partition_.corners_.size()-1, id));
                id++;
                if((y == 1 || y == height-1) && (x == 1 || x == width - 1)){
                    partition_.outer_id_corners_.push_back(partition_.id_corners_.back());
                }
                float x_mid = std::abs(float(x)-float(width)/2.0f);
                float y_mid = std::abs(float(y)-float(height)/2.0f);
                if(y_mid > 0.4f && y_mid < 1.1f && x_mid > 0.4f && x_mid < 1.1f){
                    partition_.inner_id_corners_.push_back(partition_.id_corners_.back());
                }
            }
            if((y == 0 || y == height) && (x == 0 || x == width)){
                partition_.outer_board_corners_.push_back(partition_.corners_.back());
            }
        }
    }

    partition_.inner_corner_distances_.push_back(partition_.inner_id_corners_[1].id_-partition_.inner_id_corners_[0].id_);
    partition_.inner_corner_distances_.push_back((partition_.inner_id_corners_[2].id_-partition_.inner_id_corners_[0].id_)/(width-1));

    // Calculate board center
    partition_.center_ = Eigen::Vector3d(0,0,0);
    for(const Eigen::Vector3d& outer_corner: partition_.outer_board_corners_){
        partition_.center_ += outer_corner;
    }
    partition_.center_ /= double(partition_.outer_board_corners_.size());

    // Center 3D corners
    for(Eigen::Vector3d& corner : partition_.corners_){
        corner -= partition_.center_;
    }
    for(Eigen::Vector3d& corner : partition_.outer_board_corners_){
        corner -= partition_.center_;
    }
    partition_.center_ = Eigen::Vector3d(0.0,0.0,0.0);

    // Calculate board axes for visualization
    double axes_size = double(square_size_in_mm) * double(std::min(width, height)) / 3.0;
    partition_.axes_.push_back(Eigen::Vector3d(axes_size, 0.0, 0.0));
    partition_.axes_.push_back(Eigen::Vector3d(0.0, axes_size, 0.0));
    partition_.axes_.push_back(Eigen::Vector3d(0.0, 0.0, -axes_size));

    // Calculate board size in mm
    partition_.width_in_mm_ = square_size_in_mm * (double(width) + 2.0 * double(px_border/px_per_square));
    partition_.height_in_mm_ = partition_.width_in_mm_ * double(height) / double(width);

    // Calculate minimum size ratio of marker-mm in pixels to be detected
    partition_.min_mm_per_pixel_ = square_size_in_mm * marker_square_ratio
            / (6.0 * float(marker_size_px + 2*marker_border_size));

    int square_size_in_px = partition_img.cols/width;
    cv::cvtColor(partition_img, partition_img, cv::COLOR_GRAY2RGBA);
    cv::Mat partition_img_accepted = partition_img.clone();
    format = QImage::Format_RGBA8888;

    uint8_t * img_data = (uint8_t*)partition_img.data;
    cv::MatStep step = partition_img.step;
    int idx;
    for (int row = 0; row < partition_img.rows; row++){
        for (int col = 0; col < partition_img.cols; col++){
            idx = col * 4 + row * step;
            img_data[idx + 1] = 165;
            img_data[idx + 2] = 0;
            img_data[idx + 3] = img_data[idx];
        }
    }

    // Create a copied image that is differently colored
    uint8_t * img_data_accepted = (uint8_t*)partition_img_accepted.data;
    for (int row = 0; row < partition_img_accepted.rows; row++){
        for (int col = 0; col < partition_img_accepted.cols; col++){
            idx = col * 4 + row * step;
            img_data_accepted[idx] = 0;
            img_data_accepted[idx + 2] = 0;
            img_data_accepted[idx + 3] = img_data_accepted[idx + 1];
        }
    }


    for (size_t x = 0; x < width; x++){
        for (size_t y = 0; y < height; y++){
            size_t upper_left = y * (width+1) + x;
            size_t upper_right = upper_left + 1;
            size_t bottom_left = upper_left + (width+1);
            size_t bottom_right = bottom_left + 1;
            partition_.tile_corners_.push_back(Idx_Tuple(upper_left, upper_right, bottom_left, bottom_right));

            partition_.tiles_.push_back(
                        std::make_shared<cv::Mat>(
                            partition_img(
                                cv::Range(y * square_size_in_px,
                                          (y+1) * square_size_in_px),
                                cv::Range(x * square_size_in_px,
                                          (x+1) * square_size_in_px))));

            partition_.tiles_accepted_.push_back(
                        std::make_shared<cv::Mat>(
                            partition_img_accepted(
                                cv::Range(y * square_size_in_px,
                                          (y+1) * square_size_in_px),
                                cv::Range(x * square_size_in_px,
                                          (x+1) * square_size_in_px))));
        }
    }

    arucoboard_created_ = true;
    emit patternPartitionChanged(partition_);

    return true;
}

bool ChArUco::detectPattern(std::shared_ptr<const Frame>& frame){

    // Copy data to prevent it being overwritten (TODO: find out, why this is
    // necessary!)
    long timestamp = frame->timestamp_;
    QImage frame_rgb_copy = frame->rgb_.copy();
    QImage frame_ir_copy = frame->ir_.copy();

    if(!arucoboard_created_){
        frame.reset();
        return false;
    }

    // Create timestamp
    long now = std::chrono::duration_cast<std::chrono::milliseconds>(
                std::chrono::system_clock::now().time_since_epoch()).count();

    // Limit detection frequency to 5 FPS in order to reduce CPU load in case of
    // high frame rates
   // std::cout<< timestamp - last_timestamp_<<  " "<< now - timestamp<< std::endl;
    if(timestamp - last_timestamp_ > 200 && now - timestamp < 150){
        last_timestamp_ = timestamp;
    }
    else{
        frame.reset();
        return false;
    }

    Detection detection_rgb = std::shared_ptr<std::vector<Feature_Point>>(new std::vector<Feature_Point>());
    Detection detection_ir = std::shared_ptr<std::vector<Feature_Point>>(new std::vector<Feature_Point>());


    if(!frame->is_stereo_){// Only one video feed available - no stereo camera
        if(!detectPattern(frame_rgb_copy, detection_rgb)){
            return false;
        }
        emit patternDetected(Detection_Frame(frame, {detection_rgb}));
        return true;
    }
    else if(mode_ == OPT_MODE_RGB){// RGB optimization
        if(!detectPattern(frame_rgb_copy, detection_rgb)){
            frame.reset();
            return false;
        }
        else{
            detectPattern(frame_ir_copy, detection_ir);
            emit patternDetected(Detection_Frame(frame, {detection_rgb, detection_ir}));
            return true;
        }
    }
    else if(mode_ == OPT_MODE_IR){// IR optimization
        if(!detectPattern(frame_ir_copy, detection_ir)){
            frame.reset();
            return false;
        }
        else{
            detectPattern(frame_rgb_copy, detection_rgb);
            emit patternDetected(Detection_Frame(frame,{detection_rgb, detection_ir}));
            return true;
        }
    }
    else{// Transformation optimization or overall optimization - pattern should be detected in both images
        if(!detectPattern(frame_ir_copy, detection_ir) || !detectPattern(frame_rgb_copy, detection_rgb)){
            frame.reset();
            return false;
        }

        emit patternDetected(Detection_Frame(frame,{detection_rgb, detection_ir}));
        return true;
    }
    frame.reset();
    return false;
}

bool ChArUco::detectPattern(const QImage& frame, Detection detection){

    // Check if necessary structs have been loaded
    if(!dict_ || !board_ || !detector_params_){
        return false;
    }

    detection->clear();
    int width = width_->value();
    float square_size_in_mm = square_size_in_mm_->value();

    // Convert image to cv::Mat
    cv::Mat image;
    if(frame.format() == QImage::Format_Grayscale16){
        // IR image needs to be scaled according to pattern distance due to IR
        // light falloff
        QImage frame_ir = frame.copy();
        image = cv::Mat(frame_ir.height(), frame_ir.width(), CV_16U,
                        const_cast<uchar*>(frame_ir.bits()), size_t(frame_ir.bytesPerLine()));
        image.convertTo(image, CV_8UC3, ir_scale_ / 257.0, 0);
    }
    else{
        QImage frame_rgb = frame.convertedTo(QImage::Format_RGB888).copy();
        image = cv::Mat(frame_rgb.height(), frame_rgb.width(), CV_8UC3,
                        const_cast<uchar*>(frame_rgb.bits()), size_t(frame_rgb.bytesPerLine()));
        cv::cvtColor(image, image, cv::COLOR_RGB2BGR);
    }

    // TODO: Find reason for image memory's strange behaviour
    cv::Mat image_copy = image.clone();

//        // Debug output
//        if(frame.format() != QImage::Format_Grayscale16){
//            cv::imshow("image_copy", image_copy);
//        }

    //find charuco markers
    std::vector<std::vector<cv::Point2f>> marker_corners;
    std::vector<int> marker_ids;
    cv::aruco::detectMarkers(image_copy, dict_, marker_corners, marker_ids, detector_params_);

    //std::vector<std::vector<cv::Point2f>> rejected_corners;
    //cv::aruco::refineDetectedMarkers(image, board_, marker_corners, marker_ids, rejected_corners);

    //function for calculating the image coordinates of a charuco corner
    auto charucoIdToImageCoordinates = [](const int& width, const int& id, const double& scale){
        double x = double(id % (width-1));
        double y = double(int(double(id) / double(width-1)));
        return cv::Vec3d(scale * x, scale * y, 0.0);
    };

    //find associated corners
    if(marker_ids.size() > 0){
        std::vector<cv::Vec2f> corners;
        std::vector<int> ids;
        cv::aruco::interpolateCornersCharuco(marker_corners, marker_ids,
                                             image_copy, board_, corners, ids);

        for(size_t i = 0; i<ids.size(); i++){
            cv::Vec2d corner(corners[i]);
            detection->push_back(Feature_Point(corner, ids[i], charucoIdToImageCoordinates(width, ids[i], square_size_in_mm)));
        }

        return true;
    }

    return false;
}

void ChArUco::setProposedPose(const Pose& pose){
    // calculate IR brightness scaling based on pattern distance and rotation

    // get pattern distance and rotation
    double dist = pose.getDistance() / 1000.0;
    double angle = pose.getAngleDegree();

    // assume simple quadratic falloff with increasing distance
    double scaling = dist * dist;

    // when rotated, a larger part of the IR light is reflected off the pattern
    // away from the camera thus decreasing the brightness

    scaling *= (1.0 + angle/90.0);

    ir_scale_ = float(scaling);
}

void ChArUco::refinementMethodChanged(const int& method){
    if(method == 0){
        detector_params_->cornerRefinementMethod = cv::aruco::CORNER_REFINE_NONE;
    }
    if(method == 1){
        detector_params_->cornerRefinementMethod = cv::aruco::CORNER_REFINE_SUBPIX;
    }
    if(method == 2){
        detector_params_->cornerRefinementMethod = cv::aruco::CORNER_REFINE_CONTOUR;
    }
}
