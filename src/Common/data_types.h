////////////////////////////////////////////////////////////////////////////////
///                                                                          ///
///     The data_types header defines various data holding classes used      ///
///                     by multiple program parts                            ///
///                                                                          ///
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "UI/logger.h"

#include <eigen3/Eigen/Eigen>
#include <eigen3/Eigen/Geometry>
#include <limits>
#include <opencv4/opencv2/opencv.hpp>
#include <QCheckBox>
#include <QComboBox>
#include <QDoubleSpinBox>
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QSlider>
#include <QSpinBox>
#include <QVideoFrame>
#include <QWidget>
#include <random>

/////////////////////////////// Camera related /////////////////////////////////

///
/// \brief The Frame class represents a single camera frame. In the case of a
///                        Azure Kinect, this includes an IR frame.
///
class Frame{

public:

    ///
    /// \brief Frame        is the default constructor
    ///
    Frame(){
    }

    ///
    /// \brief Frame        constructor for a single RGB frame
    /// \param rgb          RGB image
    /// \param timestamp    of capture
    ///
    Frame(const QImage& rgb, const long& timestamp = 0){
        rgb_ = rgb.copy();
        timestamp_ = timestamp;
    }

    ///
    /// \brief Frame        constructor for a single RGB frame from a QVideoSink
    /// \param rgb          RGB image
    /// \param timestamp    of capture
    ///
    Frame(const QVideoFrame& rgb, const long& timestamp = 0){
        rgb_ = rgb.toImage().copy();
        timestamp_ = timestamp;
    }

    ///
    /// \brief Frame        constructor for RGB/IR framees
    /// \param rgb          RGB image
    /// \param ir           IR image
    /// \param timestamp    of capture
    ///
    Frame(const QImage& rgb, const QImage& ir, const long& timestamp = 0){
        is_stereo_= true;
        rgb_ = rgb.copy();
        ir_ = ir;
        timestamp_ = timestamp;
    }

public:

    /// To check, whether this is an RGB/IR or an RGB frame, is_stereo_ is used
    bool is_stereo_ = false;
    /// RGB image
    QImage rgb_;
    /// Infrared image
    QImage ir_;
    /// Timestamp in ms (e.g. according to std::chrono)
    long timestamp_;
};

////////////////////////////// Property Widgets ////////////////////////////////

/// Property type flags
const int PROP_SLIDER_INT = 0;
const int PROP_SPIN_INT = 1;
const int PROP_SPIN_DOUBLE = 2;
const int PROP_SELECTOR = 3;
const int PROP_CHECK = 4;
const int PROP_BUTTON = 5;

class Property : public QWidget{

    Q_OBJECT

public:

    Property(QWidget* parent, const QString& name, const int& property_type) :
        QWidget(parent){

        name_ = name;
        property_type_ = property_type;
    }

    ///
    /// \brief name
    /// \return
    ///
    QString name(){return name_;}

    ///
    /// \brief type
    /// \return
    ///
    int type(){return property_type_;}

    ///
    /// \brief setValue
    /// \param val
    ///
    virtual void setValue(const QVariant& val) = 0;

private:
    QString name_;
    int property_type_;
};

class Property_Slider : public Property{

    Q_OBJECT

public:
    ///
    /// \brief Property_Slider
    /// \param parent
    /// \param label
    /// \param min
    /// \param max
    /// \param current
    ///
    Property_Slider(QWidget *parent, const QString& property_name,
                    const QString& label, const int& min, const int& max,
                    const int& current) :
        Property(parent, property_name, PROP_SLIDER_INT){

        QHBoxLayout* layout_ = new QHBoxLayout;
        layout_->setContentsMargins(0,0,0,0);
        QLabel* label_  = new QLabel(label);
        layout_->addWidget(label_);
        slider_  = new QSlider(Qt::Orientation::Horizontal);
        slider_->setMinimum(min);
        slider_->setMaximum(max);
        slider_->setValue(current);
        layout_->addWidget(slider_);
        display_  = new QLabel(QString::number(slider_->value()));
        layout_->addWidget(display_);

        this->setLayout(layout_);

        connect(slider_, &QSlider::valueChanged, [this](int value){display_->setText(QString::number(value));});
        connect(slider_, &QSlider::valueChanged, this, &Property_Slider::valueChanged);
    }

    ///
    /// \brief Property_Slider
    /// \param parent
    /// \param label
    /// \param min
    /// \param max
    /// \param current
    /// \param button_label
    /// \param is_reset_button
    ///
    Property_Slider(QWidget *parent, const QString& property_name,
                    const QString& label, const int& min, const int& max,
                    const int& current, const QString& button_label,
                    const bool& is_reset_button = true) :
        Property(parent, property_name, PROP_SLIDER_INT){

        QHBoxLayout* layout_ = new QHBoxLayout;
        layout_->setContentsMargins(0,0,0,0);
        QLabel* label_  = new QLabel(label);
        layout_->addWidget(label_);
        slider_  = new QSlider(Qt::Orientation::Horizontal);
        slider_->setMinimum(min);
        slider_->setMaximum(max);
        slider_->setValue(current);
        layout_->addWidget(slider_);
        display_  = new QLabel(QString::number(slider_->value()));
        layout_->addWidget(display_);
        button_ = new QPushButton(button_label);
        layout_->addWidget(button_);

        this->setLayout(layout_);

        default_value_ = current;

        if(is_reset_button){
            connect(button_, &QPushButton::clicked, [this](int value){slider_->setValue(default_value_);});
        }
        connect(slider_, &QSlider::valueChanged, [this](int value){display_->setText(QString::number(value));});
        connect(slider_, &QSlider::valueChanged, this, &Property_Slider::valueChanged);
    }

    ///
    /// \brief value
    /// \return
    ///
    int value(){
        if(slider_){
            return slider_->value();
        }
        return INT_MAX;
    }

    ///
    /// \brief setValue
    /// \param val
    ///
    void setValue(const QVariant& val){
        slider_->setValue(val.toInt());
    }

private:
    QSlider* slider_;
    QLabel* display_;
    QPushButton* button_;

    int default_value_;

signals:
    ///
    /// \brief valueChanged
    /// \param value
    ///
    void valueChanged(const int& value);

};

class Property_Spin : public Property{

    Q_OBJECT

public:
    ///
    /// \brief Property_Spin
    /// \param parent
    /// \param label
    /// \param min
    /// \param max
    /// \param current
    ///
    Property_Spin(QWidget *parent, const QString& property_name,
                  const QString& label, const int& min, const int& max,
                  const int& current) :
        Property(parent, property_name, PROP_SPIN_INT){

        QHBoxLayout* layout_ = new QHBoxLayout;
        layout_->setContentsMargins(0,0,0,0);
        QLabel* label_  = new QLabel(label);
        layout_->addWidget(label_);
        spin_  = new QSpinBox();
        spin_->setMinimum(min);
        spin_->setMaximum(max);
        spin_->setValue(current);
        layout_->addWidget(spin_);

        this->setLayout(layout_);

        connect(spin_, &QSpinBox::valueChanged, this, &Property_Spin::valueChanged);
    }

    ///
    /// \brief Property_Spin
    /// \param parent
    /// \param label
    /// \param min
    /// \param max
    /// \param current
    /// \param button_label
    /// \param is_reset_button
    ///
    Property_Spin(QWidget *parent, const QString& property_name,
                  const QString& label, const int& min, const int& max,
                  const int& current, const QString& button_label,
                  const bool& is_reset_button = true) :
        Property(parent, property_name, PROP_SPIN_INT){

        QHBoxLayout* layout_ = new QHBoxLayout;
        layout_->setContentsMargins(0,0,0,0);
        QLabel* label_  = new QLabel(label);
        layout_->addWidget(label_);
        spin_  = new QSpinBox();
        spin_->setMinimum(min);
        spin_->setMaximum(max);
        spin_->setValue(current);
        layout_->addWidget(spin_);
        button_ = new QPushButton(button_label);
        layout_->addWidget(button_);

        this->setLayout(layout_);

        default_value_ = current;

        if(is_reset_button){
            connect(button_, &QPushButton::clicked, [this](){spin_->setValue(default_value_);});
        }

        connect(spin_, &QSpinBox::valueChanged, this, &Property_Spin::valueChanged);
    }

    ///
    /// \brief value
    /// \return
    ///
    int value(){
        if(spin_){
            try{
                return spin_->value();
            }
            catch(...){
            }
        }
        return INT_MAX;
    }

    ///
    /// \brief setValue
    /// \param val
    ///
    void setValue(const QVariant& val){
        spin_->setValue(val.toInt());
    }

private:
    QSpinBox* spin_;
    QPushButton* button_;
    int default_value_;

signals:
    ///
    /// \brief valueChanged
    /// \param value
    ///
    void valueChanged(const int& value);

};

class Property_Spin_F : public Property{

    Q_OBJECT

public:
    ///
    /// \brief Property_Spin_F
    /// \param parent
    /// \param label
    /// \param min
    /// \param max
    /// \param current
    ///
    Property_Spin_F(QWidget *parent, const QString& property_name,
                    const QString& label, const float& min, const float& max,
                    const float& current) :
        Property(parent, property_name, PROP_SPIN_DOUBLE){

        QHBoxLayout* layout_ = new QHBoxLayout;
        layout_->setContentsMargins(0,0,0,0);
        QLabel* label_  = new QLabel(label);
        layout_->addWidget(label_);
        spin_  = new QDoubleSpinBox();
        spin_->setMinimum(min);
        spin_->setMaximum(max);
        spin_->setValue(current);
        spin_->setSingleStep(std::min(1.0, double(max-min)/100.0));
        layout_->addWidget(spin_);

        this->setLayout(layout_);

        connect(spin_, &QDoubleSpinBox::valueChanged, this, &Property_Spin_F::valueChanged);
        connect(spin_, &QDoubleSpinBox::editingFinished, [this](){emit valueEditFinished(spin_->value());});
    }

    ///
    /// \brief Property_Spin_F
    /// \param parent
    /// \param label
    /// \param min
    /// \param max
    /// \param current
    /// \param button_label
    /// \param is_reset_button
    ///
    Property_Spin_F(QWidget *parent, const QString& property_name,
                    const QString& label, const double& min, const double& max,
                    const double& current, const QString& button_label,
                    const bool& is_reset_button = true) :
        Property(parent, property_name, PROP_SPIN_DOUBLE){

        QHBoxLayout* layout_ = new QHBoxLayout;
        layout_->setContentsMargins(0,0,0,0);
        QLabel* label_  = new QLabel(label);
        layout_->addWidget(label_);
        spin_  = new QDoubleSpinBox();
        spin_->setMinimum(min);
        spin_->setMaximum(max);
        spin_->setValue(current);
        spin_->setSingleStep(std::min(1.0, double(max-min)/100.0));
        layout_->addWidget(spin_);
        button_ = new QPushButton(button_label);
        layout_->addWidget(button_);

        this->setLayout(layout_);

        default_value_ = current;

        if(is_reset_button){
            connect(button_, &QPushButton::clicked, [this](){spin_->setValue(default_value_);});
        }

        connect(spin_, &QDoubleSpinBox::valueChanged, this, &Property_Spin_F::valueChanged);
        connect(spin_, &QDoubleSpinBox::editingFinished, [this](){
            double value = spin_->value();
            if(value != last_value_){
                last_value_ = value;
                emit valueEditFinished(value);
            }
        });
    }

    ///
    /// \brief value
    /// \return
    ///
    double value(){
        if(spin_){
            try{
                return spin_->value();
            }
            catch(...){
            }
        }
        return NAN;
    }

    ///
    /// \brief setValue
    /// \param val
    ///
    void setValue(const QVariant& val){
        spin_->setValue(val.toDouble());
    }

private:
    QDoubleSpinBox* spin_;
    QPushButton* button_;
    double default_value_;
    double last_value_;

signals:
    ///
    /// \brief valueChanged
    /// \param value
    ///
    void valueChanged(const double& value);

    ///
    /// \brief valueEditFinished
    /// \param value
    ///
    void valueEditFinished(const double& value);

};

class Property_Selector : public Property{

    Q_OBJECT

public:
    ///
    /// \brief Property_Selector
    /// \param parent
    /// \param label
    /// \param list
    ///
    Property_Selector(QWidget *parent, const QString& property_name,
                      const QString& label, const QStringList& list,
                      const int& selected) :
        Property(parent, property_name, PROP_SELECTOR){

        QHBoxLayout* layout_ = new QHBoxLayout;
        layout_->setContentsMargins(0,0,0,0);
        QLabel* label_  = new QLabel(label);
        layout_->addWidget(label_);
        selector_  = new QComboBox();
        selector_->addItems(list);
        selector_->setCurrentIndex(selected);
        layout_->addWidget(selector_);

        this->setLayout(layout_);

        connect(selector_, &QComboBox::currentIndexChanged, this, &Property_Selector::valueChanged);
    }

    ///
    /// \brief index
    /// \return
    ///
    int index(){
        if(selector_){
            return selector_->currentIndex();
        }
        return INT_MAX;
    }

    ///
    /// \brief setValue
    /// \param val
    ///
    void setValue(const QVariant& val){
        selector_->setCurrentIndex(val.toInt());
    }

    ///
    /// \brief text
    /// \return
    ///
    QString text(){return selector_->currentText();}

private:
    QComboBox* selector_;

signals:
    ///
    /// \brief valueChanged
    /// \param value
    ///
    void valueChanged(const int& value);

};

class Property_Check : public Property{

    Q_OBJECT

public:
    ///
    /// \brief Property_Check
    /// \param parent
    /// \param label
    /// \param checked
    /// \param as_toggle_button
    /// \param alternative_label
    ///
    Property_Check(QWidget *parent, const QString& property_name,
                   const QString& label, const bool& checked,
                   const bool& as_toggle_button = false,
                   const QString& alternative_label = "",
                   const bool& label_left = true,
                   const bool& separate_label = true) :
        Property(parent, property_name, PROP_CHECK){

        QHBoxLayout* layout_ = new QHBoxLayout;
        layout_->setContentsMargins(0,0,0,0);
        checkbox_  = new QCheckBox();
        checkbox_->setChecked(checked);

        if(as_toggle_button){
            button_ = new QPushButton(label);
            layout_->addWidget(button_);
            alternative_label_text_ = (alternative_label.size() == 0) ? label : alternative_label;
            label_text_ = label;
        }
        else{

            if(separate_label){
                QLabel* label_  = new QLabel(label);
                if(label_left){
                    layout_->addWidget(label_);
                    layout_->addWidget(checkbox_);
                }
                else{
                    layout_->addWidget(checkbox_);
                    layout_->addWidget(label_);
                }
            }
            else{
                layout_->addWidget(checkbox_);
                checkbox_->setText(label);
                if(label_left){
                    checkbox_->setLayoutDirection(Qt::RightToLeft);
                }
            }
        }

        this->setLayout(layout_);

        default_value_ = checked;

        if(as_toggle_button){
            connect(button_, &QPushButton::clicked, [this](){
                bool checked = !checkbox_->isChecked();
                if(checked == default_value_){
                    button_->setText(label_text_);
                }
                else{
                    button_->setText(alternative_label_text_);
                }

                checkbox_->setChecked(checked);
                emit valueChanged(checked);
            });
        }
        else{
            connect(checkbox_, &QCheckBox::clicked, this, &Property_Check::valueChanged);
        }
    }

    ///
    /// \brief value
    /// \return
    ///
    bool value(){
        return checkbox_->isChecked();
    }

    ///
    /// \brief setValue
    /// \param val
    ///
    void setValue(const QVariant& val){
        checkbox_->setChecked(val.toBool());
    }

private:
    QCheckBox* checkbox_;
    QPushButton* button_;
    bool default_value_;
    QString label_text_;
    QString alternative_label_text_;

signals:
    ///
    /// \brief valueChanged
    /// \param value
    ///
    void valueChanged(const bool& value);

};

class Property_Button: public Property{
    Q_OBJECT

public:
    ///
    /// \brief Property_Check
    /// \param parent
    /// \param label
    /// \param checked
    /// \param as_toggle_button
    /// \param alternative_label
    ///
    Property_Button(QWidget *parent, const QString& property_name,
                    const QString& label) :
        Property(parent, property_name, PROP_BUTTON){

        QHBoxLayout* layout_ = new QHBoxLayout;
        layout_->setContentsMargins(0,0,0,0);
        button_ = new QPushButton(label);
        layout_->addWidget(button_);
        this->setLayout(layout_);

        connect(button_, &QPushButton::clicked, this, &Property_Button::valueChanged);
    }

    ///
    /// \brief setValue
    /// \param val
    ///
    void setValue(const QVariant& val){
        button_->setChecked(val.toBool());
    }


private:
    QPushButton* button_;

signals:
    ///
    /// \brief valueChanged
    /// \param value
    ///
    void valueChanged(const bool& checked);

};

/////////////////////////////  Pattern related  ////////////////////////////////

struct AxisPixels{

    AxisPixels(){}

    AxisPixels(const Eigen::Vector2d& center, const Eigen::Vector2d& x,
               const Eigen::Vector2d& y){
        center_ = center;
        x_ = x;
        y_ = y;
    }

    AxisPixels(const Eigen::Vector2d& center, const Eigen::Vector2d& x,
               const Eigen::Vector2d& y, const Eigen::Vector2d& z){
        center_ = center;
        x_ = x;
        y_ = y;
        z_ = z;
    }

    Eigen::Vector2d center_;
    Eigen::Vector2d x_;
    Eigen::Vector2d y_;
    Eigen::Vector2d z_;
};

struct Idx_Pair{

    Idx_Pair(const size_t& a, const size_t& b){
        a_=a;
        b_=b;
    }

    size_t a_;
    size_t b_;
};

struct Idx_Triple{

    Idx_Triple(const size_t& a, const size_t& b, const size_t& c){
        a_ = a;
        b_ = b;
        c_ = c;
    }

    size_t a_;
    size_t b_;
    size_t c_;
};

struct Idx_Tuple{

    Idx_Tuple(const size_t& a, const size_t& b, const size_t& c, const size_t& d){
        a_=a;
        b_=b;
        c_=c;
        d_=d;
    }

    size_t a_; //upper left
    size_t b_; //upper right
    size_t c_; //bottom left
    size_t d_; //bottom right
};

struct Idx_ID{

    Idx_ID(const size_t& idx, const int& id){
        idx_ = idx;
        id_ = id;
    }

    size_t idx_;
    int id_;
};

class Pose{

public:
    ///
    /// \brief Pose is the fedault constructor
    ///
    Pose(){}

    ///
    /// \brief Pose is a constructor based on a 4x4 pose matrix
    /// \param pose
    ///
    Pose(const Eigen::Matrix4d& pose){
        mat_ = pose;
        t_ = {pose(0,3), pose(1,3), pose(2,3)};
        Eigen::AngleAxisd R_aa;
        Eigen::Matrix3d pose_R = pose.block(0,0,3,3);
        R_aa.fromRotationMatrix(pose_R);
        R_rod_ = {R_aa.angle() * R_aa.axis()[0],
                  R_aa.angle() * R_aa.axis()[1],
                  R_aa.angle() * R_aa.axis()[2]};

        resetOptimizationParameters();
    }

    ///
    /// \brief Pose is a constructor based on a Rodrigues rotation vector and
    ///                a translation vector
    /// \param R_rod   3D Rodrigues rotation vector
    /// \param t       translation vector
    ///
    Pose(const Eigen::Vector3d& R_rod, const Eigen::Vector3d& t){
        R_rod_ = {R_rod[0], R_rod[1], R_rod[2]};
        t_ = {t[0], t[1], t[2]};
        mat_ = Eigen::Matrix4d::Identity();
        mat_(0,3) = t_[0];
        mat_(1,3) = t_[1];
        mat_(2,3) = t_[2];
        Eigen::Vector3d rod(t_[0], t_[1], t_[2]);
        Eigen::AngleAxisd aa = Eigen::AngleAxisd(rod.norm(), rod/rod.norm());
        mat_.block(0,0,3,3) = aa.toRotationMatrix();
    }

    ///
    /// \brief resetOptimizationParameters
    ///
    void resetOptimizationParameters(){
        opt_R_rod_ = R_rod_;
        opt_t_ = t_;
    }

    ///
    /// \brief acceptOptimizationParameters
    ///
    void acceptOptimizationParameters(){
        R_rod_ = opt_R_rod_;
        t_ = opt_t_;
        mat_ = Eigen::Matrix4d::Identity();
        mat_(0,3) = t_[0];
        mat_(1,3) = t_[1];
        mat_(2,3) = t_[2];
        Eigen::Vector3d rod(t_[0], t_[1], t_[2]);
        Eigen::AngleAxisd aa = Eigen::AngleAxisd(rod.norm(), rod/rod.norm());
        mat_.block(0,0,3,3) = aa.toRotationMatrix();
    }

    ///
    /// \brief getDistance calculates the camera to pattern distance
    /// \return
    ///
    double getDistance() const{
        return std::sqrt(t_[0]*t_[0] + t_[1]*t_[1] + t_[2]*t_[2]);
    }

    ///
    /// \brief getAngleDegree calculates the pattern rotation angle in degrees
    /// \return
    ///
    double getAngleDegree() const{
        return 180.0 / M_PIf32 * std::sqrt(R_rod_[0]*R_rod_[0] + R_rod_[1]*R_rod_[1] + R_rod_[2]*R_rod_[2]);
    }

public:

    std::vector<double> R_rod_ = std::vector<double>(3,0.0);
    std::vector<double> t_ = std::vector<double>(3,0.0);

    std::vector<double> opt_R_rod_ = std::vector<double>(3,0.0);
    std::vector<double> opt_t_ = std::vector<double>(3,0.0);

    Eigen::Matrix4d mat_;
};

class Partition{

public:

    /// Pattern divided in tiles for efficient distortion of AR overlay
    std::vector<std::shared_ptr<cv::Mat>> tiles_;

    /// Same as above, but differently colored to give visual feedback when the
    /// detection matches the overlay
    std::vector<std::shared_ptr<cv::Mat>> tiles_accepted_;

    /// Homographies for transforming the tiles into image space
    std::vector<Eigen::Matrix3d> homographies_;

    /// The tile corners in metric coordinates for 3D -> 2D projection
    std::vector<Eigen::Vector3d> corners_;
    ///
    std::vector<Eigen::Vector3d> outer_board_corners_;

    ///
    Eigen::Vector3d center_;
    /// Axes vector end points in metric coordinates for user friendly overlay
    std::vector<Eigen::Vector3d> axes_;
    /// Center and axes ends projected into pixel coordinates
    std::vector<Eigen::Vector2d> axes_pixels_;

    ///
    double width_in_mm_ = 0.0;
    double height_in_mm_ = 0.0;

    ///
    double min_mm_per_pixel_ = 0.0;

    ///
    std::vector<Eigen::Vector2d> corner_target_pixels_;

    ///
    QRectF bounding_box_;

    /// Index array associating the tiles with the above corner list to prevent
    /// storing corner duplicates
    std::vector<Idx_Tuple> tile_corners_;

    /// Index array associating the listed corners with the detectable points of
    /// the pattern
    std::vector<Idx_ID> id_corners_;

    /// Index array describing the indices of the four outer most corners which
    /// are used to match detections and overlays
    std::vector<Idx_ID> outer_id_corners_;

    /// Index array describing the indices of the four inner most corners which
    /// are used for the orientation visualization
    std::vector<Idx_ID> inner_id_corners_;
    std::vector<double> inner_corner_distances_;

    ///
    /// \brief clear
    ///
    void clear(){
        tiles_.clear();
        tiles_accepted_.clear();
        homographies_.clear();
        corners_.clear();
        outer_board_corners_.clear();
        axes_.clear();
        axes_pixels_.clear();
        corner_target_pixels_.clear();
        tile_corners_.clear();
        id_corners_.clear();
        outer_id_corners_.clear();
        inner_id_corners_.clear();
        inner_corner_distances_.clear();
    }

    ///
    /// \brief isEmpty
    /// \return
    ///
    bool isEmpty() const{
        return (tiles_.size() == 0 || tile_corners_.size() == 0 || corners_.size() == 0);
    }

};


//////////////////////////// Features and Points ///////////////////////////////

///
/// \brief The Feature_Point class represents a detected feature point including
///                                its pixel and metric position as well as its
///                                ID
///
class Feature_Point{

public:
    ///
    /// \brief Feature_Point
    /// \param x
    /// \param y
    /// \param id
    ///
    Feature_Point(const double& x, const double& y, const int& id = 0){
        x_ = x;
        y_ = y;
        id_ = id;
    }

    ///
    /// \brief Feature_Point
    /// \param pixel
    /// \param id
    /// \param pattern_point
    ///
    Feature_Point(const cv::Vec2d& pixel, const int& id, const cv::Vec3d& pattern_point){
        x_ = pixel[0];
        y_ = pixel[1];
        id_ = id;
        patt_x_ = pattern_point[0];
        patt_y_ = pattern_point[1];
        patt_z_ = pattern_point[2];
    }

    ///
    /// \brief Feature_Point
    /// \param pixel
    /// \param id
    /// \param pattern_point
    ///
    Feature_Point(const Eigen::Vector2d& pixel, const int& id, const Eigen::Vector3d& pattern_point){
        x_ = pixel[0];
        y_ = pixel[1];
        id_ = id;
        patt_x_ = pattern_point[0];
        patt_y_ = pattern_point[1];
        patt_z_ = pattern_point[2];
    }

    ///
    /// \brief pixel returns the detected pixel as 2D Eigen vector
    /// \return
    ///
    Eigen::Vector2d pixel() const{
        return Eigen::Vector2d(x_,y_);
    }

    ///
    /// \brief patternPoint returns the pattern point in pattern coordinates as
    ///                     3D Eigen vector
    /// \return
    ///
    Eigen::Vector3d patternPoint() const{
        return Eigen::Vector3d(patt_x_, patt_y_, patt_z_);
    }

public:

    /// Pixel coordinates
    double x_;
    double y_;
    /// Feature id
    int id_;
    /// Corresponding pattern point
    double patt_x_;
    double patt_y_;
    double patt_z_;

    /// Corresponding idx in second camera's detection (-1 = no correspondence)
    int correspondence_ = -1;

};

/// Collection of detected feature points for a single image
using Detection = std::shared_ptr<std::vector<Feature_Point>>;

///
/// \brief The Detection_Frame class
///
class Detection_Frame{

public:

    Detection_Frame(std::shared_ptr<const Frame>& frame, const std::vector<Detection> points){
        frame_ = frame;
        points_ = points;
    };

public:

    std::shared_ptr<const Frame> frame_ = nullptr;
    std::vector<Detection> points_;
};

///
/// \brief The Dedicated_Detection class wraps a frame's detection and adds
///                                      additional information such as a list
///                                      of modes for which the detection is
///                                      used and the pose of the pattern.
///
class Dedicated_Detection{

public:

    ///
    /// \brief Dedicated_Detection is the default constructor
    /// \param detection           Raw detection without additional information
    /// \param pose_proposal       Proposed pose used to generate the AR overlay
    ///                            which this detection was matched to
    /// \param mode                the current optimization mode
    ///
    Dedicated_Detection(const std::vector<Detection>& detection,
                        const std::shared_ptr<Pose>& pose_proposal,
                        const int& mode){
        detection_ = detection;
        pose_ = pose_proposal;
        used_for_mode_[mode] = true;

        // Check for correspondences and save the indices
        if(detection.size() > 1){
            num_correspondences_ = 0;
            num_cam_0_unique_ = detection[0]->size();
            num_cam_1_unique_ = detection[1]->size();

            for(size_t fp_idx_1 = 0; fp_idx_1 < detection[0]->size(); fp_idx_1++){
                for(size_t fp_idx_2 = 0; fp_idx_2 < detection[1]->size(); fp_idx_2++){
                    if((*detection[0])[fp_idx_1].id_ == (*detection[1])[fp_idx_2].id_){
                        (*detection[0])[fp_idx_1].correspondence_ = int(fp_idx_2);
                        (*detection[1])[fp_idx_2].correspondence_ = int(fp_idx_1);
                        num_correspondences_++;
                        num_cam_0_unique_--;
                        num_cam_1_unique_--;
                        break;
                    }
                }
            }
        }
    }

    ///
    /// \brief removeDetectedPoint      can be used to remove single detection
    ///                                 points, e.g. if they are considered to
    ///                                 be outliers
    /// \param fp_idx                   feature point index - NOTE: If there is
    ///                                 a list of fp indices to be removed,
    ///                                 always start with the largest to keep
    ///                                 the remaining list valid!
    /// \param cam_idx                  index of camera to remove the detection
    ///                                 from
    /// \param remove_correspondence    if set to true, the correspondence of
    ///                                 the feature point from the second camera
    ///                                 is also removed
    ///
    void removeDetectedPoint(const size_t& fp_idx, const int& cam_idx,
                             const bool& remove_correspondence = true){

        // Check whether feature point is available
        if(detection_[cam_idx]->size() > fp_idx){

            int correspondence_idx = detection_[cam_idx]->at(fp_idx).correspondence_;

            detection_[cam_idx]->erase(detection_[cam_idx]->begin() + fp_idx);

            // Correct correspondence indices
            if(detection_.size() > 1){
                for(Feature_Point& fp : *(detection_[1-cam_idx])){
                    if(fp.correspondence_ >= fp_idx){
                        fp.correspondence_--;
                    }
                }
            }

            // Check if correspondence available
            if(correspondence_idx != -1){

                if(remove_correspondence){
                    // Remove correspondence
                    detection_[1 - cam_idx]->erase(detection_[1 - cam_idx]->begin() + correspondence_idx);

                    // Correct correspondence indices
                    for(Feature_Point& fp : *(detection_[cam_idx])){
                        if(fp.correspondence_ >= correspondence_idx){
                            fp.correspondence_--;
                        }
                    }
                }
                else{
                    // Set correspondence's correspondence idx to invalid since
                    // we just deleted it
                    detection_[1 - cam_idx]->at(correspondence_idx).correspondence_ = -1;

                    // Correct the unique counters
                    if(cam_idx == 0){
                        num_cam_1_unique_++;
                    }
                    else{
                        num_cam_0_unique_++;
                    }

                }
                num_correspondences_--;
            }
            else{
                // Correct the unique counters
                if(cam_idx == 0){
                    num_cam_0_unique_--;
                }
                else{
                    num_cam_1_unique_--;
                }
            }
        }
    }

    ///
    /// \brief isDegeneratedCase checks whether the detected feature points
    ///                          represent a degnerated case (e.g. are located
    ///                          on a common line).
    /// \param mode
    /// \param remove_degnerated
    /// \param cam_idx
    /// \return
    ///
    bool isDegeneratedCase(const int& mode, const bool& remove_degenerated = false, const size_t& cam_idx = -1){

        // Create index list for cameras to check
        std::vector<size_t> cam_idc;
        if(cam_idx != -1){
            cam_idc.push_back(cam_idx);
        }
        else{
            cam_idc.push_back(0);
            cam_idc.push_back(1);
        }

        // Check feature points for the different cameras
        bool a_tested_detection_is_degenerated = false;
        for(const size_t& cam : cam_idc){
            // Calculate center
            double x_mean = 0.0;
            double y_mean = 0.0;
            double denom = double(detection_[cam]->size());
            for(const Feature_Point& fp : *(detection_[cam])){
                x_mean += fp.patt_x_ / denom;
                y_mean += fp.patt_y_ / denom;
            }

            // Calculate covariance matrix Cov
            double u_20 = 0.0;
            double u_02 = 0.0;
            double u_11 = 0.0;
            for(const Feature_Point& fp : *(detection_[cam])){
                u_20 += (fp.patt_x_ - x_mean)*(fp.patt_x_ - x_mean) / denom;
                u_02 += (fp.patt_y_ - y_mean)*(fp.patt_y_ - y_mean) / denom;
                u_20 += (fp.patt_x_ - x_mean)*(fp.patt_y_ - y_mean) / denom;
            }

            // Calculate Eigenvalues of Cov
            double root = std::sqrt(4.0 * u_11 * u_11 + (u_20 - u_02)*(u_20 - u_02))/2.0;
            double lambda_1 = (u_20 + u_02)/2.0;
            double lambda_2 = lambda_1 + root;
            lambda_1 -= root;

            // Check if degenerated
            bool detection_degenerated = (lambda_1 * 4.0 < lambda_2) || (detection_[cam]->size() < 10);
            a_tested_detection_is_degenerated |= detection_degenerated;

            if(detection_degenerated){

                // Remove all detections of the degenerated case
                if(remove_degenerated){
                    detection_[cam]->clear();
                    for(Feature_Point& fp : *(detection_[1-cam])){
                        fp.correspondence_ = -1;
                    }
                    num_correspondences_ = 0;
                    if(cam == 0){
                        num_cam_0_unique_ = 0;
                        num_cam_1_unique_ = detection_[1-cam]->size();
                    }
                    else{
                        num_cam_0_unique_ = detection_[1-cam]->size();
                        num_cam_1_unique_ = 0;
                    }
                }

                // Flag detection as unused for current optimization mode
                used_for_mode_[mode] = false;
            }


        }

        return a_tested_detection_is_degenerated;
    }

    ///
    /// \brief isEmpty
    /// \return
    ///
    bool isEmpty(){
        return (detection_[0]->size() == 0) && (detection_[1]->size() == 0);
    }

public:

    std::vector<Detection> detection_;
    int num_correspondences_ = 0;
    int num_cam_0_unique_ = 0;
    int num_cam_1_unique_ = 0;

    std::vector<bool> used_for_mode_ = {false, false, false};

    std::shared_ptr<Pose> pose_;
};


/////////////////////////// Optimization related ///////////////////////////////

/// Optimization mode flags
const int OPT_MODE_RGB = 0;     /// RGB intrinsics optimization
const int OPT_MODE_IR = 1;      /// IR intrinsics optimization
const int OPT_MODE_TRAFO = 2;   /// RGB to IR extrinsics optimization
const int OPT_MODE_ALL = 3;     /// Bundle Adjustment

/// Optimization submode flags
const int OPT_SUBMODE_RANDOM = 0;       /// Create and handle random poses
const int OPT_SUBMODE_RANDOM_AREA = 1;  /// Random poses but without specific overlay
const int OPT_SUBMODE_OPTIMIZED = 2;    /// Create and handle optimized poses
///

//////////////////////////// Camera Calibration ////////////////////////////////

/// Calibration model flags
const int CALIB_BROWN = 0;      /// Classical Brown distortion model
const int CALIB_RATIONAL = 1;   /// Brown + rational radial distortion

///
/// \brief The Cam_Parameters class
///
class Cam_Parameters{

public:

    std::string camera_name_;

    int resolution_x = 0;
    int resolution_y = 0;

    //////////////////////////////  Intrinsics  ////////////////////////////////
    /// Principal point
    double cx_ = 0.0;
    double cy_ = 0.0;

    /// Focal lengths
    double fx_ = 0.0;
    double fy_ = 0.0;

    /// Skew
    double s_ = 0.0;

    /// Determines the number of parameters to use - compare OpenCV's
    /// calibrateCamera
    int distortion_model_ = CALIB_RATIONAL;

    /// Radial distortion coefficients
    std::vector<double> k_ = std::vector<double>(6, 0.0);

    /// Tangential distortion coefficients
    double p1_ = 0.0;
    double p2_ = 0.0;

    /// The same data again as more compact double arrays for the optimization
    std::vector<double> opt_K_ = std::vector<double>(4, 0.0);
    /// The following vector contains p1, p2, k1,..., k6 in that order
    std::vector<double> opt_dist_ = std::vector<double>(8, 0.0);

    //////////////////////////////  Extrinsics  ////////////////////////////////

    /// NOTE: R and t describe the transform into the reference camera or world
    ///       system, i.e. a point p in world space is transformed into camera
    ///       space by R^-1*p - R^-1*t.

    /// Rotation matrix
    Eigen::Matrix3d R_ = Eigen::Matrix3d::Identity(3,3);
    Eigen::Vector3d R_rod_ = Eigen::Vector3d(2.0*M_PIf64, 0.0, 0.0);

    /// Translation
    Eigen::Vector3d t_ = Eigen::Vector3d(0.0, 0.0, 0.0);

    /// The same data again as more compact double arrays for the optimization
    std::vector<double> opt_R_rod_ = std::vector<double>(3, 0.0);
    std::vector<double> opt_t_ = std::vector<double>(3, 0.0);

    ////////////////////////////  Error Metrics  ///////////////////////////////

    double rmse_ = -1.0;
    double trace_ = -1.0;

    /////////////////////////////  Calculations  ///////////////////////////////

    ///
    /// \brief projectWorldToPixel
    /// \param p
    /// \return
    ///
    Eigen::Vector2d projectWorldToPixel(const Eigen::Vector3d& p){

        // Transform to camera space
        Eigen::Vector3d p_cam = R_.transpose() * (p - t_);
        // Homogenize
        p_cam /= p_cam[2];

        // apply distortion
        double r2 = p_cam[0]*p_cam[0] + p_cam[1]*p_cam[1];
        double r4 = r2 * r2;
        double r6 = r4 * r2;
        double x2 = p_cam[0] * p_cam[0];
        double y2 = p_cam[1] * p_cam[1];
        double xy = p_cam[0] * p_cam[1];

        // Standard Brown distortion model with third degree polynomial
        double dist_rad = (1.0 + k_[0]*r2 + k_[1]*r4 + k_[2]*r6);
        // Extension to rational model
        if(distortion_model_ == CALIB_RATIONAL){
            dist_rad /=(1.0 + k_[3]*r2 + k_[4]*r4 + k_[5]*r6);
        }

        double x_dist = p_cam[0] * dist_rad + 2.0 * p1_ * xy + p2_ * (r2 + 2.0 * x2);
        double y_dist = p_cam[1] * dist_rad + 2.0 * p2_ * xy + p1_ * (r2 + 2.0 * y2);

        // Transform to pixel coordinates (ignoring skew)
        return Eigen::Vector2d(fx_ * x_dist + cx_, fy_ * y_dist + cy_);
    }

    ///
    /// \brief projectCamToPixel
    /// \param p
    /// \return
    ///
    Eigen::Vector2d projectCamToPixel(const Eigen::Vector3d& p){

        // Homogenize
        Eigen::Vector3d p_cam = p/p[2];

        // Apply distortion
        double r2 = p_cam[0]*p_cam[0] + p_cam[1]*p_cam[1];
        double r4 = r2 * r2;
        double r6 = r4 * r2;
        double x2 = p_cam[0] * p_cam[0];
        double y2 = p_cam[1] * p_cam[1];
        double xy = p_cam[0] * p_cam[1];

        // Standard Brown distortion model with third degree polynomial
        double dist_rad = (1.0 + k_[0]*r2 + k_[1]*r4 + k_[2]*r6);
        // Extension to rational model
        if(distortion_model_ == CALIB_RATIONAL){
            dist_rad /=(1.0 + k_[3]*r2 + k_[4]*r4 + k_[5]*r6);
        }

        double x_dist = p_cam[0] * dist_rad + 2.0 * p1_ * xy + p2_ * (r2 + 2.0 * x2);
        double y_dist = p_cam[1] * dist_rad + 2.0 * p2_ * xy + p1_ * (r2 + 2.0 * y2);

        // Transform to pixel coordinates (ignoring skew)
        return Eigen::Vector2d(fx_ * x_dist + cx_, fy_ * y_dist + cy_);
    }

    ///
    /// \brief undistort
    /// \param pixel
    /// \return
    ///
    Eigen::Vector2d undistort(const Eigen::Vector2d& pixel){

    }

    ///
    /// \brief randomizeOptimizerParameters
    /// \param mode
    ///
    void randomizeOptimizerParameters(const int mode){

        if(mode != OPT_MODE_TRAFO){
            auto ran = [this](){
                // random value between 0.9 and 1.1
                return unif_(re_)*0.2 + 0.9;
            };
            auto ranD = [this](){
                // random value between 0.9 and 1.0
                return unif_(re_)*0.1 + 0.9;
            };

            double scale_f = ran();
            opt_K_ = {scale_f*fx_, scale_f*fy_, ran()*cx_, ran()*cy_};
            opt_dist_.clear();
            opt_dist_.push_back(p1_);
            opt_dist_.push_back(p2_);
            opt_dist_.insert(opt_dist_.begin()+2, k_.begin(), k_.end());
            for(double& dist : opt_dist_){
                dist *= ranD();
            }
        }
        else{
            auto ran = [this](){
                // random value between 0.95 and 1.05
                return unif_(re_)*0.1 + 0.95;
            };

            opt_t_ = {ran() * t_[0], ran() * t_[1], ran() * t_[2]};
            opt_R_rod_ = {ran() * R_rod_[0], ran() * R_rod_[1], ran() * R_rod_[2]};
        }
    }

    ///
    /// \brief resetOptimizerParameters
    ///
    void resetOptimizerParameters(){
        opt_K_ = {fx_, fy_, cx_, cy_};
        opt_dist_.clear();
        opt_dist_.push_back(p1_);
        opt_dist_.push_back(p2_);
        opt_dist_.insert(opt_dist_.begin()+2, k_.begin(), k_.end());
        opt_t_ = {t_[0], t_[1], t_[2]};
        opt_R_rod_ = {R_rod_[0], R_rod_[1], R_rod_[2]};
    }

    ///
    /// \brief acceptOptimized
    ///
    void acceptOptimized(){
        fx_ = opt_K_[0];
        fy_ = opt_K_[1];
        cx_ = opt_K_[2];
        cy_ = opt_K_[3];
        p1_ = opt_dist_[0];
        p2_ = opt_dist_[1];
        k_ = std::vector<double>(opt_dist_.begin()+2,opt_dist_.begin()+8);
        t_ = Eigen::Vector3d(opt_t_[0], opt_t_[1], opt_t_[2]);
        R_rod_ = Eigen::Vector3d(opt_R_rod_[0],opt_R_rod_[1],opt_R_rod_[2]);
        double angle = R_rod_.norm();
        Eigen::AngleAxisd rot(angle, R_rod_/angle);
        R_ = rot.toRotationMatrix();
    }

private:
    /// random sampler
    std::uniform_real_distribution<double> unif_ = std::uniform_real_distribution<double>(0.0, 1.0);
    std::default_random_engine re_;

};


