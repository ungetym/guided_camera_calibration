#include "data.h"
#include "Cameras/Generic_Webcam/generic_webcam.h"


#ifdef  USE_AZURE_KINECT
#include "Cameras/Azure_Kinect/azure_kinect.h"
#endif

#ifdef USE_KINECT_V2
#include "Cameras/KinectV2/kinect_v2.h"
#endif

#ifdef USE_HIKROBOT
#include "Cameras/Hikrobot/hikrobot_cam.h"
#endif

#include "Patterns/ArUco/charuco.h"
#include "Patterns/Checkerboard/checkerboard.h"

#include <QInputDialog>

Data::Data(){

    // Create pattern generator
    pattern_ = std::shared_ptr<Pattern>(new Checkerboard());
    pattern_thread_ = std::unique_ptr<QThread>(new QThread());
    pattern_->moveToThread(pattern_thread_.get());
    pattern_thread_->start();

    // Create AR overlay
    ar_overlay_ = std::unique_ptr<AR_Overlay>(new AR_Overlay());
    ar_overlay_thread_ = std::unique_ptr<QThread>(new QThread());
    ar_overlay_->moveToThread(ar_overlay_thread_.get());
    ar_overlay_thread_->start();

    // Connect pattern to overlay in order to keep the overlay updated with the
    // currently used pattern...
    connect(pattern_.get(), &Pattern::patternPartitionChanged,
            ar_overlay_.get(), &AR_Overlay::updatePattern);
    //...and check whether detected patterns match the current overlay
    connect(pattern_.get(), &Pattern::patternDetected,
            ar_overlay_.get(), &AR_Overlay::checkNewDetection);

    // Create optimizer
    optimizer_ = std::shared_ptr<Optimizer>(new Optimizer());
    optimizer_thread_ = std::unique_ptr<QThread>(new QThread());
    optimizer_->moveToThread(optimizer_thread_.get());
    optimizer_thread_->start();
    // Connect optimizer and AR overlay:
    // Initial pose request
    connect(ar_overlay_.get(), &AR_Overlay::requestNewPose,
            optimizer_.get(), &Optimizer::calculateNextProposal);
    // If detection matches the overlay, use it for the optimization..
    connect(ar_overlay_.get(), &AR_Overlay::matchedDetection,
            optimizer_.get(), &Optimizer::addDetection);
    //..and propose a new pattern pose which is then used for the AR overlay
    connect(optimizer_.get(), &Optimizer::updatedPoseProposal,
            ar_overlay_.get(), &AR_Overlay::updatePose);
    connect(optimizer_.get(), &Optimizer::updatedPoseProposal,
            pattern_.get(), &Pattern::setProposedPose);
    connect(optimizer_.get(), &Optimizer::updatedOptimizationMode,
            ar_overlay_.get(), &AR_Overlay::setOptimizationMode);
    // After the optimizer modified the camera parameters, these are also used
    // for the AR overlay
    connect(optimizer_.get(), &Optimizer::updatedCameraParameters,
            ar_overlay_.get(), &AR_Overlay::updateCameraParameters);
    // Connect optimizer and pattern creator
    connect(pattern_.get(), &Pattern::patternPartitionChanged,
            optimizer_.get(), &Optimizer::setPattern);
    connect(optimizer_.get(), &Optimizer::updatedOptimizationMode,
            pattern_.get(), &Pattern::setOptimizationMode);

    // Create camera

    // Collect camera names of all available devices
    QStringList camera_names;
    std::vector<size_t> device_list_sizes = {0,0,0,0};
    Camera* cam_generic = new Generic_Webcam();
    camera_names = cam_generic->getDeviceList();
    device_list_sizes[0] = camera_names.size();

#ifdef USE_AZURE_KINECT
    Camera* cam_azure = new Azure_Kinect();
    QStringList azure_names = cam_azure->getDeviceList();
    camera_names.append(azure_names);
    device_list_sizes[1] = azure_names.size();
#endif

#ifdef USE_KINECT_V2
    Camera* cam_kinect_v2 = new Kinect_v2();
    QStringList kinect_names = cam_kinect_v2->getDeviceList();
    camera_names.append(kinect_names);
    device_list_sizes[2] = kinect_names.size();
#endif

#ifdef USE_HIKROBOT
    Camera* cam_hikrobot = new Hikrobot_Cam();
    QStringList hikrobot_names = cam_hikrobot->getDeviceList();
    camera_names.append(hikrobot_names);
    device_list_sizes[3] = hikrobot_names.size();
#endif

    // Open a QDialog to let the user select a device
    QString selected_serial = QInputDialog::getItem(nullptr, "Select a camera",
                                                    "Device: ", camera_names,
                                                    0, false);


    int selected_device = (selected_serial.size() == 0) ? 0 : camera_names.indexOf(selected_serial);

    int type = 0;
    for(const size_t& list_size : device_list_sizes){
        if(selected_device < list_size){
            break;
        }
        else{
            selected_device -= list_size;
            type++;
        }
    }

    if(type == 0){
        camera_ = new Generic_Webcam();
    }
    else if(type == 1){
#ifdef USE_AZURE_KINECT
        camera_ = new Azure_Kinect();
#endif
    }
    else if(type == 2){
#ifdef USE_KINECT_V2
        camera_ = new Kinect_v2();
#endif
    }
    else if(type == 3){
#ifdef USE_HIKROBOT
        camera_ = new Hikrobot_Cam();
#endif
    }

    camera_thread_ = std::unique_ptr<QThread>(new QThread());
    camera_->moveToThread(camera_thread_.get());
    camera_thread_->start();
    if(camera_->init(selected_device)){
        // Connect camera to pattern detector
        connect(camera_, &Camera::receivedNewFrame, pattern_.get(),
                qOverload<std::shared_ptr<const Frame>&>(&Pattern::detectPattern));
    }
    // Connect camera and AR overlay to update initial camera parameters
    connect(camera_, &Camera::initialized,
            ar_overlay_.get(), &AR_Overlay::updateCameraParameters);
    // Connect camera and optimizer to update initial camera parameters
    connect(camera_, &Camera::initialized,
            optimizer_.get(), &Optimizer::updateCameraParameters);
}

Data::~Data(){
    // Stop camera and threads
    camera_->stop();
    camera_thread_->exit();
    camera_thread_->wait();

    optimizer_thread_->exit();
    optimizer_thread_->wait();

    ar_overlay_thread_->exit();
    ar_overlay_thread_->wait();

    pattern_thread_->exit();
    pattern_thread_->wait();
}

bool Data::switchPatternType(const int& type){
    // Disconnect pattern signals/slots
    disconnect(pattern_.get(), &Pattern::patternPartitionChanged,
               ar_overlay_.get(), &AR_Overlay::updatePattern);
    disconnect(pattern_.get(), &Pattern::patternDetected,
               ar_overlay_.get(), &AR_Overlay::checkNewDetection);
    disconnect(optimizer_.get(), &Optimizer::updatedPoseProposal,
               pattern_.get(), &Pattern::setProposedPose);
    disconnect(pattern_.get(), &Pattern::patternPartitionChanged,
               optimizer_.get(), &Optimizer::setPattern);
    disconnect(optimizer_.get(), &Optimizer::updatedOptimizationMode,
               pattern_.get(), &Pattern::setOptimizationMode);
    disconnect(camera_, &Camera::receivedNewFrame, pattern_.get(),
               qOverload<std::shared_ptr<const Frame>&>(&Pattern::detectPattern));

    // Remove old pattern
    pattern_thread_->exit();
    STATUS("Waiting for all pattern operations to be finished before switching..");
    while(!pattern_thread_->isFinished()){
        QThread::sleep(1);
    }
    STATUS("..done. Please create a new pattern.");

    // Create new pattern
    if(type == 0){
        pattern_ = std::shared_ptr<Pattern>(new ChArUco());
    }
    else if(type == 1){
        pattern_ = std::shared_ptr<Pattern>(new Checkerboard());
    }
    pattern_thread_ = std::unique_ptr<QThread>(new QThread());
    pattern_->moveToThread(pattern_thread_.get());
    pattern_thread_->start();

    // Connect new pattern
    connect(pattern_.get(), &Pattern::patternPartitionChanged,
            ar_overlay_.get(), &AR_Overlay::updatePattern);
    connect(pattern_.get(), &Pattern::patternDetected,
            ar_overlay_.get(), &AR_Overlay::checkNewDetection);
    connect(optimizer_.get(), &Optimizer::updatedPoseProposal,
            pattern_.get(), &Pattern::setProposedPose);
    connect(pattern_.get(), &Pattern::patternPartitionChanged,
            optimizer_.get(), &Optimizer::setPattern);
    connect(optimizer_.get(), &Optimizer::updatedOptimizationMode,
            pattern_.get(), &Pattern::setOptimizationMode);
    connect(camera_, &Camera::receivedNewFrame, pattern_.get(),
            qOverload<std::shared_ptr<const Frame>&>(&Pattern::detectPattern));

    return true;
}

std::vector<std::shared_ptr<Property>> Data::getAllProperties(){

    std::vector<std::shared_ptr<Property>> properties;

    // Collect properties from pattern
    std::vector<std::shared_ptr<Property>> pattern_props = pattern_->getProperties();
    properties.insert(properties.end(), pattern_props.begin(), pattern_props.end());
    // Collect properties from camera
    std::vector<std::shared_ptr<Property>> cam_props = camera_->getProperties();
    properties.insert(properties.end(), cam_props.begin(), cam_props.end());
    // Collect properties from optimizer
    std::vector<std::shared_ptr<Property>> optimization_props = optimizer_->getProperties();
    properties.insert(properties.end(), optimization_props.begin(), optimization_props.end());

    return properties;
}
