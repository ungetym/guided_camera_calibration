////////////////////////////////////////////////////////////////////////////////
///                                                                          ///
///     Helper class for advance math functions used throughout multiple     ///
///                             program parts                                ///
///                                                                          ///
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <eigen3/Eigen/Eigen>

namespace math_gcc{

///
/// \brief calcHomography calculates a homography H between two sets of 2d
///                       points, so that H*source_point=target_point is met in
///                       the least squares sense
/// \param source_points
/// \param target_points
/// \return
///
Eigen::Matrix3d calcHomography(std::vector<Eigen::Vector2d> source_points,
                               std::vector<Eigen::Vector2d> target_points){
    Eigen::Matrix<double, 8, 9> A;

    for(int i=0; i<4; i++){
        Eigen::Matrix<double, 9, 1> ax;
        Eigen::Matrix<double, 9, 1> ay;

        ax << source_points[i][0], source_points[i][1], 1.0, 0.0, 0.0, 0.0,
                -source_points[i][0] * target_points[i][0],
                -source_points[i][1] * target_points[i][0],
                -target_points[i][0];
        ay << 0.0, 0.0, 0.0, -source_points[i][0], -source_points[i][1], -1.0,
                source_points[i][0] * target_points[i][1],
                source_points[i][1] * target_points[i][1],
                target_points[i][1];

        A.row(2*i) = ax;
        A.row(2*i+1) = ay;
    }

    Eigen::JacobiSVD<Eigen::Matrix<double, 8, 9>> svd(A, Eigen::ComputeFullU
                                                      | Eigen::ComputeFullV);

    Eigen::Matrix<double, 9, 9> V = svd.matrixV();
    Eigen::Matrix<double, 9, 1> h = V.col(8);

    Eigen::Matrix<double, 3, 3> homography;
    homography << h(0,0), h(1,0), h(2,0),
                  h(3,0), h(4,0), h(5,0),
                  h(6,0), h(7,0), h(8,0);

    return homography;
}


}
