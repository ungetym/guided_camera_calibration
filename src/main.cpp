#include "UI/main_window.h"

#include <QApplication>

int main(int argc, char *argv[]){

    QApplication a(argc, argv);
    std::shared_ptr<Data> data = std::shared_ptr<Data>(new Data());
    Main_Window w(nullptr, data);
    w.show();
    return a.exec();
}
