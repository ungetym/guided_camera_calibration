#pragma once

#include "Common/data_types.h"

#define GLOG_NO_ABBREVIATED_SEVERITIES
#include <ceres/ceres.h>
#include <ceres/rotation.h>

///
/// \brief The Projection_Error_Intrinsics_Brown class creates the cost functor
///                                              to calculate the residuals for
///                                              the intrinsics optimization via
///                                              ceres assuming the Brownian
///                                              distortion model with 3 radial
///                                              distortion parameters
///
class Projection_Error_Intrinsics_Brown{

public:

    ///
    /// \brief Projection_Error_Intrinsics_Brown is the default constructor
    /// \param p                detected 2D pixel position
    /// \param P                associated 3D corner position in pattern
    ///                         coordinates
    /// \param R_rod            rotation of camera into target frame
    /// \param t                translation of camera into target frame
    /// \param weight           for the residuals
    ///
    Projection_Error_Intrinsics_Brown(const Eigen::Vector2d& p,
                                      const Eigen::Vector3d& P,
                                      const Eigen::Vector3d& R_rod,
                                      const Eigen::Vector3d& t,
                                      const double& weight = 1.0){
        p_ = p;
        P_ = P;
        R_rod_ = R_rod;
        t_ = t;
        weight_ = weight;
    }

    ///
    /// \brief operator ()
    /// \param K            contains fx, fy, cx, cy
    /// \param dist         contains k1,...,k6,p1,p2
    /// \param rot_P        pattern rotation Rodrigues vector
    /// \param t_P          pattern translation
    /// \param residuals
    /// \return
    ///
    template <typename T>
    bool operator()(const T* const K, const T* const dist, const T* const rot_P,
                    const T* const t_P, T* residuals) const;

private:
    /// 3D pattern point in pattern coordinates
    Eigen::Vector3d P_;
    /// Detected 2D pixel position
    Eigen::Vector2d p_;
    /// Rotation of camera into target frame
    Eigen::Vector3d R_rod_;
    /// Translation of camera into target frame
    Eigen::Vector3d t_;
    /// Weight for residuals in case different error types or cameras with
    /// different numbers of detections are optimized simultaneously
    double weight_;
};

///
/// \brief The Projection_Error_Intrinsics_Rational class is the analogue class
///                                        to Projection_Error_Intrinsics_Brown
///                                        for the rational distortion model
///
class Projection_Error_Intrinsics_Rational{

public:

    ///
    /// \brief Projection_Error_Intrinsics_Rational is the default constructor
    /// \param p                detected 2D pixel position
    /// \param P                associated 3D corner position in pattern
    ///                         coordinates
    /// \param R_rod            rotation of camera into target frame
    /// \param t                translation of camera into target frame
    /// \param weight           for the residuals
    ///
    Projection_Error_Intrinsics_Rational(const Eigen::Vector2d& p,
                                         const Eigen::Vector3d& P,
                                         const Eigen::Vector3d& R_rod,
                                         const Eigen::Vector3d& t,
                                         const double& weight = 1.0){
        p_ = p;
        P_ = P;
        R_rod_ = R_rod;
        t_ = t;
        weight_ = weight;
    }

    ///
    /// \brief operator ()
    /// \param K            contains fx, fy, cx, cy
    /// \param dist         contains k1,...,k6,p1,p2
    /// \param rot_P        pattern rotation Rodrigues vector
    /// \param t_P          pattern translation
    /// \param residuals
    /// \return
    ///
    template <typename T>
    bool operator()(const T* const K, const T* const dist, const T* const rot_P,
                    const T* const t_P, T* residuals) const;

private:
    /// 3D pattern point in pattern coordinates
    Eigen::Vector3d P_;
    /// Detected 2D pixel position
    Eigen::Vector2d p_;
    /// Rotation of camera into target frame
    Eigen::Vector3d R_rod_;
    /// Translation of camera into target frame
    Eigen::Vector3d t_;
    /// Weight for residuals in case different error types or cameras with
    /// different numbers of detections are optimized simultaneously
    double weight_;
};

///
/// \brief The Projection_Error_Extrinsics_Brown class creates the cost functor
///                                              to calculate the residuals for
///                                              the extrinsics optimization via
///                                              ceres
///
class Projection_Error_Extrinsics_Brown{

public:

    ///
    /// \brief Projection_Error_Extrinsics_Brown is the default constructor
    /// \param p_0              detected 2D pixel position in first camera
    /// \param p_1              detected 2D pixel position in second camera
    /// \param P                associated 3D corner position in pattern
    ///                         coordinates
    /// \param params           intrinsic parameters of both cameras
    /// \param weight           for the residuals
    ///
    Projection_Error_Extrinsics_Brown(const Eigen::Vector2d& p_0,
                                const Eigen::Vector2d& p_1,
                                const Eigen::Vector3d& P,
                                const std::vector<Cam_Parameters>& params,
                                const bool& first_cam_is_ref = true,
                                const double& weight = 1.0){
        p_0_ = p_0;
        p_1_ = p_1;
        P_ = P;
        params_ = params;
        first_cam_is_ref_ = first_cam_is_ref;
        weight_ = weight;
    }

    ///
    /// \brief operator ()
    /// \param R_rod_cam    camera rotation Rodrigues vector
    /// \param t_cam        camera translation
    /// \param R_rod_P      pattern rotation Rodrigues vector
    /// \param t_P          pattern translation
    /// \param residuals
    /// \return
    ///
    template <typename T>
    bool operator()(const T* const R_rod_cam, const T* const t_cam,
                    const T* const R_rod_P, const T* const t_P,
                    T* residuals) const;

    ///
    /// \brief evaluate
    /// \param R_rod_cam
    /// \param t_cam
    /// \param R_rod_P
    /// \param t_P
    /// \return
    ///
    std::vector<double> evaluate(const double* R_rod_cam, const double* t_cam,
                                 const double* R_rod_P, const double* t_P) const;

private:
    /// detected 2D pixel position...
    Eigen::Vector2d p_0_;
    /// ...and correspondence in second camera
    Eigen::Vector2d p_1_;
    /// 3D pattern point in pattern coordinates
    Eigen::Vector3d P_;
    /// camera parameters
    std::vector<Cam_Parameters> params_;
    /// is used to determine whether the camera transformation has to be
    /// inverted
    bool first_cam_is_ref_;
    /// weight for residuals in case different error types or cameras with
    /// different numbers of detections are optimized simultaneously
    double weight_;
};

///
/// \brief The Projection_Error_Extrinsics_Rational class is the analogue to
///                                  Projection_Error_Extrinsics_Brown for the
///                                  rational radial distortion model
///
class Projection_Error_Extrinsics_Rational{

public:

    ///
    /// \brief Projection_Error_Extrinsics_Rational is the default constructor
    /// \param p_0              detected 2D pixel position in first camera
    /// \param p_1              detected 2D pixel position in second camera
    /// \param P                associated 3D corner position in pattern
    ///                         coordinates
    /// \param params           intrinsic parameters of both cameras
    /// \param weight           for the residuals
    ///
    Projection_Error_Extrinsics_Rational(const Eigen::Vector2d& p_0,
                                const Eigen::Vector2d& p_1,
                                const Eigen::Vector3d& P,
                                const std::vector<Cam_Parameters>& params,
                                const bool& first_cam_is_ref = true,
                                const double& weight = 1.0){
        p_0_ = p_0;
        p_1_ = p_1;
        P_ = P;
        params_ = params;
        first_cam_is_ref_ = first_cam_is_ref;
        weight_ = weight;
    }

    ///
    /// \brief operator ()
    /// \param R_rod_cam    camera rotation Rodrigues vector
    /// \param t_cam        camera translation
    /// \param R_rod_P      pattern rotation Rodrigues vector
    /// \param t_P          pattern translation
    /// \param residuals
    /// \return
    ///
    template <typename T>
    bool operator()(const T* const R_rod_cam, const T* const t_cam,
                    const T* const R_rod_P, const T* const t_P,
                    T* residuals) const;

    ///
    /// \brief evaluate
    /// \param R_rod_cam
    /// \param t_cam
    /// \param R_rod_P
    /// \param t_P
    /// \return
    ///
    std::vector<double> evaluate(const double* R_rod_cam, const double* t_cam,
                                 const double* R_rod_P, const double* t_P) const;

private:
    /// detected 2D pixel position...
    Eigen::Vector2d p_0_;
    /// ...and correspondence in second camera
    Eigen::Vector2d p_1_;
    /// 3D pattern point in pattern coordinates
    Eigen::Vector3d P_;
    /// camera parameters
    std::vector<Cam_Parameters> params_;
    /// is used to determine whether the camera transformation has to be
    /// inverted
    bool first_cam_is_ref_;
    /// weight for residuals in case different error types or cameras with
    /// different numbers of detections are optimized simultaneously
    double weight_;
};


///
/// \brief The Projection_Error_All class creates the cost functor to calculate
///                                 the residuals for the combined ext/intr
///                                 optimization via ceres using correspondences
///                                 between the two cameras
///
class Projection_Error_All{

public:

    ///
    /// \brief Projection_Error_All is the default constructor
    /// \param p_0              detected 2D pixel position in first camera
    /// \param p_1              detected 2D pixel position in second camera
    /// \param P                associated 3D corner position in pattern
    ///                         coordinates
    /// \param weight           for the residuals
    ///
    Projection_Error_All(const Eigen::Vector2d& p_0,
                         const Eigen::Vector2d& p_1,
                         const Eigen::Vector3d& P,
                         const double& weight = 1.0){
        p_0_ = p_0;
        p_1_ = p_1;
        P_ = P;
        weight_ = weight;
    }

    ///
    /// \brief operator ()
    /// \param K_1          contains fx, fy, cx, cy for camera 1
    /// \param dist_1       contains k1,...,k6,p1,p2 for camera 1
    /// \param K_2          contains fx, fy, cx, cy for camera 2
    /// \param dist_2       contains k1,...,k6,p1,p2 for camera 2
    /// \param R_rod_cam    camera rotation Rodrigues vector
    /// \param t_cam        camera translation
    /// \param R_rod_P      pattern rotation Rodrigues vector
    /// \param t_P          pattern translation
    /// \param residuals
    /// \return
    ///
    template <typename T>
    bool operator()(const T* const K_1, const T* const dist_1,
                    const T* const K_2, const T* const dist_2,
                    const T* const R_rod_cam, const T* const t_cam,
                    const T* const R_rod_P, const T* const t_P,
                    T* residuals) const;

private:
    /// 3D pattern point in pattern coordinates
    Eigen::Vector3d P_;
    /// detected 2D pixel position in first camera
    Eigen::Vector2d p_0_;
    /// detected 2D pixel position in second camera
    Eigen::Vector2d p_1_;
    /// weight for residuals in case different error types or cameras with
    /// different numbers of detections are optimized simultaneously
    double weight_;
};

///
/// \brief The Rational_Cost class creates the cost functor which is used to
///                                prevent the rational distortion polynomial
///                                from having a denominator root located within
///                                the sensor area, which leads to artifacts
///                                during the undistortion
///
class Rational_Cost{

public:

    ///
    /// \brief Rational_Cost
    /// \param min_r
    /// \param num_reprojection_residuals
    /// \param weight
    ///
    Rational_Cost(const double& min_r, const int& num_reprojection_residuals,
                  const double& weight = 1.0){
        min_r_ = min_r;
        num_reprojection_residuals_ = num_reprojection_residuals;
        weight_ = weight;
    }

    ///
    /// \brief operator ()
    /// \param dist
    /// \param residual
    /// \return
    ///
    template <typename T>
    bool operator()(const T* const dist, T* residual) const;

private:
    /// minimum distance at which radial distortion denominator polynomial is
    /// allowed to have a root
    double min_r_;

    ///
    int num_reprojection_residuals_;

    /// weight for residuals in case different error types or cameras with
    /// different numbers of detections are optimized simultaneously
    double weight_;
};

