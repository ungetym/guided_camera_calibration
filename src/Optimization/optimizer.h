#pragma once

#include "Optimization/pose_cost.h"

#include <nlopt.hpp>
#include <QObject>
#include <random>

///
/// \brief The Optimizer class
///
class Optimizer : public QObject{

    Q_OBJECT

public:
    ///
    /// \brief Optimizer is the default constructor
    ///
    Optimizer();

    ///
    /// \brief propertyListIntr
    /// \return
    ///
    std::vector<std::shared_ptr<Property> > propertyListIntr();

    ///
    /// \brief propertyListPose
    /// \return
    ///
    std::vector<std::shared_ptr<Property>> propertyListPose();

    ///
    /// \brief propertyListGeneral
    /// \return
    ///
    std::vector<std::shared_ptr<Property>> propertyListGeneral();

    ///
    /// \brief propertyListOverlayControl
    /// \return
    ///
    std::vector<std::shared_ptr<Property>> propertyListOverlayControl();

    ///
    /// \brief getProperties
    /// \return
    ///
    std::vector<std::shared_ptr<Property>> getProperties();

public slots:
    ///
    /// \brief setPattern
    /// \param pattern
    ///
    void setPattern(const Partition& pattern);

    ///
    /// \brief updateCameraParameters
    /// \param parameters
    ///
    void updateCameraParameters(const std::vector<Cam_Parameters>& parameters);

    ///
    /// \brief addDetection
    /// \param detection
    ///
    void addDetection(const std::vector<Detection> &detection);

    ///
    /// \brief calculateNextProposal
    ///
    void calculateNextProposal();

    ///
    /// \brief calculatePatternDistanceRange
    ///
    void calculatePatternDistanceRange();

private:

    ///
    /// \brief createProblem
    /// \return
    ///
    bool createProblem();

    ///
    /// \brief removeOrFlagOutliers
    /// \param remove
    /// \return
    ///
    bool removeOrFlagOutliers(const bool& remove = true);

    ///
    /// \brief startSingleParameterOptimizationRun
    /// \return
    ///
    bool startSingleParameterOptimizationRun();

    ///
    /// \brief startParameterOptimizationLoop
    /// \return
    ///
    bool startParameterOptimizationLoop();

private:

    /// 'shared' properties which are also shown in the optimization widget
    std::shared_ptr<Property_Selector> LM_distortion_model_;
    std::shared_ptr<Property_Spin> LM_max_iter_;
    std::shared_ptr<Property_Spin> LM_threads;
    std::shared_ptr<Property_Spin_F> LM_grad_tolerance;
    std::shared_ptr<Property_Spin> LM_num_tries_;
    std::shared_ptr<Property_Check> LM_log_reports_;

    std::shared_ptr<Property_Selector> pose_random_mode_;
    std::shared_ptr<Property_Spin> pose_num_random_;
    std::shared_ptr<Property_Spin> pose_num_optimized_;
    std::shared_ptr<Property_Spin> pose_num_iter_;

    std::shared_ptr<Property_Check> general_refine_intrinsics_;

    std::vector<std::shared_ptr<Property>> properties_intr_;
    std::vector<std::shared_ptr<Property>> properties_pose_;
    std::vector<std::shared_ptr<Property>> properties_general_;

    /// 'shared' properties for overlay control
    std::shared_ptr<Property_Slider> OC_scale_;
    std::shared_ptr<Property_Spin_F> OC_min_working_distance_;
    std::shared_ptr<Property_Spin_F> OC_max_working_distance_;
    double min_pattern_dist_;
    double max_pattern_dist_;

    std::vector<std::shared_ptr<Property>> properties_overlay_control_;

    std::vector<std::shared_ptr<Property>> all_properties_;

    /// Random sampler
    std::uniform_real_distribution<double> unif_angle_;
    std::uniform_real_distribution<double> unif_distance_;
    std::default_random_engine re_;

    /// Contains up to 6 sets of detection sets:
    /// detections_[0] = detections from random pattern poses for cam 0
    /// detections_[1] = detections from optimized pattern poses for cam 0
    /// detections_[2] = detections from random pattern poses for cam 1
    /// detections_[3] = detections from optimized pattern poses for cam 1
    /// detections_[4] = detections from random pattern poses for both cams
    /// detections_[5] = detections from optimized pattern poses for both cams
    std::vector<std::vector<Dedicated_Detection>> detection_lists_ = {};

    /// Target number of random poses as set at the start of the calibration
    int num_random_;
    /// Target number of optimized poses as set at the start of the calibration
    int num_optimized_;
    /// Lists of random pose indices for cam 0, cam 1 and both cameras combined
    /// which indicate the remaining random sectors to cover with further poses
    std::vector<std::vector<int>> random_idx_todos_ = {};

    ///
    std::vector<Cam_Parameters> cam_params_;
    bool waiting_for_cam_params_ = false;

    ///
    std::shared_ptr<Pose> current_pose_proposal_;

    /// The currently used pattern
    std::shared_ptr<Partition> pattern_ = nullptr;

    /// Ceres optimizer for the overall calibration procedure
    ceres::Problem problem_;
    ceres::Solver::Options options_;
    ceres::Solver::Summary summary_;
    double avg_error_ = -1.0;

    ///
    std::vector<Idx_Tuple> list_detection_cam_feature_idx_;
    ///
    std::vector<Idx_Pair> list_detection_idx_;

    ceres::Problem::EvaluateOptions options_eval_;
    ceres::CRSMatrix J_;
    Pose_Cost_Data pose_cost_data_;

    /// NLopt optimizer for the next best pose estimation
    nlopt::opt pose_optimizer_;
    bool options_set_ = false;


    /// The optimization mode determines, whether RGB intrinsics, IR intrinsics,
    /// the transformation between those two, or everything will be optimized
    int optimization_mode_ = OPT_MODE_RGB;
    /// The optimization mode determines, whether random or optimized poses are
    /// created
    int optimization_submode_ = OPT_SUBMODE_RANDOM_AREA;


signals:

    ///
    /// \brief updatedOptimizationMode
    /// \param mode
    /// \param submode
    ///
    void updatedOptimizationMode(const int& mode, const int& submode);

    ///
    /// \brief updatedPoseProposal
    /// \param pose
    ///
    void updatedPoseProposal(const Pose& pose);

    ///
    /// \brief updatedCameraParameters
    /// \param cam_params
    ///
    void updatedCameraParameters(const std::vector<Cam_Parameters>& cam_params);

    ///
    /// \brief log
    /// \param msg
    /// \param type
    ///
    void log(const QString& msg, const int& type);
};
