﻿#include "optimizer.h"
#include "UI/logger.h"

#include <Eigen/Geometry>
#include <QThread>

Optimizer::Optimizer(){
    unif_angle_ = std::uniform_real_distribution<double>(-1.0, 1.0);
    unif_distance_ = std::uniform_real_distribution<double>(0.0, 1.0);

    // Create shared properties
    LM_distortion_model_ = std::shared_ptr<Property_Selector>(new Property_Selector(nullptr, "Optimizer_Distortion_Model", "Distortion Model", {"Brown","Rational"}, 0));
    LM_max_iter_ = std::shared_ptr<Property_Spin>(new Property_Spin(nullptr, "Optimizer_LM_Max_Iter", "LM Maximum Iteration", 5, 10000, 100));
    LM_threads = std::shared_ptr<Property_Spin>(new Property_Spin(nullptr, "Optimizer_Num_Threads", "Number of Threads", 1, 8, 4));
    LM_grad_tolerance = std::shared_ptr<Property_Spin_F>(new Property_Spin_F(nullptr, "Optimizer_Gradient_Tolerance", "Gradient Tolerance", 0.0, 100000.0, 0.0));
    LM_num_tries_ = std::shared_ptr<Property_Spin>(new Property_Spin(nullptr, "Optimizer_Num_Opt_Runs", "Number of Optimization Runs", 1, 100, 5));
    LM_log_reports_ = std::shared_ptr<Property_Check>(new Property_Check(nullptr, "Optimizer_Log_Full_Report", "Log Full Optimization Reports", false));
    pose_random_mode_ = std::shared_ptr<Property_Selector>(new Property_Selector(nullptr, "Optimizer_Random_Proposal_Type", "Random Proposal Type", {"General Area", "Specific Pose"}, 0));
    pose_num_random_ = std::shared_ptr<Property_Spin>(new Property_Spin(nullptr, "Optimizer_Num_Random", "Number of Random Poses", 3, 100, 6));
    pose_num_optimized_ = std::shared_ptr<Property_Spin>(new Property_Spin(nullptr, "Optimizer_Num_Optimized", "Number of Optimized Poses", 3, 100, 8));
    pose_num_iter_ = std::shared_ptr<Property_Spin>(new Property_Spin(nullptr, "Optimizer_Num_Global_Opt_Steps", "Number of Global Optimization Steps", 1, 9999999, 1000));
    general_refine_intrinsics_ = std::shared_ptr<Property_Check>(new Property_Check(nullptr, "Optimizer_Refine_Intrinsics", "Refine Intrinsics", false));

    properties_intr_= {LM_distortion_model_, LM_max_iter_, LM_threads, LM_num_tries_, LM_grad_tolerance, LM_log_reports_};
    properties_pose_ = {pose_random_mode_, pose_num_random_, pose_num_optimized_, pose_num_iter_};
    properties_general_ = {general_refine_intrinsics_};

    all_properties_ = {LM_distortion_model_, LM_max_iter_, LM_threads,
                       LM_num_tries_, LM_grad_tolerance, LM_log_reports_,
                       pose_random_mode_, pose_num_random_, pose_num_optimized_,
                       pose_num_iter_,general_refine_intrinsics_};

    OC_scale_ = std::shared_ptr<Property_Slider>(new Property_Slider(nullptr, "Optimizer_OC_Random_scale", "Random Proposal Scale", 20, 100, 75));
    OC_min_working_distance_ = std::shared_ptr<Property_Spin_F>(new Property_Spin_F(nullptr, "Optimizer_OC_Min_Work_Dist", "Minimum Pattern Distance in m", 0.0, 100000.0, 0.8));
    OC_max_working_distance_ = std::shared_ptr<Property_Spin_F>(new Property_Spin_F(nullptr, "Optimizer_OC_Max_Work_Dist", "Maximum Pattern Distance in m", 0.0, 100000.0, 2.0));

    properties_overlay_control_ = {OC_scale_, OC_min_working_distance_, OC_max_working_distance_};

    // Configure ceres solver
    options_.trust_region_strategy_type = ceres::LEVENBERG_MARQUARDT;
    options_.linear_solver_type = ceres::DENSE_QR;
    options_.max_num_iterations = LM_max_iter_->value();
    options_.num_threads = LM_threads->value();
    options_.gradient_tolerance = LM_grad_tolerance->value();
    options_.minimizer_progress_to_stdout = false;
    options_eval_.num_threads = 1;
    options_eval_.apply_loss_function = true;

    // Configure nlopt solver
    pose_optimizer_ = nlopt::opt(nlopt::GN_ISRES, 6);

    // Connect optimization parameter widgets
    connect(pose_random_mode_.get(), &Property_Selector::valueChanged, [this](const int& value){
        if(optimization_submode_ != OPT_SUBMODE_OPTIMIZED){
            optimization_submode_ = (value == 0) ? OPT_SUBMODE_RANDOM_AREA : OPT_SUBMODE_RANDOM;
            emit updatedOptimizationMode(optimization_mode_, optimization_submode_);
        }
    });

    // Connect overlay control widgets
    connect(OC_min_working_distance_.get(), &Property_Spin_F::valueEditFinished,
            this, &Optimizer::calculatePatternDistanceRange);
    connect(OC_max_working_distance_.get(), &Property_Spin_F::valueEditFinished,
            this, &Optimizer::calculatePatternDistanceRange);
}

std::vector<std::shared_ptr<Property>> Optimizer::propertyListIntr(){
    return properties_intr_;
}

std::vector<std::shared_ptr<Property>> Optimizer::propertyListPose(){
    return properties_pose_;
}

std::vector<std::shared_ptr<Property>> Optimizer::propertyListGeneral(){
    return properties_general_;
}

std::vector<std::shared_ptr<Property>> Optimizer::propertyListOverlayControl(){
    return properties_overlay_control_;
}

std::vector<std::shared_ptr<Property>> Optimizer::getProperties(){
    return all_properties_;
}

void Optimizer::setPattern(const Partition& pattern){
    pattern_ = std::shared_ptr<Partition>(new Partition(pattern));

    // Calculate pattern distance range
    if(cam_params_.size() > 0){
        // Calculate pattern distance range
        calculatePatternDistanceRange();
    }
}

void Optimizer::updateCameraParameters(const std::vector<Cam_Parameters>& parameters){

    if(parameters.size() > 1){
        detection_lists_.resize(6);
        random_idx_todos_.resize(3);
    }
    else{
        detection_lists_.resize(2);
        random_idx_todos_.resize(1);
    }

    cam_params_ = parameters;
    for(Cam_Parameters& params: cam_params_){
        params.resetOptimizerParameters();
        params.distortion_model_ = LM_distortion_model_->index();
    }

    // If pattern is initialized before camera
    if(waiting_for_cam_params_){
        waiting_for_cam_params_ = false;
        calculateNextProposal();
    }

    // Calculate pattern distance range
    calculatePatternDistanceRange();
}

void Optimizer::addDetection(const std::vector<Detection>& detection){

    // Find correct detection list
    size_t list_idx = size_t((optimization_submode_ != OPT_SUBMODE_OPTIMIZED) ? 0 : 1);
    if(optimization_mode_ == OPT_MODE_IR){
        list_idx += 2;
    }
    else if(optimization_mode_ == OPT_MODE_TRAFO){
        list_idx += 4;
    }

    // Get list
    std::vector<Dedicated_Detection>& detections = detection_lists_[list_idx];

    // Save new detections
    if(optimization_submode_ != OPT_SUBMODE_OPTIMIZED){
        size_t pose_idx = random_idx_todos_[list_idx/2].back();
        random_idx_todos_[list_idx/2].pop_back();

        if(pose_idx < detections.size()){
            detections.insert(detections.begin() + pose_idx, Dedicated_Detection(detection, current_pose_proposal_, optimization_mode_));
        }
        else{
            detections.push_back(Dedicated_Detection(detection, current_pose_proposal_, optimization_mode_));
        }
    }
    else{
        detections.push_back(Dedicated_Detection(detection, current_pose_proposal_, optimization_mode_));
    }

    // Dismiss current detection in extrinsics mode if no correspondences have
    // been found
    if(optimization_mode_ == OPT_MODE_TRAFO && detections.back().num_correspondences_ == 0){
        detections.pop_back();
        calculateNextProposal();
        return;
    }

    // Only optimize if at least the initial pose_num_random_ poses were detected
    if(optimization_mode_ == OPT_MODE_RGB){
        if(detection_lists_[0].size() == num_random_){
            startParameterOptimizationLoop();
        }
    }
    else if(optimization_mode_ == OPT_MODE_IR){
        if(detection_lists_[2].size() == num_random_){
            startParameterOptimizationLoop();
        }
    }
    else{
        startParameterOptimizationLoop();
    }

    // Generate next pattern pose
    calculateNextProposal();
}

void Optimizer::calculateNextProposal(){

    if(cam_params_.size() == 0){
        waiting_for_cam_params_ = true;
        return;
    }

    // Check whether pattern is set
    if(!pattern_){
        return;
    }

    // Get number of random and optimized poses from GUI
    if(detection_lists_[0].size() == 0){
        optimization_submode_ = (pose_random_mode_->index() == 0) ? OPT_SUBMODE_RANDOM_AREA : OPT_SUBMODE_RANDOM;
        num_random_ = pose_num_random_->value();
        num_optimized_ = pose_num_optimized_->value();
        for(int i = 0; i < num_random_; i++){
            for(std::vector<int>& list : random_idx_todos_){
                list.push_back(i);
            }
        }
        // Initialize pattern distance range
        calculatePatternDistanceRange();

        emit updatedOptimizationMode(optimization_mode_, optimization_submode_);
    }

    // Function to check whether pattern is completely visible according to
    // current camera parameters
    auto checkOuterCorners = [this](const Eigen::Matrix4d& pose_proposal){
        for(const Eigen::Vector3d& corner : this->pattern_->corners_){
            Eigen::Vector4d p_cam = pose_proposal *
                    Eigen::Vector4d(corner[0], corner[1], corner[2], 1.0);

            size_t cam_idx = (optimization_mode_ == OPT_MODE_IR) ? 1 : 0;

            Eigen::Vector2d p = this->cam_params_[cam_idx].projectCamToPixel(
                        Eigen::Vector3d(p_cam[0],p_cam[1],p_cam[2]));

            if ((p[0]< 0.0) || (p[1]< 0.0)
                    || (p[0] > double(this->cam_params_[cam_idx].resolution_x-1))
                    || (p[1] > double(this->cam_params_[cam_idx].resolution_y-1))){
                return false;
            }
        }
        return true;
    };

    auto calcScale = [this](const Eigen::Matrix4d& pose_proposal){

        size_t cam_idx = (optimization_mode_ == OPT_MODE_IR) ? 1 : 0;
        double res_x = double(this->cam_params_[cam_idx].resolution_x);
        double res_y = double(this->cam_params_[cam_idx].resolution_y);

        double max_x = 0.0;
        double min_x = res_x;
        double min_y = res_y;
        double max_y = 0.0;

        for(const Eigen::Vector3d& corner : this->pattern_->corners_){
            Eigen::Vector4d p_cam = pose_proposal *
                    Eigen::Vector4d(corner[0], corner[1], corner[2], 1.0);

            Eigen::Vector2d p = this->cam_params_[cam_idx].projectCamToPixel(
                        Eigen::Vector3d(p_cam[0],p_cam[1],p_cam[2]));

            max_x = std::max(max_x, p[0]);
            max_y = std::max(max_y, p[1]);
            min_x = std::min(min_x, p[0]);
            min_y = std::min(min_y, p[1]);
        }

        return std::max( (max_x-min_x)/res_x, (max_y-min_y)/res_y);
    };

    Eigen::Matrix4d pose_proposal = Eigen::Matrix4d::Identity();

    size_t cam_idx = (optimization_mode_ == OPT_MODE_IR) ? 1 : 0;

    // Calculate number of valid detections for current optimization mode
    int num_valid_poses = 0;

    if(optimization_mode_ == OPT_MODE_RGB){
        num_valid_poses = (optimization_submode_ != OPT_SUBMODE_OPTIMIZED) ?
                    detection_lists_[0].size() : detection_lists_[0].size() + detection_lists_[1].size();
    }
    else if(optimization_mode_ == OPT_MODE_IR){
        num_valid_poses = (optimization_submode_ != OPT_SUBMODE_OPTIMIZED) ?
                    detection_lists_[2].size() : detection_lists_[2].size() + detection_lists_[3].size();
    }
    else{
        for(const std::vector<Dedicated_Detection>& list : detection_lists_){
            for(const Dedicated_Detection& detection_pair : list){
                if(detection_pair.used_for_mode_[optimization_mode_]){
                    num_valid_poses++;
                }
            }
        }
    }

    // Set constraints
    if(num_valid_poses == num_random_ && !options_set_){

        pose_optimizer_.add_inequality_constraint(Pose_Cost::constraints, &pose_cost_data_);
        options_set_ = true;
    }

    //
    if(optimization_submode_ == OPT_SUBMODE_OPTIMIZED){
        // Pattern pose optimization via global search

        pose_optimizer_.set_min_objective(Pose_Cost::evaluate, &pose_cost_data_);
        double limit = M_PIf64/6.0;
        double limit_x = 0.5 * (max_pattern_dist_ * cam_params_[cam_idx].resolution_x / cam_params_[cam_idx].fx_ - pattern_->width_in_mm_);
        double limit_y = 0.5 * (max_pattern_dist_ * cam_params_[cam_idx].resolution_y / cam_params_[cam_idx].fy_ - pattern_->height_in_mm_);

        pose_optimizer_.set_lower_bounds({-limit,-limit,-limit,-limit_x,-limit_y, min_pattern_dist_});
        pose_optimizer_.set_upper_bounds({limit,limit,limit,limit_x,limit_y, max_pattern_dist_});

        std::vector<double> pose_to_optimize = {0.0, 0.0, 0.0, -pattern_->center_[0], -pattern_->center_[1], 0.2f * max_pattern_dist_+ 0.8f * min_pattern_dist_};

        pose_optimizer_.set_maxeval(pose_num_iter_->value());
        double trace;
        nlopt::result success = pose_optimizer_.optimize(pose_to_optimize, trace);

        Eigen::Vector3d axis(pose_to_optimize[0],
                pose_to_optimize[1], pose_to_optimize[2]);
        Eigen::AngleAxisd aa(axis.norm(), axis/axis.norm());
        pose_proposal.block(0,0,3,3) = aa.toRotationMatrix();
        pose_proposal.block(0,3,3,1) = Eigen::Vector3d(
                    pose_to_optimize[3] * pose_to_optimize[5] / max_pattern_dist_,
                pose_to_optimize[4] * pose_to_optimize[5] / max_pattern_dist_,
                pose_to_optimize[5]);

        for(int i = 0; i < 5 && !checkOuterCorners(pose_proposal); i++){
            pose_to_optimize = {0.0, 0.0, 0.0, -pattern_->center_[0], -pattern_->center_[1], 0.2f * max_pattern_dist_+ 0.8f * min_pattern_dist_};
            pose_optimizer_.set_maxeval(int(float(pose_num_iter_->value())/float(std::pow(2.0f, i))));
            success = pose_optimizer_.optimize(pose_to_optimize, trace);

            Eigen::Vector3d axis(pose_to_optimize[0],
                    pose_to_optimize[1], pose_to_optimize[2]);
            Eigen::AngleAxisd aa(axis.norm(), axis/axis.norm());
            pose_proposal.block(0,0,3,3) = aa.toRotationMatrix();
            pose_proposal.block(0,3,3,1) = Eigen::Vector3d(
                        pose_to_optimize[3] * pose_to_optimize[5] / max_pattern_dist_,
                    pose_to_optimize[4] * pose_to_optimize[5] / max_pattern_dist_,
                    pose_to_optimize[5]);
        }

        if(!checkOuterCorners(pose_proposal)){
            ERROR("Unable to find optimized pose within FoV.");
        }
        else{
            STATUS("New pose calculated. "
                   + QString::number(pose_cost_data_.num_opt_steps - pose_cost_data_.num_failed_opt_steps)
                   + "/" + QString::number(pose_cost_data_.num_opt_steps)+" within constraints.");
        }

    }
    else{ // Generate a random pattern in one of the num_random image sectors

        // Initial angle between x-axis and vector from image center to pattern
        // center
        double initial_angle = std::abs(
                    std::atan(cam_params_[cam_idx].resolution_y /
                              (2.0 * cam_params_[cam_idx].resolution_x)));

        // Get image sector to place the pattern in
        int random_list_idx = 0;
        if(optimization_mode_ == OPT_MODE_TRAFO){
            random_list_idx = 2;
        }
        else{
            random_list_idx = cam_idx;
        }
        int pose_idx = random_idx_todos_[random_list_idx].back();

        // Calculate angle between x-axis and vector from image center to
        // pattern center
        double angle = initial_angle + double(pose_idx) * 2.0 * M_PI / double(num_random_);

        Eigen::Vector2d direction (std::cos(angle) * cam_params_[cam_idx].resolution_x/cam_params_[cam_idx].resolution_y,
                                   std::sin(angle));
        direction /= direction.norm();

        auto mapAngle = [](const double& angle){
            return angle / std::abs(angle) * M_PIf64 / 6.0 *
                    std::pow(std::abs(angle), 1.0/8.0);
        };

        // Randomly generate new poses until the pattern is completely visible
        bool valid = false;
        while(!valid){
            double angle_1 = mapAngle(unif_angle_(re_));
            double angle_2 = mapAngle(unif_angle_(re_));
            // A rotation around the pattern's z-axis is not as relevant at this
            // stage and harder to match for the user
            double angle_3 = mapAngle(unif_angle_(re_))/6.0;
            // Prefer distances closer to the minimum distance to cover a larger
            // part of the camera's FoV
            double dist = min_pattern_dist_ + std::pow(unif_distance_(re_),6.0) * (max_pattern_dist_-min_pattern_dist_);

            Eigen::Matrix3d rotation = Eigen::AngleAxisd(angle_1, Eigen::Vector3d::UnitX()).matrix()
                    * Eigen::AngleAxisd(angle_2, Eigen::Vector3d::UnitY()).matrix()
                    * Eigen::AngleAxisd(angle_3, Eigen::Vector3d::UnitZ()).matrix();

            pose_proposal.block(0,0,3,3) = rotation;
            pose_proposal.block(0,3,3,1) = - rotation * pattern_->center_;
            pose_proposal(2,3) += dist;

            valid = checkOuterCorners(pose_proposal);

            if(valid){
                // Scale the target until the user defined scale of the max FoV
                // is reached
                double target_scale = double(OC_scale_->value())/100.0;

                double current_scale = calcScale(pose_proposal);
                double dist_step = - dist / 100.0;
                if(current_scale > target_scale){
                    dist_step *= -1.0;
                }

                // Move the pattern pose as close to the camera as possible
                while(valid && std::abs(current_scale-target_scale) > 0.05){
                    pose_proposal(2,3) += dist_step;
                    current_scale = calcScale(pose_proposal);
                    valid = checkOuterCorners(pose_proposal);
                }
                if(!valid){
                    pose_proposal(2,3) -= dist_step;
                    valid = true;
                }

                // Move the pattern pose as close to the camera's FOV bounding
                // box as possible
                for(int n = 10; n >= 0; n--){
                    double step_size = std::pow(2.0, double(n));
                    while(valid){
                        pose_proposal(0,3) += step_size * direction[0];
                        pose_proposal(1,3) += step_size * direction[1];
                        valid = checkOuterCorners(pose_proposal);
                    }
                    pose_proposal(0,3) -= step_size * direction[0];
                    pose_proposal(1,3) -= step_size * direction[1];
                    valid = true;
                }
            }
        }
    }

    // Save proposal as initial pose for next detected pattern
    current_pose_proposal_ = std::shared_ptr<Pose>(new Pose(pose_proposal));

    emit updatedPoseProposal(*current_pose_proposal_);
}

void Optimizer::calculatePatternDistanceRange(){

    if(pattern_ && cam_params_.size() > 0){
        size_t cam_idx = (optimization_mode_ == OPT_MODE_IR) ? 1 : 0;

        // User set working range in mm
        double user_min = 1000.0 * OC_min_working_distance_->value();
        double user_max = 1000.0 * OC_max_working_distance_->value();

        // Detectable minimum distance is given by assuming the pattern exactly
        // fills the whole image
        double detectable_min = pattern_->width_in_mm_ * cam_params_[cam_idx].fx_ / cam_params_[cam_idx].resolution_x;
        // The detectable maximum distance is defined by the patterns
        // min_mm_per_pixel_ value
        double detectable_max = cam_params_[cam_idx].fx_ * pattern_->min_mm_per_pixel_;

        min_pattern_dist_ = std::max(detectable_min, user_min);
        max_pattern_dist_ = std::min(detectable_max, user_max);

        if(detectable_max < user_min){
            WARN("Due to its small size, the pattern's maximum detection "
                 "distance is smaller than the set minimum working distance. "
                 "Please increase the pattern size by printing it in a larger "
                 "format or decrease the minimum working distance.");

            WARN("The minimum working range is temporarily decreased.");
            min_pattern_dist_ = (detectable_min + detectable_max) / 2.0;
        }

        if(detectable_min > user_max){
            WARN("Due to its large size, the pattern's minimum detection "
                 "distance is larger than the set maximum working distance. "
                 "Please decrease the pattern size by printing it in a smaller "
                 "format or increase the minimum working distance.");

            WARN("The maximum working range is temporarily increased.");
            max_pattern_dist_ = (detectable_min + detectable_max) / 2.0;
        }
    }
}

bool Optimizer::createProblem(){
    // Create ceres problem
    problem_ = ceres::Problem();
    ceres::LossFunction* loss_function = new ceres::HuberLoss(1.0);
    list_detection_cam_feature_idx_.clear();
    list_detection_idx_.clear();

    // Add residual blocks and bounds according to optimization mode
    if(optimization_mode_ == OPT_MODE_RGB || optimization_mode_ == OPT_MODE_IR){

        // Add intrinsic residuals for single camera
        int num_intr_residuals = 0;
        size_t cam_idx = (optimization_mode_ == OPT_MODE_IR) ? 1 : 0;
        std::vector<size_t> detection_list_idc;
        if(optimization_mode_ == OPT_MODE_IR){
            detection_list_idc = {2, 3};
        }
        else{
            detection_list_idc = {0, 1};
        }


        for(const size_t& list_idx : detection_list_idc){
            std::vector<Dedicated_Detection>& detection_list = detection_lists_[list_idx];
            for(size_t detect_idx = 0; detect_idx < detection_list.size(); detect_idx++){
                const Dedicated_Detection& dedicated_detection = detection_list[detect_idx];
                if(dedicated_detection.used_for_mode_[optimization_mode_]){
                    const std::vector<Detection>& detection = dedicated_detection.detection_;
                    for(size_t feature_idx = 0; feature_idx < detection[cam_idx]->size(); feature_idx++){
                        Feature_Point& fp = (*(detection[cam_idx]))[feature_idx];
                        ceres::CostFunction* cost_function = nullptr;
                        if(cam_params_[cam_idx].distortion_model_ == CALIB_BROWN){
                            cost_function = new ceres::AutoDiffCostFunction<Projection_Error_Intrinsics_Brown, 2, 4, 5, 3, 3>(
                                        new Projection_Error_Intrinsics_Brown(fp.pixel(),
                                                                              fp.patternPoint(),
                                                                              cam_params_[cam_idx].R_rod_,
                                                                              cam_params_[cam_idx].t_));
                        }
                        else{
                            cost_function = new ceres::AutoDiffCostFunction<Projection_Error_Intrinsics_Rational, 2, 4, 8, 3, 3>(
                                        new Projection_Error_Intrinsics_Rational(fp.pixel(),
                                                                                 fp.patternPoint(),
                                                                                 cam_params_[cam_idx].R_rod_,
                                                                                 cam_params_[cam_idx].t_));
                        }
                        problem_.AddResidualBlock(cost_function, loss_function,
                                                  cam_params_[cam_idx].opt_K_.data(),
                                                  cam_params_[cam_idx].opt_dist_.data(),
                                                  dedicated_detection.pose_->opt_R_rod_.data(),
                                                  dedicated_detection.pose_->opt_t_.data());
                        num_intr_residuals++;
                        list_detection_cam_feature_idx_.push_back(Idx_Tuple(list_idx, detect_idx, cam_idx, feature_idx));
                    }
                    list_detection_idx_.push_back(Idx_Pair(list_idx, detect_idx));
                }
            }
        }
        // Add residuals for rational polynomial
        //        ceres::CostFunction* cost_function = new ceres::AutoDiffCostFunction<Rational_Cost, 1, 8>(
        //                    new Rational_Cost(20.0, num_intr_residuals, 10.0));
        //        problem_.AddResidualBlock(cost_function, nullptr, cam_params_[cam_idx].opt_dist_.data());

        // Set parameter bounds to prevent rational distortion coefficients
        // from blowing up
        int num_distortion_params = (cam_params_[cam_idx].distortion_model_ == CALIB_BROWN) ? 3 : 6;
        for(int i = 0; i < num_distortion_params; i++){
            problem_.SetParameterUpperBound(cam_params_[cam_idx].opt_dist_.data(), i, 10.0);
            problem_.SetParameterLowerBound(cam_params_[cam_idx].opt_dist_.data(), i, -10.0);
        }

    }

    else if(optimization_mode_ == OPT_MODE_TRAFO){
        // Camera intrinsics are static, only the pose is optimized
        size_t cam_idx = (optimization_mode_ == OPT_MODE_IR) ? 1 : 0;
        for(size_t list_idx = 0; list_idx < detection_lists_.size(); list_idx++){
            std::vector<Dedicated_Detection>& detection_list = detection_lists_[list_idx];
            for(size_t detect_idx = 0; detect_idx < detection_list.size(); detect_idx++){
                const Dedicated_Detection& dedicated_detection = detection_list[detect_idx];
                if(dedicated_detection.used_for_mode_[optimization_mode_]){
                    const std::vector<Detection>& detection = dedicated_detection.detection_;
                    for(size_t feature_idx = 0; feature_idx < detection[0]->size(); feature_idx++){
                        Feature_Point& fp = (*(detection[0]))[feature_idx];
                        if(fp.correspondence_ > -1){
                            Feature_Point& fp_2 = (*(detection[1]))[fp.correspondence_];
                            ceres::CostFunction* cost_function = nullptr;
                            if(cam_params_[cam_idx].distortion_model_ == CALIB_BROWN){
                                cost_function = new ceres::AutoDiffCostFunction<Projection_Error_Extrinsics_Brown, 4, 3, 3, 3, 3>(
                                            new Projection_Error_Extrinsics_Brown(fp.pixel(),
                                                                                  fp_2.pixel(),
                                                                                  fp.patternPoint(),
                                                                                  cam_params_));
                            }
                            else{
                                cost_function = new ceres::AutoDiffCostFunction<Projection_Error_Extrinsics_Rational, 4, 3, 3, 3, 3>(
                                            new Projection_Error_Extrinsics_Rational(fp.pixel(),
                                                                                     fp_2.pixel(),
                                                                                     fp.patternPoint(),
                                                                                     cam_params_));
                            }

                            problem_.AddResidualBlock(cost_function, loss_function,
                                                      cam_params_[1].opt_R_rod_.data(),
                                    cam_params_[1].opt_t_.data(),
                                    dedicated_detection.pose_->opt_R_rod_.data(),
                                    dedicated_detection.pose_->opt_t_.data());
                            list_detection_cam_feature_idx_.push_back(Idx_Tuple(list_idx, detect_idx, 0, feature_idx));


                            //                            // TODO: Remove debug code
                            //                            Projection_Error_Extrinsics pe = Projection_Error_Extrinsics(fp.pixel(),
                            //                                                                                         fp_2.pixel(),
                            //                                                                                         fp.patternPoint(),
                            //                                                                                         cam_params_);

                            //                            std::vector<double> residuals = pe.evaluate(                                                  cam_params_[1].opt_R_rod_.data(),
                            //                                    cam_params_[1].opt_t_.data(),
                            //                                    dedicated_detection.pose_->opt_R_rod_.data(),
                            //                                    dedicated_detection.pose_->opt_t_.data());

                            //                            std::cerr << residuals[0] << " / " << residuals[1]
                            //                                                      << " / " << residuals[2]
                            //                                                      << " / " << residuals[3] << std::endl;
                        }
                    }
                    list_detection_idx_.push_back(Idx_Pair(list_idx, detect_idx));
                }
            }
        }
    }

    return true;
}

bool Optimizer::removeOrFlagOutliers(const bool& remove){

    size_t num_res_per_feature = (optimization_mode_ == OPT_MODE_TRAFO) ? 4 : 2;

    // Get the residuals
    std::vector<double> residuals = std::vector<double>(num_res_per_feature * list_detection_cam_feature_idx_.size(), 0.0);
    problem_.Evaluate(options_eval_, nullptr, &residuals, nullptr, nullptr);

    // Find outliers
    int num_outliers = 0;
    for(int feature_idx = list_detection_cam_feature_idx_.size() - 1; feature_idx >= 0; feature_idx--){
        bool is_outlier = false;
        for(size_t res_idx = 0; res_idx < num_res_per_feature; res_idx++){
            if(std::abs(residuals[num_res_per_feature * feature_idx + res_idx]) > 10.0){
                is_outlier = true;
                num_outliers++;
                break;
            }
        }

        // Remove outliers or flag dedicated detection as unused for current
        // optimization mode
        if(is_outlier){
            int list_idx = list_detection_cam_feature_idx_[feature_idx].a_;
            int detect_idx = list_detection_cam_feature_idx_[feature_idx].b_;
            Dedicated_Detection& detection = detection_lists_[list_idx][detect_idx];

            if(remove){
                int cam_idx = list_detection_cam_feature_idx_[feature_idx].c_;
                int fp_idx = list_detection_cam_feature_idx_[feature_idx].d_;
                detection.removeDetectedPoint(fp_idx, cam_idx);
            }
            else{
                // TODO: single outlier leads to whole detection not used!
                // Per-fp flags can prevent this
                detection.used_for_mode_[optimization_mode_] = false;
            }
        }
    }

    STATUS("Found and removed/flagged "+QString::number(num_outliers)+" outliers.");

    // Check for degenerated detections
    int num_degnerated = 0;
    int num_removed_degenerated = 0;
    for(int idx = list_detection_idx_.size()-1; idx >= 0; idx--){

        int list_idx = list_detection_idx_[idx].a_;
        int detect_idx = list_detection_idx_[idx].b_;
        int cam_idx = (optimization_mode_ == OPT_MODE_TRAFO) ? -1 : optimization_mode_;

        if(detection_lists_[list_idx][detect_idx].isDegeneratedCase(optimization_mode_, false, cam_idx)){
            num_degnerated++;
            if(cam_idx != -1){
                if(list_idx == cam_idx + (optimization_submode_ != OPT_SUBMODE_OPTIMIZED) ? 0 : 1){
                    // Remove detection completely
                    detection_lists_[list_idx].erase(detection_lists_[list_idx].begin() + detect_idx);
                    num_removed_degenerated++;

                    if(optimization_submode_ != OPT_SUBMODE_OPTIMIZED){
                        // In the case of a random pose based detection being
                        // deleted add the image sector to the todo list
                        random_idx_todos_[cam_idx].push_back(detect_idx);
                    }
                }
            }
        }
    }

    STATUS("Found "+QString::number(num_degnerated)+" degenerated detections and removed "+
           QString::number(num_removed_degenerated)+" of them.");

    return true;
}

bool Optimizer::startSingleParameterOptimizationRun(){
    // Start solver and print summary to terminal
    ceres::Solve(options_, &problem_, &summary_);

    if(LM_log_reports_ -> value()){
        STATUS(QString::fromStdString(summary_.FullReport()));
    }
    double error_per_residual = summary_.final_cost / double(summary_.num_residuals_reduced);
    if(avg_error_ < 0.0){
        avg_error_ = error_per_residual;
    }

    if(summary_.termination_type == ceres::CONVERGENCE){
        if(error_per_residual > 2.0 && error_per_residual > 2.0 * avg_error_){
            STATUS("Optimization converged with error increasing by more than 100%. Retry.");
            return false;
        }
        STATUS("Successful optimization with mean error (MSE): "
               + QString::number(error_per_residual));
        avg_error_ = error_per_residual;
    }
    else{
        STATUS("Optimization failed: LM algorithm did not converge. "
               "Retry with randomized initialization.");
        return false;
    }

    return true;
}

bool Optimizer::startParameterOptimizationLoop(){
    bool success;

    // Set distortion model
    for(Cam_Parameters& params: cam_params_){
        params.distortion_model_ = LM_distortion_model_->index();
    }

    // Set current LM options
    options_.max_num_iterations = LM_max_iter_->value();
    options_.num_threads = LM_threads->value();
    options_.gradient_tolerance = LM_grad_tolerance->value();

    // Start loop - in each iteration the initial values are slightly randomized
    // to prevent the LM solver from staying at the closest local minimum
    for(int try_idx = 0; try_idx < LM_num_tries_->value(); try_idx++){

        for(Cam_Parameters& params: cam_params_){
            params.resetOptimizerParameters();
        }
        if(try_idx > 0){
            // Randomize the starting values after first failed iteration
            for(Cam_Parameters& params: cam_params_){
                params.randomizeOptimizerParameters(optimization_mode_);
            }
        }

        // Define the residuals
        createProblem();

        // Accept optimized results if successfully solved
        success = startSingleParameterOptimizationRun();
        if(success){
            for(const Idx_Pair& idx : list_detection_idx_){
                detection_lists_[idx.a_][idx.b_].pose_->acceptOptimizationParameters();
            }
            if(optimization_mode_ != OPT_MODE_TRAFO){
                size_t cam_idx = (optimization_mode_ == OPT_MODE_IR) ? 1 : 0;
                cam_params_[cam_idx].acceptOptimized();
            }
            else{
                for(Cam_Parameters& params: cam_params_){
                    params.acceptOptimized();
                }
            }
            break;
        }
    }

    // Reset all parameters if all optimization runs failed
    if(!success){
        STATUS("All optimization runs failed. Removing faulty detection.");
        for(const Idx_Pair& idx : list_detection_idx_){
            detection_lists_[idx.a_][idx.b_].pose_->resetOptimizationParameters();
        }
        for(Cam_Parameters& params: cam_params_){
            params.resetOptimizerParameters();
        }
        detection_lists_[list_detection_idx_.back().a_].pop_back();

        return success;
    }
    else{
        removeOrFlagOutliers(optimization_mode_ != OPT_MODE_TRAFO);
    }

    // Decide upon next optimization mode
    bool mode_changed = false;
    if(cam_params_.size() == 1){
        if(optimization_mode_ == OPT_MODE_RGB){
            if(detection_lists_[0].size() == num_random_){
                if(detection_lists_[1].size() == 0){
                    // Switch to optimized mode
                    STATUS("Switching from random to optimized pattern pose proposals.");
                    optimization_submode_ = OPT_SUBMODE_OPTIMIZED;
                    emit updatedOptimizationMode(optimization_mode_, optimization_submode_);
                }
            }
        }
    }
    else if(cam_params_.size() > 1){

        if(optimization_mode_ == OPT_MODE_RGB){
            if(detection_lists_[0].size() == num_random_){
                if(detection_lists_[1].size() == 0){
                    // Switch to optimized mode
                    STATUS("Switching from random to optimized pattern pose proposals.");
                    optimization_submode_ = OPT_SUBMODE_OPTIMIZED;
                    emit updatedOptimizationMode(optimization_mode_, optimization_submode_);
                }
                else if(detection_lists_[1].size() >= num_optimized_){
                    // Switch to IR intrinsics calibration after optimized poses
                    optimization_mode_ = OPT_MODE_IR;
                    optimization_submode_ = (pose_random_mode_->index() == 1) ? OPT_SUBMODE_RANDOM : OPT_SUBMODE_RANDOM_AREA;
                    emit updatedOptimizationMode(optimization_mode_, optimization_submode_);
                    avg_error_ = -1.0;
                    STATUS("Switching to IR intrinsics optimization");
                    mode_changed = true;
                }
            }
        }
        else if(optimization_mode_ == OPT_MODE_IR){
            if(detection_lists_[2].size() == num_random_){
                if(detection_lists_[3].size() == 0){
                    // Switch to optimized mode
                    STATUS("Switching from random to optimized pattern pose proposals.");
                    optimization_submode_ = OPT_SUBMODE_OPTIMIZED;
                    emit updatedOptimizationMode(optimization_mode_, optimization_submode_);
                }
                else if(detection_lists_[3].size() >= num_optimized_){
                    // switch to trafo calibration after IR optimized poses
                    optimization_mode_ = OPT_MODE_TRAFO;
                    optimization_submode_ = (pose_random_mode_->index() == 1) ? OPT_SUBMODE_RANDOM : OPT_SUBMODE_RANDOM_AREA;
                    emit updatedOptimizationMode(optimization_mode_, optimization_submode_);
                    avg_error_ = -1.0;
                    STATUS("Switching to RGB->IR transformation optimization.");
                    mode_changed = true;
                }
            }
        }
        else if((detection_lists_[4].size() >= num_random_) /*&& (detection_lists_[5].size() >= num_optimized_)*/){
            // toggle between calibration modes after additional optimized
            // poses
            optimization_mode_ = OPT_MODE_RGB;
            emit updatedOptimizationMode(optimization_mode_, optimization_submode_);
            avg_error_ = -1.0;
            STATUS("Switching to RGB intrinsics optimization.");
            mode_changed = true;
        }
    }
    // Calculate and store Jacobi matrix for next parameter optimization
    if(mode_changed){
        if(optimization_mode_ == OPT_MODE_TRAFO){

            STATUS("Using intrinsic detections for initial extrinsics optimization");

            // On mode change, the problem is defined using different cost functions
            // resulting in a different Jacobi matrix. Accordingly, the problem has
            // to be reinitialized.
            startParameterOptimizationLoop();
        }
    }
    else{
        size_t cam_idx = (optimization_mode_ == OPT_MODE_IR) ? 1 : 0;
        cam_params_[cam_idx].rmse_ = std::sqrt(avg_error_);
        cam_params_[cam_idx].trace_ = pose_cost_data_.getTrace();
        cam_params_[cam_idx].resetOptimizerParameters();
        // Get the Jacobi matrix
        problem_.Evaluate(options_eval_, nullptr, nullptr, nullptr, &J_);
        // TODO: Only update J instead of creating a new Pose_Cost_Data instance
        pose_cost_data_ = Pose_Cost_Data(J_, pattern_->corners_,
                                         pattern_->outer_board_corners_,
                                         cam_params_,
                                         LM_distortion_model_->index(),
                                         optimization_mode_,
                                         min_pattern_dist_,
                                         max_pattern_dist_);

    }

    if(success){
        emit updatedCameraParameters(cam_params_);
    }
    return success;
}
