#include "camera_feed.h"
#include "ui_camera_feed.h"

#include <QThread>

Camera_Feed::Camera_Feed(QWidget *parent) :
    QWidget(parent),
    ui_(new Ui::Camera_Feed)
{
    ui_->setupUi(this);
    ui_->frame_properties->setHidden(true);
    ui_->group_overlay->setHidden(true);

    mirrored_ = std::shared_ptr<Property_Check>(new Property_Check(nullptr, "Camera_Feed_Mirrored", "Mirrored", true, false, "", false, false));
    QGridLayout* layout = static_cast<QGridLayout*>(ui_->group_feed->layout());
    layout->addWidget(mirrored_.get(), 1, 0);

    advanced_ = std::shared_ptr<Property_Check>(new Property_Check(nullptr, "Camera_Feed_Advanced", "Advanced Control", false, false, "", true, false));
    layout = static_cast<QGridLayout*>(ui_->group_control->layout());
    layout->addWidget(advanced_.get(), 0, 4);
    advanced_->setHidden(true);

    feature_pen_ = std::shared_ptr<QPen>(new QPen(QBrush(QColor(0,255,0,255)),4));

    box_pen_ = std::shared_ptr<QPen>(new QPen(QBrush(QColor(255,128,0,255)),9));
    box_pen_accepted_ = std::shared_ptr<QPen>(new QPen(QBrush(QColor(0,255,0,255)),9));

    axes_pens_.push_back(std::shared_ptr<QPen>(new QPen(QBrush(QColor(255,0,0,255)),6)));
    axes_pens_.push_back(std::shared_ptr<QPen>(new QPen(QBrush(QColor(0,255,0,255)),6)));
    axes_pens_.push_back(std::shared_ptr<QPen>(new QPen(QBrush(QColor(100,150,255,255)),6)));

    connect(ui_->button_start, &QPushButton::clicked, this, &Camera_Feed::camStartRequested);
    connect(ui_->button_pause, &QPushButton::clicked, this, &Camera_Feed::camPauseRequested);
    connect(ui_->button_stop, &QPushButton::clicked, this, &Camera_Feed::camStopRequested);
    connect(ui_->button_next_pose, &QPushButton::clicked, this, &Camera_Feed::requestNextPattern);
    connect(advanced_.get(), &Property_Check::valueChanged, [this](const bool& checked){
        ui_->frame_properties->setHidden(!checked);}
    );
    connect(mirrored_.get(), &Property_Check::valueChanged, ui_->feed_view, &Graphics_View::toggleMirrored);
    ui_->feed_view->toggleMirrored();
    connect(ui_->button_fullscreen, &QPushButton::clicked, [this](){
        if(ui_->group_feed->parent()){
            feed_window_flags_ = ui_->group_feed->windowFlags();
            feed_size_ = ui_->group_feed->size();
            ui_->group_feed->setParent(nullptr);
            ui_->group_feed->setWindowFlags(Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);
            ui_->group_feed->showMaximized();
            ui_->group_feed->show();
        }
        else{
            ui_->group_feed->setParent(this);
            ui_->group_feed->resize(feed_size_);
            ui_->group_feed->overrideWindowFlags(feed_window_flags_);
            QGridLayout* layout = static_cast<QGridLayout*>(this->layout());
            layout->addWidget(ui_->group_feed, 0, 0);
            ui_->group_feed->show();
            show();
        }

    });

}

Camera_Feed::~Camera_Feed(){
    // unparent property widgets to avoid double deletion
    for(const std::shared_ptr<QWidget>& property : widget_list_){
        property->setParent(nullptr);
    }
    for(const std::shared_ptr<QWidget>& property : widget_list_OC_){
        property->setParent(nullptr);
    }
    delete ui_;
}

void Camera_Feed::setImage(std::shared_ptr<const Frame>& frame){
    if(mode_ == 1){//ir intrinsics optimization mode
        ui_->feed_view->setImage(frame->ir_);
    }
    else{//in all other optimization modes, the rgb image will be shown
        ui_->feed_view->setImage(frame->rgb_);
    }
}

void Camera_Feed::showDetectedPattern(const Detection_Frame& detection){

    size_t cam_idx = (mode_ == OPT_MODE_IR) ? 1 : 0;

    for(size_t i = feature_points_.size(); i < detection.points_[cam_idx]->size(); i++){
        std::shared_ptr<QGraphicsEllipseItem> circle = std::shared_ptr<QGraphicsEllipseItem>(new QGraphicsEllipseItem(0,0,15,15));
        circle->setPen(*feature_pen_);
        feature_points_.push_back(circle);
        ui_->feed_view->getScene()->addItem(circle.get());
    }
    for(size_t i = detection.points_[cam_idx]->size(); i < feature_points_.size(); i++){
        feature_points_[i]->setVisible(false);
    }

    for(size_t i = 0; i < detection.points_[cam_idx]->size(); i++){
        feature_points_[i]->setVisible(true);
        feature_points_[i]->setPos((*detection.points_[cam_idx])[i].x_-7.5,(*detection.points_[cam_idx])[i].y_-7.5);
    }
}

void Camera_Feed::showDetectionAxes(const AxisPixels& axes){
    if(detection_axes_.empty()){

        detection_axes_.push_back(std::shared_ptr<QGraphicsLineItem>(new QGraphicsLineItem(axes.center_[0],axes.center_[1],axes.x_[0],axes.x_[1])));
        detection_axes_.push_back(std::shared_ptr<QGraphicsLineItem>(new QGraphicsLineItem(axes.center_[0],axes.center_[1],axes.y_[0],axes.y_[1])));
        detection_axes_.push_back(std::shared_ptr<QGraphicsLineItem>(new QGraphicsLineItem(axes.center_[0],axes.center_[1],axes.z_[0],axes.z_[1])));

        for(int i = 0; i < 3; i++){
            ui_->feed_view->getScene()->addItem(detection_axes_[i].get());
            detection_axes_[i]->setVisible(true);
            detection_axes_[i]->setPen(*(axes_pens_[i].get()));
        }
    }
    else{
        detection_axes_[0]->setLine(axes.center_[0], axes.center_[1], axes.x_[0], axes.x_[1]);
        detection_axes_[1]->setLine(axes.center_[0], axes.center_[1], axes.y_[0], axes.y_[1]);
        detection_axes_[2]->setLine(axes.center_[0], axes.center_[1], axes.z_[0], axes.z_[1]);
    }
}

void Camera_Feed::showAROverlay(const Partition& pattern, const bool& update_tiles){
    if(pattern.isEmpty()){
        return;
    }

    if(accepted_timestamp_ > 0){
        // Sleep for a second in order to show the accepted pattern
        long now = std::chrono::duration_cast<std::chrono::milliseconds>(
                    std::chrono::system_clock::now().time_since_epoch()).count();
        long m_dur = now-accepted_timestamp_;
        if(m_dur < 1000){
            QThread::msleep(1000 - m_dur);
        }
        accepted_timestamp_ = 0;
    }

    if(submode_ == OPT_SUBMODE_RANDOM_AREA){

        QRectF bb = pattern.bounding_box_;
        QPointF center = bb.center();
        double pen_size = 0.125 * std::max(bb.width(), bb.height());
        bb.setWidth(bb.width() - pen_size);
        bb.setHeight(bb.height() - pen_size);
        bb.moveCenter(center);
        box_pen_->setWidth(int(pen_size));
        box_pen_accepted_->setWidth(int(pen_size));

        if(!overlay_box_.get()){
            overlay_box_ = std::shared_ptr<QGraphicsRectItem>(new QGraphicsRectItem(bb));
            overlay_box_->setPen(*(box_pen_.get()));
            overlay_box_->setOpacity(0.5f);
            ui_->feed_view->getScene()->addItem(overlay_box_.get());
        }
        else{
            overlay_box_->setRect(bb);
            overlay_box_->setPen(*(box_pen_.get()));
        }
        // Set visibility of different overlay types
        overlay_box_->setVisible(true);
        for(const std::shared_ptr<QGraphicsPixmapItem>& tile : overlay_tiles_){
            tile->setVisible(false);
        }

    }
    else{
        // Set visibility of different overlay types
        if(overlay_box_.get()){
            overlay_box_->setVisible(false);
        }
        for(const std::shared_ptr<QGraphicsPixmapItem>& tile : overlay_tiles_){
            tile->setVisible(true);
        }

        if(update_tiles){
            // Remove old tiles
            for(const std::shared_ptr<QGraphicsPixmapItem>& tile : overlay_tiles_){
                ui_->feed_view->getScene()->removeItem(tile.get());
            }
            overlay_tiles_.clear();

            // Add new tiles
            QImage::Format format = QImage::Format_RGBA8888;
            for(const std::shared_ptr<cv::Mat>& tile : pattern.tiles_){
                QImage tile_q = QImage(tile->data, tile->rows, tile->cols,
                                       tile->step, format);
                overlay_tiles_.insert(overlay_tiles_.begin(),
                                      std::shared_ptr<QGraphicsPixmapItem>(
                                          new QGraphicsPixmapItem(
                                              QPixmap::fromImage(tile_q))));
                ui_->feed_view->getScene()->addItem(overlay_tiles_[0].get());
                overlay_tiles_[0]->setVisible(true);
                overlay_tiles_[0]->setOpacity(0.5f);
            }
        }


        if(pattern.homographies_.size() != overlay_tiles_.size()){
            return;
        }

        // Set homographies as tile transforms
        for(size_t i = 0; i < overlay_tiles_.size(); i++){
            Eigen::Matrix3d h = pattern.homographies_[i];
            if(h(2,2) < 0.0f){
                h *= -1.0f;
            }
            overlay_tiles_[i]->setTransform(QTransform(h(0,0),h(1,0),h(2,0),h(0,1),h(1,1),h(2,1),h(0,2),h(1,2),h(2,2)));
        }
    }

    if(update_tiles){
        // Remove old axes
        for(const std::shared_ptr<QGraphicsLineItem>& axis : overlay_axes_){
            ui_->feed_view->getScene()->removeItem(axis.get());
        }
        overlay_axes_.clear();

        // Add new axes
        for(int i = 1; i < 4; i++){
            overlay_axes_.push_back(std::shared_ptr<QGraphicsLineItem>(new QGraphicsLineItem(pattern.axes_pixels_[0].x(),
                                                                       pattern.axes_pixels_[0].y(),
                                    pattern.axes_pixels_[i].x(),
                                    pattern.axes_pixels_[i].y())));
            ui_->feed_view->getScene()->addItem(overlay_axes_.back().get());
            overlay_axes_.back()->setVisible(true);
            overlay_axes_.back()->setPen(*(axes_pens_[i-1].get()));
        }
    }

    // Reset graphics scene size
    ui_->feed_view->fitViewToImage();
}

void Camera_Feed::showAcceptedAROverlay(const Partition& pattern){

    if(submode_ == OPT_SUBMODE_RANDOM_AREA){
        overlay_box_->setPen(*(box_pen_accepted_.get()));
    }
    else{
        QImage::Format format = QImage::Format_RGBA8888;
        for(size_t i = 0; i < overlay_tiles_.size(); i++){
            overlay_tiles_[i]->setOpacity(1.0f);
            //        overlay_tiles_[i].co
            //        QImage tile_q = QImage(pattern.tiles_accepted_[i]->data,
            //                               pattern.tiles_accepted_[i]->rows,
            //                               pattern.tiles_accepted_[i]->cols,
            //                               pattern.tiles_accepted_[i]->step, format);
            //        overlay_tiles_[overlay_tiles_.size() - i - 1]->setPixmap(
            //                    QPixmap::fromImage(tile_q));
        }
    }
    // Reset graphics scene size
    ui_->feed_view->fitViewToImage();

    // Create timestamp
    accepted_timestamp_ = std::chrono::duration_cast<std::chrono::milliseconds>(
                std::chrono::system_clock::now().time_since_epoch()).count();
}

void Camera_Feed::setCameraProperties(const std::vector<std::shared_ptr<Property>>& list){
    widget_list_ = list;
    QGridLayout* layout = static_cast<QGridLayout*>(ui_->frame_properties->layout());
    for(const std::shared_ptr<Property>& property : list){
        layout->addWidget(property.get());
    }
    if(list.size() > 0){
        advanced_->setHidden(false);
        ui_->frame_properties->setHidden(!(advanced_->value()));
    }

}

void Camera_Feed::setOverlayControlProperties(const std::vector<std::shared_ptr<Property> > &list, const bool &overwrite){
    QGridLayout* layout = static_cast<QGridLayout*>(ui_->group_overlay->layout());

    if(overwrite){
        for(const std::shared_ptr<Property>& property : list){
            layout->removeWidget(property.get());
        }
        widget_list_OC_ = list;
    }

    for(const std::shared_ptr<Property>& property : list){
        layout->addWidget(property.get());
        if(!overwrite){
            widget_list_OC_.push_back(property);
        }
    }
    if(list.size() > 0){
        ui_->group_overlay->setHidden(false);
    }
}
