#pragma once

#include "Common/data.h"

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class Main_Window; }
QT_END_NAMESPACE

class Main_Window : public QMainWindow{

    Q_OBJECT

public:
    ///
    /// \brief Main_Window
    /// \param parent
    /// \param data
    ///
    Main_Window(QWidget *parent = nullptr, std::shared_ptr<Data> data = nullptr);

    ///
    ~Main_Window();

private:
    ///
    Ui::Main_Window* ui_;

    ///
    std::shared_ptr<Data> data_;

private slots:

    ///
    /// \brief requestNewPatternType
    /// \param type
    ///
    void requestNewPatternType(const int& type);

    ///
    /// \brief saveConfig
    ///
    void saveConfig();

    ///
    /// \brief loadConfig
    ///
    void loadConfig();
};
