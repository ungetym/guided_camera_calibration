#pragma once

#include "Common/data_types.h"

#include <QGraphicsItemGroup>
#include <QGraphicsPixmapItem>
#include <QWidget>

namespace Ui {
class Camera_Feed;
}

class Camera_Feed : public QWidget
{
    Q_OBJECT

public:
    ///
    /// \brief Camera_Feed
    /// \param parent
    ///
    explicit Camera_Feed(QWidget *parent = nullptr);

    ///
    ~Camera_Feed();

public slots:
    ///
    /// \brief setImage
    /// \param frame
    ///
    void setImage(std::shared_ptr<const Frame> &frame);

    ///
    /// \brief showDetectedPattern
    /// \param detection
    ///
    void showDetectedPattern(const Detection_Frame& detection);

    ///
    /// \brief showDetectionAxes
    /// \param axes
    ///
    void showDetectionAxes(const AxisPixels& axes);

    ///
    /// \brief showAROverlay
    /// \param pattern
    /// \param update_tiles
    ///
    void showAROverlay(const Partition& pattern, const bool& update_tiles = false);

    ///
    /// \brief showAcceptedAROverlay
    /// \param pattern
    ///
    void showAcceptedAROverlay(const Partition& pattern);

    ///
    /// \brief setOptimizationMode is the setter for the optimization mode which
    ///                            determines the images (rgb and/or ir) used
    ///                            for detection
    /// \param mode                OPT_MODE_* variables
    ///
    void setOptimizationMode(const int& mode, const int& submode){
        mode_ = mode;
        submode_ = submode;
    }

    ///
    /// \brief setCameraProperties
    /// \param list
    ///
    void setCameraProperties(const std::vector<std::shared_ptr<Property>>& list);

    ///
    /// \brief setOverlayControlProperties
    /// \param list
    ///
    void setOverlayControlProperties(const std::vector<std::shared_ptr<Property>>& list, const bool& overwrite = true);


private:
    /// GUI
    Ui::Camera_Feed* ui_;

    std::shared_ptr<Property_Check> mirrored_;
    std::shared_ptr<Property_Check> advanced_;

    /// Detected corner UI elements
    std::vector<std::shared_ptr<QGraphicsEllipseItem>> feature_points_;
    std::shared_ptr<QPen> feature_pen_;
    /// Detection coordinate axes
    std::vector<std::shared_ptr<QGraphicsLineItem>> detection_axes_;

    /// The displayed image
    std::shared_ptr<QImage> current_image_ = nullptr;

    /// AR overlay tiles
    std::vector<std::shared_ptr<QGraphicsPixmapItem>> overlay_tiles_;

    /// AR overlay bounding box
    std::shared_ptr<QPen> box_pen_, box_pen_accepted_;
    std::shared_ptr<QGraphicsRectItem> overlay_box_;

    /// AR overlay axes
    std::vector<std::shared_ptr<QGraphicsLineItem>> overlay_axes_;
    std::vector<std::shared_ptr<QPen>> axes_pens_;

    /// Optimization mode
    int mode_ = -1;
    int submode_ = -1;

    /// List of camera property and overlay property widgets
    std::vector<std::shared_ptr<Property>> widget_list_, widget_list_OC_;

    ///
    Qt::WindowFlags feed_window_flags_;
    QSize feed_size_;

    ///
    long accepted_timestamp_ = 0;


signals:
    ///
    /// \brief camStartRequested
    ///
    void camStartRequested();

    ///
    /// \brief camPauseRequested
    ///
    void camPauseRequested();

    ///
    /// \brief camStopRequested
    ///
    void camStopRequested();

    ///
    /// \brief requestNextPattern
    ///
    void requestNextPattern();
};
