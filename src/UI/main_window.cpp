#include "main_window.h"
#include "./ui_main_window.h"

Main_Window::Main_Window(QWidget *parent, std::shared_ptr<Data> data)
    : QMainWindow(parent)
    , ui_(new Ui::Main_Window)
    , data_(data)

{
    ui_->setupUi(this);
    this->setWindowTitle("Guided Camera Calibration");
    this->setWindowIcon(QIcon("./bin/Icons/Cam_white.png"));

    ui_->tab_pattern->setPattern(data_->pattern_);
    ui_->tab_optimization->setOptimizer(data_->optimizer_);
    ui_->tab_camera_feed->setOverlayControlProperties(data_->optimizer_->propertyListOverlayControl());
    ui_->tab_camera_feed->setOverlayControlProperties(data_->ar_overlay_->propertyList(), false);

    // Connect data to logger
    connect(data_.get(), &Data::log, ui_->widget_log, &Logger::log);

    // Connect pattern creator to logger
    connect(data_->pattern_.get(), &Pattern::log, ui_->widget_log, &Logger::log);

    // Connect camera feed controls to camera
    connect(ui_->tab_camera_feed, &Camera_Feed::camStartRequested,
            data_->camera_, &Camera::start);
    connect(ui_->tab_camera_feed, &Camera_Feed::camPauseRequested,
            data_->camera_, &Camera::pause);
    connect(ui_->tab_camera_feed, &Camera_Feed::camStopRequested,
            data_->camera_, &Camera::stop);
    // If a new image is received by the camera, show it in the camera feed view
    connect(data_->camera_, &Camera::receivedNewFrame,
            ui_->tab_camera_feed, &Camera_Feed::setImage);
    // Connect camera to logger
    connect(data_->camera_, &Camera::log, ui_->widget_log, &Logger::log);

    // If a pattern is detected, show the detections in the camera feed
    connect(data_->pattern_.get(), &Pattern::patternDetected,
            ui_->tab_camera_feed, &Camera_Feed::showDetectedPattern);

    // If AR overlay is recalculated, updated the UI elements in the camera feed
    connect(data_->ar_overlay_.get(), &AR_Overlay::overlayChanged,
            ui_->tab_camera_feed, &Camera_Feed::showAROverlay);
    connect(data_->ar_overlay_.get(), &AR_Overlay::detectionAxesCalculated,
            ui_->tab_camera_feed, &Camera_Feed::showDetectionAxes);

    connect(data_->ar_overlay_.get(), &AR_Overlay::matchedPartition,
            ui_->tab_camera_feed, &Camera_Feed::showAcceptedAROverlay);

    // If next pattern is requested, also remove last proposal
    connect(ui_->tab_camera_feed, &Camera_Feed::requestNextPattern,
            data_->optimizer_.get(), &Optimizer::calculateNextProposal);

    // Connect pattern PDF export button
    connect(ui_->tab_pattern, &Pattern_Widget::patternPDFrequested,
            [this](const QPageSize& page_size, const double& dpi){
        // Get qimage of current pattern
        QImage pattern_image = data_->pattern_->getPattern();
        // Create PDF
        IO::createPatternPDF(pattern_image, page_size, dpi);
    });

    // Connect camera parameter widget to show current intrinsics
    connect(data_->optimizer_.get(), &Optimizer::updatedCameraParameters,
            ui_->widget_camera_params, &Camera_Parameter_Widget::setCameraParameters);
    connect(data_->optimizer_.get(), &Optimizer::updatedCameraParameters,
            ui_->tab_metrics, &Metrics_Widget::updateParameters);
    connect(data_->optimizer_.get(), &Optimizer::updatedOptimizationMode,
            ui_->tab_camera_feed, &Camera_Feed::setOptimizationMode);
    connect(ui_->widget_camera_params, &Camera_Parameter_Widget::visualizationRequested,
            data_->camera_, &Camera::visualizeCalibration);
    connect(ui_->widget_camera_params, &Camera_Parameter_Widget::parametersUpdated,
            data_->ar_overlay_.get(), &AR_Overlay::updateCameraParameters);
    connect(ui_->widget_camera_params, &Camera_Parameter_Widget::parametersUpdated,
            data_->optimizer_.get(), &Optimizer::updateCameraParameters);

    // Connect camera parameter widget to logger
    connect(ui_->widget_camera_params, &Camera_Parameter_Widget::log,
            ui_->widget_log, &Logger::log);

    // Connect optimizer to logger
    connect(data_->optimizer_.get(), &Optimizer::log,
            ui_->widget_log, &Logger::log);

    // Connect metrics tab to logger
    connect(ui_->tab_metrics, &Metrics_Widget::log, ui_->widget_log, &Logger::log);


    // Check whether camera initialized and get initial parameters
    if(data_->camera_->isReady()){
        ui_->widget_camera_params->setVisualizationAvailable(data_->camera_->supportsVisualization());
        ui_->widget_camera_params->setCameraParameters(data_->camera_->getInitialParameters());
        ui_->tab_camera_feed->setCameraProperties(data_->camera_->getProperties());
    }
    else{
        connect(data_->camera_, &Camera::initialized, [this](){
            ui_->widget_camera_params->setVisualizationAvailable(
                        data_->camera_->supportsVisualization());
        });
        connect(data_->camera_, &Camera::initialized,
                ui_->widget_camera_params, &Camera_Parameter_Widget::setCameraParameters);
        connect(data_->camera_, &Camera::propertiesInitialized,
                ui_->tab_camera_feed, &Camera_Feed::setCameraProperties);
    }

    // Connect new pattern type request
    connect(ui_->tab_pattern, &Pattern_Widget::differentPatternTypeRequested,
            this, &Main_Window::requestNewPatternType);

    // Connect Config IO
    connect(ui_->actionSave_Config, &QAction::triggered, this, &Main_Window::saveConfig);
    connect(ui_->actionSave_As_Default, &QAction::triggered, this, &Main_Window::saveConfig);
    connect(ui_->actionLoad_Config, &QAction::triggered, this, &Main_Window::loadConfig);
    connect(ui_->actionReset_To_Default, &QAction::triggered, this, &Main_Window::loadConfig);

    // Load default settings
    ui_->actionReset_To_Default->triggered();
}

Main_Window::~Main_Window(){
    delete ui_;
}

void Main_Window::requestNewPatternType(const int& type){
    // Disconnect pattern signals
    disconnect(data_->pattern_.get(), &Pattern::log, ui_->widget_log, &Logger::log);
    disconnect(data_->pattern_.get(), &Pattern::patternDetected,
               ui_->tab_camera_feed, &Camera_Feed::showDetectedPattern);
    // Switch to new pattern type
    data_->switchPatternType(type);
    // Connect new pattern
    connect(data_->pattern_.get(), &Pattern::log, ui_->widget_log, &Logger::log);
    connect(data_->pattern_.get(), &Pattern::patternDetected,
            ui_->tab_camera_feed, &Camera_Feed::showDetectedPattern);
    // Set pattern to pattern tab
    ui_->tab_pattern->setPattern(data_->pattern_);
}

void Main_Window::saveConfig(){
    bool ask_user = (this->sender() == ui_->actionSave_Config);
    std::string error;
    IO::saveUISettings(data_->getAllProperties(), ask_user, &error);
}

void Main_Window::loadConfig(){
    bool ask_user = (this->sender() == ui_->actionLoad_Config);
    std::string error;
    IO::loadUISettings(data_->getAllProperties(), ask_user, &error);
}
