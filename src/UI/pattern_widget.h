#pragma once

#include <Patterns/pattern.h>

#include <QPageSize>
#include <QWidget>

namespace Ui {
class Pattern_Widget;
}

class Pattern_Widget : public QWidget
{
    Q_OBJECT

public:
    ///
    /// \brief Pattern_Widget
    /// \param parent
    ///
    explicit Pattern_Widget(QWidget *parent = nullptr);

    ///
    ~Pattern_Widget();

    ///
    /// \brief setPattern
    /// \param pattern
    ///
    void setPattern(const std::shared_ptr<Pattern>& pattern);

private:

    ///
    /// \brief setProperties
    /// \param list
    ///
    void setProperties(const std::vector<std::vector<std::shared_ptr<Property>>>& list);

private:
    ///
    Ui::Pattern_Widget* ui_;

    std::shared_ptr<Property_Selector> pattern_type_;
    std::shared_ptr<Property_Selector> pdf_format_;
    std::shared_ptr<Property_Spin> pdf_dpi_;

    ///
    std::vector<std::shared_ptr<Property>> widget_list_pattern_;
    std::vector<std::shared_ptr<Property>> widget_list_detector_;

    /// A list of most common paper formats
    std::vector<std::tuple<QString, int, int, QPageSize>> templates_ = {
        { "A0", 841, 1189, QPageSize::A0},
        { "A1", 594, 841, QPageSize::A1},
        { "A2", 420, 594, QPageSize::A2},
        { "A3", 297, 420, QPageSize::A3},
        { "A4", 210, 297, QPageSize::A4},
        { "A5", 148, 210, QPageSize::A5},
        { "A6", 105, 148, QPageSize::A6},
        { "B0", 1000, 1414, QPageSize::B0},
        { "B1", 707, 1000, QPageSize::B1},
        { "B2", 500, 707, QPageSize::B2},
        { "B3", 353, 500, QPageSize::B3},
        { "B4", 250, 353, QPageSize::B4},
        { "B5", 176, 250, QPageSize::B5},
        { "B6", 125, 176, QPageSize::B6}
    };

    ///
    std::shared_ptr<Pattern> pattern_;

signals:

    ///
    /// \brief patternPDFrequested
    /// \param page_size
    /// \param dpi
    ///
    void patternPDFrequested(const QPageSize& page_size, const double& dpi);

    ///
    /// \brief differentPatternTypeRequested
    /// \param type
    ///
    void differentPatternTypeRequested(const int& type);
};
