#include "logger.h"
#include "ui_logger.h"

#include <QTime>

Logger::Logger(QWidget *parent) :
    QWidget(parent),
    ui_(new Ui::Logger)
{
    ui_->setupUi(this);
}

Logger::~Logger()
{
    delete ui_;
}

void Logger::log(const QString& msg, const int& type){
    QString new_log;
    if(type==LOG_ERROR){//set error text
        QObject* sender = QObject::sender();
        new_log+=QTime::currentTime().toString()+": "+sender->objectName()+" Error: ";
        ui_->text_box->setTextColor(QColor::fromRgb(255,0,0));
    }
    else if(type==LOG_NORMAL){//set status text
        new_log+=QTime::currentTime().toString()+": ";
        ui_->text_box->setTextColor(QColor::fromRgb(0,0,0));
    }
    else if(type==LOG_WARNING){//set warning text
        new_log+=QTime::currentTime().toString()+": Warning: ";
        ui_->text_box->setTextColor(QColor::fromRgb(255,128,0));
    }
    new_log+=msg;

    if(type==LOG_APPEND){//append msg to last logger entry
        ui_->text_box->insertPlainText(new_log);
    }
    else{
        ui_->text_box->append(new_log);
    }
    repaint();
}

void Logger::logMat(const QString& msg, const cv::Mat& mat){
    ui_->text_box->setTextColor(QColor::fromRgb(0,0,0));
    ui_->text_box->append(msg);
    for(int row = 0; row<mat.rows; row++){
        QString mat_row = "  ";
        for(int col = 0; col<mat.cols; col++){
            if(mat.type()==CV_64F){
                mat_row += QString::number((float)mat.at<double>(row,col)).mid(0,8)+"\t";
            }
            else if(mat.type()==CV_32F){
                mat_row += QString::number((float)mat.at<double>(row,col)).mid(0,8)+"\t";
            }
        }
        ui_->text_box->append(mat_row);
    }
    repaint();
}
