#pragma once

#include <QtCharts/QtCharts>
#include <QWidget>

/// Parameter flags
const int PARAM_RMSE = 0;
const int PARAM_TRACE = 1;
const int PARAM_FX = 2;
const int PARAM_FY = 3;
const int PARAM_CX = 4;
const int PARAM_CY = 5;
const int PARAM_K1 = 6;
const int PARAM_K2 = 7;
const int PARAM_K3 = 8;
const int PARAM_K4 = 9;
const int PARAM_K5 = 10;
const int PARAM_K6 = 11;
const int PARAM_P1 = 12;
const int PARAM_P2 = 13;

namespace Ui {
class Parameter_Graph;
}

class Parameter_Graph : public QWidget
{
    Q_OBJECT

public:
    ///
    /// \brief Parameter_Graph
    /// \param parent
    ///
    explicit Parameter_Graph(QWidget *parent = nullptr);
    ~Parameter_Graph();

public slots:

    ///
    /// \brief clearData
    ///
    void clearData();

    ///
    /// \brief extendParameterSeries
    /// \param params
    ///
    void extendParameterSeries(const double& value);

    ///
    /// \brief setParameterSeries
    /// \param history
    ///
    void setParameterSeries(const std::vector<double>& history);

    ///
    /// \brief setParameterType
    /// \param param_code
    ///
    void setParameterType(const int& param_code);

private:
    ///
    Ui::Parameter_Graph* ui_;
    /// Chart for visualization
    QChart* chart_ = nullptr;
    QChartView* chart_view_ = nullptr;
    /// Data series
    QLineSeries* chart_data_ = nullptr;
    /// Axes
    QValueAxis* x_axis_ = nullptr;
    QValueAxis* y_axis_ = nullptr;

signals:
    ///
    /// \brief newHistoryRequested
    /// \param param_code
    ///
    void newHistoryRequested(const int& param_code);
};
