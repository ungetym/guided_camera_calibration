#include "parameter_graph.h"
#include "ui_parameter_graph.h"

Parameter_Graph::Parameter_Graph(QWidget *parent) :
    QWidget(parent),
    ui_(new Ui::Parameter_Graph)
{
    ui_->setupUi(this);

    // Create chart view and add to layout
    chart_view_ = new QChartView();
    QGridLayout* layout = static_cast<QGridLayout*>(ui_->chart_box->layout());
    layout->removeWidget(ui_->placeholder);
    delete ui_->placeholder;
    layout->addWidget(chart_view_,0,0,1,3);

    // Create data series and add to chart
    chart_data_ = new QLineSeries();
    chart_ = new QChart();
    chart_->addSeries(chart_data_);
    chart_view_->setChart(chart_);
    chart_->legend()->setVisible(false);

    connect(ui_->selector_param, &QComboBox::currentIndexChanged, this, &Parameter_Graph::newHistoryRequested);
    connect(ui_->selector_param, &QComboBox::currentIndexChanged, [this](const int& param_code){
        std::vector<QString> titles = {"trace(RMSE)","trace(Sigma)","f_x","f_y","c_x","c_y","k1","k2","k3","k4","k5","k6","p1","p2"};
        ui_->chart_box->setTitle(titles[param_code]);
    });
}

Parameter_Graph::~Parameter_Graph(){
    delete ui_;
}

void Parameter_Graph::clearData(){
    chart_data_->clear();
    if(x_axis_){
        chart_->removeAxis(x_axis_);
        chart_data_->detachAxis(x_axis_);
    }
    if(y_axis_){
        chart_->removeAxis(y_axis_);
        chart_data_->detachAxis(y_axis_);
    }
}

void Parameter_Graph::extendParameterSeries(const double& value){

    if(chart_data_->count() == 0){
        x_axis_ = new QValueAxis();
        x_axis_->setRange(-1.0,1.0);
        x_axis_->setTickCount(3);
        y_axis_ = new QValueAxis();
        y_axis_->setMin((value > 0.0) ? 0.99*value : 1.01*value);
        y_axis_->setMax((value > 0.0) ? 1.01*value : 0.99*value);
        y_axis_->setTickCount(5);
        chart_->addAxis(x_axis_, Qt::AlignBottom);
        chart_->addAxis(y_axis_, Qt::AlignLeft);
        chart_data_->attachAxis(x_axis_);
        chart_data_->attachAxis(y_axis_);
    }
    else{
        int x_range_max = int (ceil(double(chart_data_->count()) / 5.0)+0.01) * 5;
        x_axis_->setRange(0.0, x_range_max);
        x_axis_->setTickCount(x_range_max / 5 + 1);
        y_axis_->setMin(std::min(y_axis_->min(),(value > 0.0) ? 0.99*value : 1.01*value));
        y_axis_->setMax(std::max(y_axis_->max(),(value > 0.0) ? 1.01*value : 0.99*value));
    }

    chart_data_->append(double(chart_data_->count()), value);
}

void Parameter_Graph::setParameterSeries(const std::vector<double>& history){
    clearData();
    for(const double& val : history){
        extendParameterSeries(val);
    }
}

void Parameter_Graph::setParameterType(const int& param_code){
    ui_->selector_param->setCurrentIndex(param_code);
}
