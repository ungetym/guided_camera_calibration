#include "ar_overlay.h"

#include "Common/math_gcc.h"

AR_Overlay::AR_Overlay(){
    store_images_ = std::shared_ptr<Property_Check>(new Property_Check(nullptr, "AROverlay_Store_Accepted_Imgs", "Store accepted images", false));

    properties_ = {store_images_};
}

AR_Overlay::~AR_Overlay(){
    // unparent property widgets to avoid double deletion
    store_images_->setParent(nullptr);
}

void AR_Overlay::updatePattern(const Partition& partition){

    initial_matched_detection_ = {};
    initial_outer_points_ = {};

    partition_ = partition;

    if(parameters_.size() == 0){
        return;
    }

    recalculateHomographies();
    waiting_for_new_pose_ = true;
    emit requestNewPose();
    emit overlayChanged(partition_, true);
}

void AR_Overlay::updateCameraParameters(const std::vector<Cam_Parameters> &parameters){

    initial_matched_detection_ = {};
    initial_outer_points_ = {};

    bool is_initial_parameter_set = (parameters_.size() == 0);
    bool pattern_already_availabe = !partition_.isEmpty();

    parameters_ = parameters;
    recalculateHomographies();

    if(pattern_already_availabe){
        waiting_for_new_pose_ = true;
    }
    if(is_initial_parameter_set){
        emit requestNewPose();
    }

    //emit overlayChanged(partition_, false);
}

void AR_Overlay::updatePose(const Pose &pose){
    initial_matched_detection_ = {};
    initial_outer_points_ = {};

    pose_ = pose;
    recalculateHomographies();
    waiting_for_new_pose_ = false;

    // Set tile update to true in order to reset the overlay color
    emit overlayChanged(partition_, true);
}

void AR_Overlay::checkNewDetection(const Detection_Frame& detection){

    if(waiting_for_new_pose_ || detection.points_.size() == 0
            || detection.points_[0]->size() == 0){
        return;
    }

    // Determine, which camera to use
    // (IR is only used for IR intrinsics optimization)
    size_t cam_idx = (mode_ == 1) ? 1 : 0;

    long current_time_in_ms =
            std::chrono::duration_cast<std::chrono::milliseconds>(
                std::chrono::system_clock::now().time_since_epoch()).count();

    // Calculate detection center and axes
    Eigen::Vector2d center(0,0);
    std::vector<Feature_Point> inner_points;
    for(const Idx_ID& inner_corner_: partition_.inner_id_corners_){
        for(const Feature_Point& feat_point : *(detection.points_[cam_idx])){
            if(feat_point.id_ == inner_corner_.id_){
                center += feat_point.pixel();
                inner_points.push_back(feat_point);
                break;
            }
        }
    }
    center /= 4.0;

    if(inner_points.size() < 4){
        return;
    }

    AxisPixels axes(center,
                    center + (inner_points[1].pixel() - inner_points[0].pixel())/
            partition_.inner_corner_distances_[0],
            center + (inner_points[2].pixel() - inner_points[0].pixel())/
            partition_.inner_corner_distances_[1],
            center);
    emit detectionAxesCalculated(axes);


    // Check center and axes directions
    if((partition_.axes_pixels_[0] - center).norm() > match_threshold_){
        initial_matched_detection_ = {};
        initial_outer_points_ = {};
        return;
    }

    if( abs((partition_.axes_pixels_[1]-partition_.axes_pixels_[0]).normalized().transpose() * (axes.x_-axes.center_).normalized()) < std::cos(10.0f/180.0f*M_PIf) ||
            abs((partition_.axes_pixels_[2]-partition_.axes_pixels_[0]).normalized().transpose() * (axes.y_-axes.center_).normalized()) < std::cos(10.0f/180.0f*M_PIf)){
        initial_matched_detection_ = {};
        initial_outer_points_ = {};
        return;
    }


    std::vector<Feature_Point> outer_points;
    if(submode_ != OPT_SUBMODE_RANDOM_AREA){

        // Check, whether detection matches overlay by checking the four outer
        // corners
        int num_matched_corners = 0;
        for(const Idx_ID& outer_corner_: partition_.outer_id_corners_){
            // Find this corner in the detected corners
            for(const Feature_Point& feat_point : *(detection.points_[cam_idx])){
                if(feat_point.id_ == outer_corner_.id_){
                    const Eigen::Vector2d detected_pos = feat_point.pixel();
                    const Eigen::Vector2d overlay_pos = partition_.corner_target_pixels_[outer_corner_.idx_];
                    float distance = (detected_pos - overlay_pos).norm();
                    if(distance < match_threshold_){
                        num_matched_corners ++;
                    }
                    outer_points.push_back(feat_point);
                    break;
                }
            }
        }

        // If overlay is not matched, delete initial matched detection and return
        if(num_matched_corners < 3){
            initial_matched_detection_ = {};
            initial_outer_points_ = {};
            return;
        }
    }
    else{ // Check whether pattern outer points are in bounding box  - one
        // should be close to the border
        std::vector<Feature_Point> outer_points;
        QRectF& bb = partition_.bounding_box_;
        double dist_threshold = 0.125 * std::max(bb.width(), bb.height());
        double min_dist = 9999999.9;
        for(const Idx_ID& outer_corner_: partition_.outer_id_corners_){
            // Find this corner in the detected corners
            for(const Feature_Point& feat_point : *(detection.points_[cam_idx])){
                if(feat_point.id_ == outer_corner_.id_){
                    const Eigen::Vector2d detected_pos = feat_point.pixel();

                    double left = detected_pos.x() - bb.left();
                    double right = bb.right() - detected_pos.x();
                    double top = detected_pos.y() - bb.top();
                    double bottom = bb.bottom() - detected_pos.y();

                    min_dist = std::min(min_dist, std::min(std::abs(left), std::min(std::abs(right), std::min(std::abs(top), std::abs(bottom)))));

                    if(left < 0.0 || right < 0.0 ||
                            top < 0.0 || bottom < 0.0){
                        initial_matched_detection_ = {};
                        initial_outer_points_ = {};
                        return;
                    }
                    outer_points.push_back(feat_point);
                }
            }
        }
        if(min_dist > dist_threshold){ // No outer corner on bounding box edge
            initial_matched_detection_ = {};
            initial_outer_points_ = {};
            return;
        }
    }

    // If overlay was matched before, check amount of movement/time...
    if(initial_matched_detection_.size() > 0){
        if(current_time_in_ms - overlay_match_time_in_ms_ > 1000){
            // Calculate movement
            double max_movement_px = 0;
            for(const Feature_Point& fp : outer_points){
                for(const Feature_Point& initial_fp : initial_outer_points_){
                    if(fp.id_ == initial_fp.id_){
                        max_movement_px = std::max(max_movement_px,
                                                   (fp.pixel()-initial_fp.pixel()).norm());
                    }
                }
            }
            // Smaller amount of movement reduces the motion blur
            if(max_movement_px < 5.0){
                emit matchedPartition(partition_);
                emit matchedDetection(detection.points_);
                initial_matched_detection_ = {};
                waiting_for_new_pose_ = true;

                if(store_images_->value()){
                    detection.frame_->rgb_.save(QString::number(num_debug_imgs_)+".png");
                    num_debug_imgs_++;
                }
            }
            else{
                initial_matched_detection_ = detection.points_;
                initial_outer_points_ = outer_points;
                overlay_match_time_in_ms_ = current_time_in_ms;
            }
        }
        else{
            return;
        }
    }
    // ...else save detection and time as initial match
    else{
        initial_matched_detection_ = detection.points_;
        initial_outer_points_ = outer_points;
        overlay_match_time_in_ms_ = current_time_in_ms;
    }

}

bool AR_Overlay::recalculateHomographies(){

    if(partition_.isEmpty()){
        return false;
    }

    // Determine, which camera to use
    // (IR is only used for IR intrinsics optimization)
    size_t cam_idx = (mode_ == 1) ? 1 : 0;

    // Transform every partition corner and map it to image plane
    std::vector<Eigen::Vector2d> corner_pixels;
    QRectF& bb = partition_.bounding_box_;
    for(const Eigen::Vector3d& corner : partition_.corners_){
        Eigen::Vector4d p_cam = pose_.mat_ * Eigen::Vector4d(corner[0], corner[1], corner[2], 1.0);
        Eigen::Vector2d pixel = parameters_[cam_idx].projectCamToPixel(Eigen::Vector3d(p_cam[0],p_cam[1],p_cam[2]));
        corner_pixels.push_back(pixel);
        if(corner_pixels.size() == 1){
            bb.setRect(pixel[0], pixel[1], 0 ,0);
        }
        else{
            bb.setLeft(std::min(bb.left(),pixel[0]));
            bb.setRight(std::max(bb.right(),pixel[0]));
            bb.setTop(std::min(bb.top(),pixel[1]));
            bb.setBottom(std::max(bb.bottom(),pixel[1]));
        }
    }
    partition_.corner_target_pixels_ = corner_pixels;

    // Transform axis points
    partition_.axes_pixels_.clear();
    Eigen::Vector4d axis_zero_cam = pose_.mat_ * Eigen::Vector4d(partition_.center_[0], partition_.center_[1], partition_.center_[2], 1.0);
    partition_.axes_pixels_.push_back(parameters_[cam_idx].projectCamToPixel(Eigen::Vector3d(axis_zero_cam[0], axis_zero_cam[1], axis_zero_cam[2])));
    for(const Eigen::Vector3d& axis : partition_.axes_){
        Eigen::Vector4d axis_cam = pose_.mat_ * Eigen::Vector4d(partition_.center_[0]+axis[0], partition_.center_[1]+axis[1], partition_.center_[2]+axis[2], 1.0);
        partition_.axes_pixels_.push_back(parameters_[cam_idx].projectCamToPixel(Eigen::Vector3d(axis_cam[0], axis_cam[1], axis_cam[2])));
    }

    // Calculate homographies
    partition_.homographies_.clear();
    for(size_t i = 0; i < partition_.tiles_.size(); i++){

        Idx_Tuple idx_tuple = partition_.tile_corners_[i];
        std::shared_ptr<cv::Mat> tile = partition_.tiles_[i];

        partition_.homographies_.insert(partition_.homographies_.begin(),
                                        math_gcc::calcHomography({Eigen::Vector2d(0.0, 0.0),
                                                                  Eigen::Vector2d(tile->cols, 0.0),
                                                                  Eigen::Vector2d(0.0, tile->rows),
                                                                  Eigen::Vector2d(tile->cols, tile->rows)},
                                                                 {corner_pixels[idx_tuple.a_],
                                                                  corner_pixels[idx_tuple.b_],
                                                                  corner_pixels[idx_tuple.c_],
                                                                  corner_pixels[idx_tuple.d_]}));
    }

    return true;
}
