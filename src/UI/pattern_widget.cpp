#include "pattern_widget.h"
#include "ui_pattern_widget.h"

Pattern_Widget::Pattern_Widget(QWidget *parent) :
    QWidget(parent),
    ui_(new Ui::Pattern_Widget)
{
    ui_->setupUi(this);

    pattern_type_ = std::shared_ptr<Property_Selector>(new Property_Selector(nullptr, "Pattern_Type", "Pattern Type", {"ChArUco Board", "Checkerboard"}, 1));
    QGridLayout* layout = static_cast<QGridLayout*>(ui_->frame_parameters->layout());
    layout->addWidget(pattern_type_.get(), 0, 0);

    QStringList templates;
    for(const std::tuple<QString, int, int, QPageSize>& temp : templates_ ){
        templates.push_back(std::get<0>(temp)+"  "
                            +QString::number(std::get<1>(temp))+" x "
                            +QString::number(std::get<2>(temp)));
    }
    pdf_format_ = std::shared_ptr<Property_Selector>(new Property_Selector(nullptr, "Pattern_Template", "Template", templates, 0));
    pdf_dpi_ = std::shared_ptr<Property_Spin>(new Property_Spin(nullptr, "Pattern_DPI", "DPI", 0, 10000, 150));
    layout = static_cast<QGridLayout*>(ui_->frame_canvas->layout());
    layout->addWidget(pdf_format_.get(), 0, 0);
    layout->addWidget(pdf_dpi_.get(), 1, 0);

    connect(ui_->button_create_PDF, &QPushButton::clicked, [this](){
        emit patternPDFrequested(std::get<3>(templates_[pdf_format_->index()]),
                pdf_dpi_->value());});

    connect(pattern_type_.get(), &Property_Selector::valueChanged,
            this, &Pattern_Widget::differentPatternTypeRequested);
    connect(pattern_type_.get(), &Property_Selector::valueChanged,
            [this](){ui_->pattern_view->resetImage();});

}

Pattern_Widget::~Pattern_Widget(){
    // unparent property widgets to avoid double deletion
    for(const std::shared_ptr<QWidget>& property : widget_list_pattern_){
        property->setParent(nullptr);
    }
    for(const std::shared_ptr<QWidget>& property : widget_list_detector_){
        property->setParent(nullptr);
    }
    delete ui_;
}

void Pattern_Widget::setPattern(const std::shared_ptr<Pattern>& pattern){
    // Disconnect old pattern if available
    if(pattern_){
        disconnect(ui_->button_create_pattern, &QPushButton::clicked, pattern_.get(), &Pattern::createPattern);
        disconnect(pattern_.get(), &Pattern::patternChanged, ui_->pattern_view, qOverload<const QImage&>(&Graphics_View::setImage));
        disconnect(pattern_.get(), &Pattern::patternChanged, ui_->pattern_view, &Graphics_View::fitViewToImage);
    }
    if(pattern){
        // Save new pattern
        pattern_ = pattern;
        setProperties({pattern_->patternProperties(), pattern_->detectorProperties()});
        // Connect new pattern
        connect(ui_->button_create_pattern, &QPushButton::clicked, pattern_.get(), &Pattern::createPattern);
        connect(pattern_.get(), &Pattern::patternChanged, ui_->pattern_view, qOverload<const QImage&>(&Graphics_View::setImage));
        connect(pattern_.get(), &Pattern::patternChanged, ui_->pattern_view, &Graphics_View::fitViewToImage);
    }
}

void Pattern_Widget::setProperties(const std::vector<std::vector<std::shared_ptr<Property>>> &list){
    QGridLayout* layout = static_cast<QGridLayout*>(ui_->frame_parameters->layout());

    // Remove old properties if available
    for(const std::shared_ptr<Property>& property : widget_list_pattern_){
        layout->removeWidget(property.get());
    }

    widget_list_pattern_ = list[0];

    for(const std::shared_ptr<Property>& property : list[0]){
        layout->addWidget(property.get());
    }

    // Repeat everything for the detector parameters
    layout = static_cast<QGridLayout*>(ui_->frame_detector_parameters->layout());

    // Remove old properties if available
    for(const std::shared_ptr<Property>& property : widget_list_detector_){
        layout->removeWidget(property.get());
    }

    widget_list_detector_ = list[1];

    for(const std::shared_ptr<Property>& property : list[1]){
        layout->addWidget(property.get());
    }
}
