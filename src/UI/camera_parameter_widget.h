#pragma once

#include "Common/io.h"
#include "UI/logger.h"

#include <QWidget>

namespace Ui {
class Camera_Parameter_Widget;
}

class Camera_Parameter_Widget : public QWidget
{
    Q_OBJECT

public:
    ///
    /// \brief Camera_Parameter_Widget
    /// \param parent
    ///
    explicit Camera_Parameter_Widget(QWidget *parent = nullptr);

    ///
    ~Camera_Parameter_Widget();

public slots:

    ///
    /// \brief setCameraParameters
    /// \param parameters
    ///
    void setCameraParameters(const std::vector<Cam_Parameters>& parameters);

    ///
    /// \brief setVisualizationAvailable
    /// \param available
    ///
    void setVisualizationAvailable(const bool& available);

private slots:

    ///
    /// \brief setAcceptButtonVisible
    ///
    void setAcceptButtonVisible();

    ///
    /// \brief updateGUI
    /// \param index
    ///
    void updateGUI(const int &index);

    ///
    /// \brief updateFromGUI
    ///
    void updateFromGUI();

    ///
    /// \brief loadCameraParameters
    ///
    void loadCameraParameters();

    ///
    /// \brief saveCameraParameters
    ///
    void saveCameraParameters();

    ///
    /// \brief requestVisualization
    ///
    void requestVisualization();

private:
    ///
    Ui::Camera_Parameter_Widget* ui_;

    ///
    std::vector<Cam_Parameters> parameters_;

    ///
    int currently_selected_cam = -1;

signals:

    ///
    /// \brief visualizationRequested
    /// \param parameters
    ///
    void visualizationRequested(const std::vector<Cam_Parameters>& parameters);

    ///
    /// \brief log
    /// \param msg
    /// \param type
    ///
    void log(const QString& msg, const int& type);

    ///
    /// \brief parametersUpdated
    /// \param parameters_
    ///
    void parametersUpdated(const std::vector<Cam_Parameters>& parameters_);
};
